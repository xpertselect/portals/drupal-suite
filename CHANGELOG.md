# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

### Fixed

---

## Release 3.7.0

### Added

- Add a Facet mapping configuration screen to `AdminSettingsForm`.

### Changed

- Use the new facet mapping in the `SearchForm`.

---

## Release 3.6.0

### Changed

- Add an option to the `NodeIndexationHookService` to index unpublished nodes.
- Change the `getContentIdentifiersToIndex` function from private to protected in `BaseContentIndexationForm` to allow overrides.

---

## Release 3.5.1

### Changed

- Upgraded the `xpertselect/psr-tools` dependency
- The OAS enums added by `xs_lists` will now only include the list source in the description when the source is remote.

---

## Release 3.5.0

### Added

- Autocomplete endpoint in the `xs_searchable_content` module.

---

## Release 3.4.2

### Fixed

- The `node_access` hook in the `xs_page` module no longer returns allowed to prevent the permissions from being overwritten.

---

## Release 3.4.1

### Changed

- The dataloader of `xs_lists` wil now retry HTTP requests under certain conditions.

---

## Release 3.4.0

### Added

- Add an information category filter.

---

## Release 3.3.0

### Added

- Add a document type filter.

---

## Release 3.2.0

### Added

- Add a hierarchical list formatter.

---

## Release 3.1.0

### Added

- Add a link to the IMCE file manager in the user menu.

---

## Release 3.0.1

### Changed

- Updated various composer packages.

---

## Release 3.0.0

### Changed

- Update to php8.2.

___

## Release 2.10.2

### Fixed

- The autocomplete javascript in `xs_searchable_content` now escapes html that is enter in the search box.

___

## Release 2.10.1

### Fixed

- The search controller in `xs_searchable_content` now adds the correct cache tags, in case a content type filter is in the query.

___

## Release 2.10.0

### Changed

- Dependencies have been updated to their latest minor and/or patch version.

___

## Release 2.9.0

### Changed

- Switch to a post request when a solr request has more than 4096 characters.

___

## Release 2.8.3

### Changed

- Add a missing translation for 'Private result'.
- Alter the markup of a result to reduce the amount of empty space.

___

## Release 2.8.2

### Changed

- Remove the tooltip of the cogwheel button in the `admin-controls`.

___

## Release 2.8.1

### Changed

- Disable the OSDD tab-to-search endpoint when `short name` is empty in the config.

___

## Release 2.8.0

### Changed

- The OSDD endpoint for tab-to-search is no longer configurable.
- Add a setting to define the short name of tab-to-search.
- Removed the `OpenSearchDefinitionEndpointInterface`.

___

## Release 2.7.1

### Fixed

- Add aria-hidden on close icon of an active filter.

___

## Release 2.7.0

### Changed

- On the search page, alter the markup of the result details to a datalist element.
- Added the `aria-current` attribute of the selected facets on the search page.
- Add `aria-label` attribute to the `collapse-facets` button.
- Change the default tooltips to the bootstrap tooltips.

___

## Release 2.6.1

### Changed

- The search field in the `search block` now includes an aria-label attribute.
- The header of each facet in the facet list uses a h4-element instead of a strong-element.

___

## Release 2.6.0

### Added

- Created a `ResultsPerThemeBlock` in the `xs_searchable_content` module.

### Added

- A _service collector_ `TranslationMapperManager` in `xs_lists` is added that collects services with tag `translation_mapper`. Modules can create a service that implements `TranslationMapperInterface` and is tagged with `translation_mapper` so that a named list and their translations can be created.
- A global `xs_searchable_content_content_type_list` function is now available to use in `xs_lists`, which uses the `TranslationMapperManager` _service collector_.

### Fixed

- `MutableSearchProfile` in `xs_search` now calls the `sortCallback` with the `parsedQuery` parameter.

___

## Release 2.5.4

### Changed

- Simplify the logic needed in the `xs_searchable_content` twig templates for search results.
- Added a `SolrHighlightedFieldTrait` in `xs_searchable_content` for retrieving a highlighted snippet from a Solr response.

___

## Release 2.5.3

### Changed

- `getReusableActiveFilterString` is moved from the `SearchController` to a `SearchFiltersTrait` in `xs_searchable_content`.

___

## Release 2.5.2

### Fixed

- The `SearchForm` in `xs_searchable_content` now links to the new search page.
- The base-search result in `xs_searchable_content` always displays content as `show_extended_display`.

___

## Release 2.5.1

### Fixed

- Add missing facets in `$translationMap` in the `SearchController` in `xs_searchable_content`.
- Translation of facet names contain the context "facet" once again.

### Changed

- The first character of facet labels and active filter labels are now capitalized.

___

## Release 2.5.0

### Added

- `SearchProfile` in `xs_searchable_content` adds the facet field `facet_type`.
- Adding facet fields using `MutableSearchProfile` in `xs_search` should now be done using the `addFacetFields` method. This will **overwrite** the `facet.field` parameter when this is set in `additionalParameters`.  

___

## Release 2.4.0

### Added

- The `xs_searchable_content` now contains twig templates for rendering the search results page.
- The `PreparingSearchQuery` event in `xs_searchable_content` contains a new mapping to correctly render search results of different types.

___

## Release 2.3.0

### Added

- The generic search profile in `xs_searchable_content` now contains a generic facet mapping that allows filtering on type.

___

## Release 2.2.0

### Added

- The `xs_searchable_content` module now provides the constant `NODE_DATE_FORMAT` which describes the format of date fields of a Drupal Node.
- The `xs_searchable_content` module now provides the constant `DEFAULT_INDEX_BATCH_SIZE` which defines the default batch size.
- The `xs_searchable_content` module now has a trait to extract values from a Drupal node.
- The `TypeFilterTrait` in the `xs_searchable_content` module now has a function to get all types.

___

## Release 2.1.0

### Added

- The `xs_searchable_content` module now provides a Controller for searching through all available content-types.

---

## Release 2.0.2

### Fixed

- Allowed the `xs_searchable_content` settings to be managed.
- Created an `XsSearchableContent` class to hold various constants.

---

## Release 2.0.1

### Fixed

- The `search block` now adds css classes used by the js

---

## Release 2.0.0

### Added

- Moved the header `search block` to the `xs_searchable_content` module.
- Expanded the `search block` to be able to work with documents.
- `PreparingSuggestQuery` events are dispatched when suggestions are requested, so that modules can modify the search profile.

---

## Release 1.5.2

### Fixed

- Allow the use of multiple get parameters with the same name.

---

## Release 1.5.1

### Fixed

- When deleting a dictionary check for the given content type, in case of colliding machine names.

---

## Release 1.5.0

### Added

- A new service for indexing dictionaries is introduced.

---

## Release 1.4.1

### Fixed

- The `QueryStringParser` (And `ParsedQuery`) allow the `searchProfile` to interpret the sorting.

---

## Release 1.4.0

### Changed

- The `QueryStringParser` (And `ParsedQuery`) can now parse sorting information from a incoming query.

---

## Release 1.3.0

### Changed

- `swagger-api/swagger-ui` is updated to a new major version: 5.

---

## Release 1.2.2

### Fixed

- The API specification won't be generated anymore when the API is disabled.

---

## Release 1.2.1

### Fixed

- The MarkDown to HTML TwigFilter now passes its text through the Drupal Xss filter prior to rendering.

---

## Release 1.2.0

### Changed

- The `ApiFormatSetter` is no longer `@final` to ensure Drupal 10 compatibility.
- The `xs_api_user_api_key_block` Drupal Block no longer renders content when the API is disabled.

### Fixed

- The `NodeIndexationService` can now be instantiated even when not all required configuration is present. It will refuse to operate and log its unhealthy state rather than crashing Drupal.

---

## Release 1.1.0

### Added

- The `SolrClient` of the `xs_solr` module now receives an EventDispatcher to dispatch events when the Solr index is updated.

## Release 1.0.1

### Fixed

- The `AdminSettingsForm` of `xs_searchable_content` now displays a submit button and saves its values correctly.
- All properties of the `BaseContentIndexationForm` are no longer `readonly`. This caused Drupal to crash after submitting the form.
- Access checking is set to `FALSE` explicitly when querying for nodes to index, which is mandatory in Drupal 10.
- The correct service name for the `NodeIndexationService` is now used in the creation of the `NodeIndexationBatchService`.
- The format of the Solr query for deleting documents from the index is fixed.

## Release 1.0.0

### Changed

- Removed the `xs_ckan` and `xs_membership` modules. These are now offered as part of the `xpertselect-portals/xsp_dcat_suite` package.

---

## Release 0.22.1

### Changed

- Some translations in various modules and updated the translations file which had a double empty line at the end of the file.

---

## Release 0.22.0

### Changed

- The `xs_api` module no longer has a hard requirement on the `drupal/simple_sitemap` module. This dependency has been moved to the `suggest` section of the `composer.json` file.

---

## Release 0.21.0

### Changed

- The `composer.json` now allows development releases for the `drupal/openid_connect` package for Drupal 10 support.

---

## Release 0.20.1

### Changed

- The `CkanSdkFactory` of `xs_membership` now assigns its instances with ID `default.user`.

---

## Release 0.20.0

### Added

- The `CkanSdkFactory` methods now assign instance ID's to the created instances.

### Fixed

- Corrected the `rowspan` attribute of a table cell in the CKAN status dashboard page when CKAN has no private datasets.

---

## Release 0.19.2

### Added

- Translations for `xs_api`, `xs_lists` and `xs_osdd` settings page.

### Changed

- Settings menu items for `xs_api` and `xs_lists` modules.

---

## Release 0.19.1

### Fixed

- The user event subscriber is now registered as a service tagged as `event_subscriber`, so that it actually subscribes to (user) events.

---

## Release 0.19.0

### Added

- The `MembershipService` method `getMembershipsForUser` now supports the optional parameter `labelField`. This parameter controls which field will be used as a value in the resulting array.

---

## Release 0.18.4

### Fixed

- The `xs_membership_user_memberships_block` Drupal block now correctly renders the individual memberships.
- The translation for the header of the `xs_membership_user_memberships_block` now correctly includes the `@count` variable.
- The table holding the memberships of the `xs_membership_user_memberships_block` is now wrapped in a `<div class="table-responsive" />` to ensure it does not break the page layout on smaller viewports.

---

## Release 0.18.3

- Upgraded the `xpertselect/ckan-sdk` package to version `0.4.0` from `0.3.0`.

---

## Release 0.18.2

### Fixed

- The `CkanUserResolver` now correctly receives a `CkanSdk` instance configured with CKAN sysadmin credentials.

---

## Release 0.18.1

### Changed

- It is now possible to specify the key field (which defaults to 'id' for backwards compatibility) when retrieving memberships via the `MembershipService::getMembershipsForUser` method.

---

## Release 0.18.0

### Added

- The `xs_membership` module now ensures that a corresponding CKAN user exists whenever a Drupal user is created and/or updated.
- The `CkanUserResolver` now maintains an in-memory cache of resolved CKAN users. This cache is shared between all instances of the `CkanUserResolver` service.

---

## Release 0.17.0

### Added

- Introduced the `xs_membership.ckan_sdk.user` service for interacting with CKAN as the current Drupal user.

---

## Release 0.16.1

### Changed

- Upgraded the `xpertselect/ckan-sdk` version to `0.3.0` from `0.2.2`.

---

## Release 0.16.0

### Added

- The `xs_membership` module will now always insert the membership block on the user profile page.
- The `MembershipService` now allows specifying the minimum permission a user should have when retrieving the memberships of a user from CKAN.

___

## Release 0.15.1

### Changed

- The `label_to_identifier` and `identifier_to_label` twig functions can now accept `NULL` as their first parameter and will return an empty string instead of throwing an error.

### Fixed

- The `xs_membership` Memberships block now has the correct PHPDoc annotations so that Drupal can treat it as a Drupal block.

---

## Release 0.15.0

### Added

- Ported the `xs_ckan` module from [gitlab.textinfo.nl](https://gitlab.textinfo.nl). This module enables Drupal to interact with the CKAN instance of an XpertSelect Portals installation. Parts of this module have been refactored into a new Drupal module, `xs_membership`.
- Introduced the `xs_membership` module for managing the roles/permissions of a Drupal user in CKAN.
- The `xs_general` module now offers a `number-formatter` library which uses Javascript to format the number found in HTML elements annotated with the `formatNumber` class according to the browsers' locale.

### Changed

- The changelog viewer on the XpertSelect dashboard will now strip the "Changelog" header when rendering the changelog.

---

## Release 0.14.2

### Fixed

- The `SearchService` offered via `xs_search` will now fallback to a default collection name when the `xs_search.settings.search_collection` configuration entry is missing or invalid, rather than crashing PHP during its creation via its factory.

___

## Release 0.14.1

### Added

- The XpertSelect dashboard now offers the changelog of individual XpertSelect packages and the root project itself, if those projects come bundled with a `CHANGELOG.md` file.

___

## Release 0.13.0

### Added

- The `SearchService` now offers a `suggest` method to request autocomplete suggestions for a given query.

___

## Release 0.12.2

### Added

- This project now maintains a changelog in `CHANGELOG.md`. The changelog contains the changes introduced since release `0.10.7`.

### Fixed

- The HTTP GET parameter `commit` will now be correctly encoded when sending an update request to Apache Solr.

---

## Release 0.12.1

### Fixed

- Corrected the PHPDoc of various methods of the `SearchService` class.

---

## Release 0.12.0

### Added

- Ported the `xs_search` Drupal module from [gitlab.textinfo.nl](https://gitlab.textinfo.nl).

---

## Release 0.11.0

### Added

- Optimization of managed settings.

---

## Release 0.10.7

### Fixed

- The username of a user that logs in using the OpenID connection for the first time is set to the full name instead of the email address.
