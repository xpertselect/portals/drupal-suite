# XpertSelect / Portals / Drupal Suite

[gitlab.com/xpertselect/portals/drupal-suite](https://gitlab.com/xpertselect/portals/drupal-suite)

The suite of modules required to integrate all the XpertSelect Portals features into a [Drupal](https://www.drupal.org/) installation.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect-portals/xsp_drupal_suite`](https://packagist.org/packages/xpertselect-portals/xsp_drupal_suite) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect-portals/xsp_drupal_suite
```
