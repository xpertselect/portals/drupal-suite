<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Drupal\xs_api\XsApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserApiKeyBlock.
 *
 * Provides a 'User API key' Block that displays the API key of the user from the route match.
 *
 * @Block(
 *   id = "xs_api_user_api_key_block",
 *   admin_label = @Translation("User API key"),
 * )
 */
final class UserApiKeyBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id,
                                $plugin_definition): self
  {
    /** @var RouteMatchInterface $currentRouteMatch */
    $currentRouteMatch = $container->get('current_route_match');

    /** @var AccountProxyInterface $currentUser */
    $currentUser = $container->get('current_user');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $currentRouteMatch,
      $currentUser,
      boolval($configFactory->get(XsApi::SETTINGS_KEY)->get('enabled') ?? FALSE)
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param RouteMatchInterface   $currentRouteMatch The current route match to get the user for which the API key is
   *                                                 shown for
   * @param AccountProxyInterface $currentUser       The current user to determine whether the current user can manage
   *                                                 API keys
   * @param bool                  $apiEnabled        Whether the API is currently enabled
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              private readonly RouteMatchInterface $currentRouteMatch,
                              private readonly AccountProxyInterface $currentUser,
                              private readonly bool $apiEnabled)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    if (!$this->apiEnabled) {
      return [];
    }

    /** @var null|UserInterface $user */
    $user = $this->currentRouteMatch->getParameter('user');

    if (is_null($user)) {
      return [];
    }

    $manageApiKeyLink = $this->currentUser->hasPermission('use key authentication')
      ? Link::createFromRoute($this->t('manage'), 'key_auth.user_key_auth_form', ['user' => $user->id()])
      : NULL;

    return [
      '#theme'               => 'user_api_key',
      '#api_key'             => $user->get('api_key')->getString(),
      '#manage_api_key_link' => $manageApiKeyLink,
      '#cache'               => [
        'keys'    => [
          'xs_api',
          'api_key',
          'user:' . $user->id(),
        ],
        'tags'    => [
          'user:' . $user->id(),
        ],
        'max-age' => CacheBackendInterface::CACHE_PERMANENT,
      ],
    ];
  }
}
