<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Url;
use Drupal\xs_api\Event\PostGenerationEvent;
use Drupal\xs_api\Event\PreGenerationEvent;
use Drupal\xs_general\DrupalUrlUtilities;
use OpenApi\Analysers\TokenAnalyser;
use OpenApi\Analysis;
use OpenApi\Annotations\Components;
use OpenApi\Annotations\ExternalDocumentation;
use OpenApi\Annotations\Info;
use OpenApi\Annotations\OpenApi;
use OpenApi\Context;
use OpenApi\Generator;
use OpenApi\Loggers\DefaultLogger;
use Psr\EventDispatcher\EventDispatcherInterface;
use RuntimeException;

/**
 * Class ApiSpecificationGenerator.
 *
 * A Drupal service for generating the Open API Specification based on annotations and site configuration.
 */
final class ApiSpecificationGenerator
{
  use DrupalUrlUtilities;

  /**
   * The key of the cache item holding the generated specification.
   *
   * @var string
   */
  private const CACHE_KEY = 'xs_api.open_api_specification';

  /**
   * ApiSpecificationGenerator constructor.
   *
   * @param CacheBackendInterface    $cache         The Drupal cache backend
   * @param ExtensionList            $extensionList The list of Drupal extensions
   * @param ConfigFactoryInterface   $configFactory The Drupal config store
   * @param EventDispatcherInterface $dispatcher    Service for dispatching events
   */
  public function __construct(private readonly CacheBackendInterface $cache,
                              private readonly ExtensionList $extensionList,
                              private readonly ConfigFactoryInterface $configFactory,
                              private readonly EventDispatcherInterface $dispatcher)
  {
  }

  /**
   * Loads the specification from cache, if available. Otherwise, the specification will be generated, cached and
   * returned.
   *
   * @return array<mixed, mixed> The specification
   */
  public function load(): array
  {
    if (($cached = $this->cache->get(self::CACHE_KEY)) && isset($cached->data)) {
      $jsonSpecification = $cached->data;
    } else {
      $jsonSpecification = $this->generateSpecification()->jsonSerialize();
      $this->cache->set(self::CACHE_KEY, $jsonSpecification);
    }

    return (array) $jsonSpecification;
  }

  /**
   * Flushes the generated API specification from the cache, ensuring that the next call to `load()` will generate a
   * fresh copy of the specification.
   */
  public function flush(): void
  {
    $this->cache->delete(self::CACHE_KEY);
  }

  /**
   * Generates the API specification based on the annotations found in XS modules and the configuration of this Drupal
   * site.
   *
   * @return OpenApi The generated specification
   */
  public function generateSpecification(): OpenApi
  {
    $baseSpecification = $this->createBaseApiSpecification();
    $analysis          = $this->createOpenApiAnalysis($baseSpecification);

    $this->dispatcher->dispatch(new PreGenerationEvent($baseSpecification));

    $apiSpecification = Generator::scan($this->getPaths(), [
      'analyzer' => new TokenAnalyser(),
      'analysis' => $analysis,
    ]);

    if (is_null($apiSpecification)) {
      throw new RuntimeException('Failed to generate the Open API Specification from available annotations');
    }

    $this->dispatcher->dispatch(new PostGenerationEvent($apiSpecification));
    $this->filterApiPaths($apiSpecification);

    return $apiSpecification;
  }

  /**
   * Creates a base OpenApi instance that can be expanded upon by event listeners and by scanning the annotations found
   * in the PHP codebase.
   *
   * @return OpenApi The base API specification
   */
  private function createBaseApiSpecification(): OpenApi
  {
    return new OpenApi([
      'info'         => $this->createBaseInfoSection(),
      'components'   => $this->createBaseComponentsSection(),
      'servers'      => $this->createBaseServerSection(),
      'externalDocs' => $this->createBaseExternalDocsSection(),
    ]);
  }

  /**
   * Creates a base Info block for the OpenApi specification.
   *
   * @return Info The base Info block
   */
  private function createBaseInfoSection(): Info
  {
    return new Info([
      'title'   => sprintf('%s - API', $this->configFactory->get('system.site')->get('name')),
      'version' => '1.0',
    ]);
  }

  /**
   * Creates a base Components block for the OpenApi specification.
   *
   * @return Components The base Components block
   */
  private function createBaseComponentsSection(): Components
  {
    return new Components(['schemas' => []]);
  }

  /**
   * Creates the base Server array for the OpenApi specification.
   *
   * @return array<int, array{url: string}> The base list of servers
   */
  private function createBaseServerSection(): array
  {
    $url = Url::fromRoute('<front>')->setAbsolute();

    return [['url' => $this->drupalUrlAsString($url)]];
  }

  /**
   * Creates a base ExternalDocs block for the OpenApi specification.
   *
   * @return ExternalDocumentation The base ExternalDocumentation block
   */
  private function createBaseExternalDocsSection(): ExternalDocumentation
  {
    return new ExternalDocumentation([
      'url'         => 'https://jsonapi.org/format/1.0/',
      'description' => 'This API conforms to the JSON API 1.0 specification.',
    ]);
  }

  /**
   * Retrieve the paths to scan for annotations.
   *
   * @return array<int, array<int, string>> The paths to scan
   */
  private function getPaths(): array
  {
    $installedExtensions = $this->extensionList->getAllInstalledInfo();
    $paths               = [DRUPAL_ROOT . '/../vendor/xpertselect'];

    foreach ($this->extensionList->getPathnames() as $name => $value) {
      if (!array_key_exists($name, $installedExtensions)) {
        continue;
      }

      if (!str_starts_with($name, 'xs_')) {
        continue;
      }

      $paths[] = DRUPAL_ROOT . '/' . $this->extensionList->getPath($name);
    }

    return [$paths];
  }

  /**
   * Creates the base Analysis object to be given to the OpenApi Generator.
   *
   * @param OpenApi $specification The base specification to expand upon
   *
   * @return Analysis The analysis instance to be given to the Generator
   */
  private function createOpenApiAnalysis(OpenApi $specification): Analysis
  {
    $analysis = new Analysis([$specification], new Context([
      'version' => NULL,
      'logger'  => new DefaultLogger(),
    ]));

    $specification->_analysis = $analysis;

    return $analysis;
  }

  /**
   * Filters the '/' path from the available paths, but only if there are other paths available. The '/' path is
   * inserted to ensure that at least one path is present in order to properly generate the specification. This path can
   * be removed when other paths are present.
   *
   * @param OpenApi $specification The specification to filter the paths for
   */
  private function filterApiPaths(OpenApi $specification): void
  {
    if (1 === count($specification->paths)) {
      return;
    }

    $specification->paths = array_filter($specification->paths, function($pathItem): bool {
      return '/' !== $pathItem->path;
    });
  }
}
