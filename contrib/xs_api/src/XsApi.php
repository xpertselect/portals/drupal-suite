<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api;

/**
 * Class XsApi.
 *
 * Simple PHP class holding various constants used by the `xs_api` Drupal module.
 */
final class XsApi
{
  /**
   * The key identifying the `xs_api` settings in Drupal.
   */
  public const SETTINGS_KEY = 'xs_api.settings';

  /**
   * The name of the format of XS API requests.
   */
  public const API_REQUEST_FORMAT = 'xs_api';
}
