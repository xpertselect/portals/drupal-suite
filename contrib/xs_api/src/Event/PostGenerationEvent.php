<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Event;

use OpenApi\Annotations\OpenApi;
use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class PostGenerationEvent.
 *
 * Event that describes that the contained OpenApi specification instance was just generated and includes the
 * annotations found in the PHP codebase.
 *
 * This is the last chance for any listeners to make any changes to the specification before it is published.
 */
final class PostGenerationEvent implements StoppableEventInterface
{
  /**
   * PostGenerationEvent constructor.
   *
   * @param OpenApi $apiSpecification The generated specification
   */
  public function __construct(public readonly OpenApi $apiSpecification)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function isPropagationStopped(): bool
  {
    return FALSE;
  }
}
