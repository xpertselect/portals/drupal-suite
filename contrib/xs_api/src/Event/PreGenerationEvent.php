<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Event;

use OpenApi\Annotations\OpenApi;
use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class PreGenerationEvent.
 *
 * Event that describes that the contained OpenApi specification instance is about to be expanded with the annotations
 * found within the PHP codebase.
 *
 * If any of the annotations in the PHP codebase rely on the presence of dynamic annotations, then this event allows
 * such dynamic annotations to be inserted prior to scanning the PHP codebase.
 */
final class PreGenerationEvent implements StoppableEventInterface
{
  /**
   * PreGenerationEvent constructor.
   *
   * @param OpenApi $apiSpecification The specification that is about to be expanded with annotations found in the PHP
   *                                  codebase
   */
  public function __construct(public readonly OpenApi $apiSpecification)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function isPropagationStopped(): bool
  {
    return FALSE;
  }
}
