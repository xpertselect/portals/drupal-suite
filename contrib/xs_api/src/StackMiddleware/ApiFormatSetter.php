<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\StackMiddleware;

use Drupal\xs_api\XsApi;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class ApiFormatSetter.
 *
 * This sets the request format to xs-api if it is a request to a URI that starts with /api/datasets (for now).
 */
class ApiFormatSetter implements HttpKernelInterface
{
  /**
   * ApiFormatSetter constructor.
   *
   * @param HttpKernelInterface $httpKernel The decorated kernel
   */
  public function __construct(protected HttpKernelInterface $httpKernel)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE): Response
  {
    if (str_starts_with($request->getRequestUri(), '/api/datasets')) {
      $request->setRequestFormat(XsApi::API_REQUEST_FORMAT);
    }

    return $this->httpKernel->handle($request, $type, $catch);
  }
}
