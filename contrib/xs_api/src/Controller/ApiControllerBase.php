<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use XpertSelect\JsonApi\JsonApiResponseUtilities;

/**
 * Class ApiControllerBase.
 *
 * Contains the base functionality all API-related controllers should have. In addition, several Open Api Specification
 * annotations are defined here.
 *
 * @OA\PathItem(path="/")
 *
 * @OA\SecurityScheme(securityScheme="bearer_token", type="apiKey", in="header", name="Authorization")
 *
 * @OA\Parameter(
 *   parameter="StartParameter",
 *   description="Describes from which offset to start returning records.",
 *   name="start",
 *   in="query",
 *   required=false,
 *
 *   @OA\Schema(type="integer", format="int64", default=0, minimum=0)
 * )
 */
abstract class ApiControllerBase extends ControllerBase
{
  use JsonApiResponseUtilities;
}
