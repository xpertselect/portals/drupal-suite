<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class SwaggerController.
 *
 * Drupal controller responsible for rendering the page containing the Open API Specification.
 */
final class SwaggerController extends ControllerBase
{
  /**
   * Returns the markup for the swagger page.
   *
   * @return array<string, mixed> The Drupal render array
   */
  public function view(): array
  {
    $containedId = 'swagger-ui';

    return [
      '#type'       => 'html_tag',
      '#tag'        => 'div',
      '#attributes' => [
        'class' => 'container',
        'id'    => $containedId,
      ],
      '#attached' => [
        'library' => [
          'xs_api/swagger-init',
          'xs_api/swagger-ui',
        ],
        'drupalSettings' => [
          'xs_api' => [
            'swaggerContainer' => $containedId,
            'oasLink'          => Url::fromRoute('xs_api.open-api-specification')->toString(),
          ],
        ],
      ],
    ];
  }
}
