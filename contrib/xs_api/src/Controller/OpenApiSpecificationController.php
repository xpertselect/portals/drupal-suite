<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\xs_api\ApiSpecificationGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class OpenApiSpecificationController.
 *
 * Drupal controller responsible for rendering the Open Api Specification in various representations.
 */
final class OpenApiSpecificationController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): OpenApiSpecificationController
  {
    /** @var ApiSpecificationGenerator $specificationService */
    $specificationService = $container->get('xs_api.api-specification-generator');

    return new self($specificationService);
  }

  /**
   * OpenApiSpecificationController constructor.
   *
   * @param ApiSpecificationGenerator $apiSpecificationGenerator The API specification generator
   */
  public function __construct(private readonly ApiSpecificationGenerator $apiSpecificationGenerator)
  {
  }

  /**
   * Renders the Open API Specification of this XpertSelect Portals installation in a JSON representation.
   *
   * @return JsonResponse The JSON representation
   */
  public function jsonSpecification(): JsonResponse
  {
    return new JsonResponse($this->apiSpecificationGenerator->load());
  }
}
