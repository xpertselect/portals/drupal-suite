<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_api\XsApi;

/**
 * Class AdminSettingsForm.
 *
 * A Drupal configuration form for managing the settings of the `xs_api` module.
 */
final class AdminSettingsForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_api_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $config = $this->config(XsApi::SETTINGS_KEY);

    $form['enabled'] = [
      '#type'    => 'radios',
      '#title'   => $this->t('Enable API'),
      '#options' => [
        'y' => $this->t('Yes'),
        'n' => $this->t('No'),
      ],
      '#default_value' => $config->get('enabled') === TRUE ? 'y' : 'n',
      '#description'   => $this->t('After saving this setting the entire Drupal cache will be regenerated.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $this->config(XsApi::SETTINGS_KEY)
      ->set('enabled', $form_state->getValue('enabled') === 'y')
      ->save();

    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsApi::SETTINGS_KEY];
  }
}
