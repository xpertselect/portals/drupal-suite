<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\xs_api\XsApi;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use XpertSelect\JsonApi\JsonApiResponseUtilities;

/**
 * Class ApiHttpExceptionSubscriber.
 *
 * An exception subscriber that sets JSON responses for 403 and 404 exceptions.
 */
final class ApiHttpExceptionSubscriber extends HttpExceptionSubscriberBase
{
  use JsonApiResponseUtilities;

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int
  {
    return parent::getPriority() + 25;
  }

  /**
   * Sets a JSON response when a 403 occurs.
   *
   * @param ExceptionEvent $event The event that contains the response
   */
  public function on403(ExceptionEvent $event): void
  {
    $event->setResponse($this->noAccessErrorResponse());
  }

  /**
   * Sets a JSON response when a 404 occurs.
   *
   * @param ExceptionEvent $event The event that contains the response
   */
  public function on404(ExceptionEvent $event): void
  {
    $event->setResponse($this->notFoundErrorResponse());
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array
  {
    return [XsApi::API_REQUEST_FORMAT];
  }
}
