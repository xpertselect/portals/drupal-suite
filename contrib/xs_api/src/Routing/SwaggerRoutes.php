<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_api\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_api\XsApi;
use Symfony\Component\Routing\Route;

/**
 * Class SwaggerRoutes.
 *
 * Registers the routes to the Open API Specification documents and the Swagger page based on whether the API has been
 * enabled or not.
 */
final class SwaggerRoutes
{
  /**
   * Instantiates a new instance of this class.
   *
   * @param ConfigFactoryInterface $configFactory The factory for retrieving settings
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    return new self($configFactory->get(XsApi::SETTINGS_KEY)->get('enabled') ?? FALSE);
  }

  /**
   * SwaggerRoutes constructor.
   *
   * @param bool $apiEnabled Whether the API is enabled
   */
  public function __construct(public readonly bool $apiEnabled = FALSE)
  {
  }

  /**
   * Determine and return an array of routes that should be registered into Drupal.
   *
   * @return array<string, Route> The routes to register
   */
  public function routes(): array
  {
    if (!$this->apiEnabled) {
      return [];
    }

    return [
      'xs_api.open-api-specification' => new Route(
        '/open-api-specification.json',
        ['_controller' => '\Drupal\xs_api\Controller\OpenApiSpecificationController::jsonSpecification'],
        ['_access'     => 'TRUE'],
      ),
      'xs_api.swagger' => new Route(
        '/api',
        [
          '_controller' => '\Drupal\xs_api\Controller\SwaggerController::view',
          '_title'      => 'Open API Specification',
        ],
        ['_access'  => 'TRUE'],
        ['no_cache' => 'TRUE'],
      ),
    ];
  }
}
