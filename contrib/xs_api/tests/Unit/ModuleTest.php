<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_api\Unit;

use PHPUnit\Framework\Assert;
use Tests\xs_api\TestCase;

/**
 * @internal
 */
final class ModuleTest extends TestCase
{
  public static function templateDataset(): array
  {
    return [
      ['user_api_key'],
    ];
  }

  /**
   * @dataProvider templateDataset
   */
  public function testTemplateIsExposedViaThemeHook(string $template): void
  {
    Assert::assertArrayHasKey($template, xs_api_theme([], 'theme', 'test_theme', '/dev/null'));
  }
}
