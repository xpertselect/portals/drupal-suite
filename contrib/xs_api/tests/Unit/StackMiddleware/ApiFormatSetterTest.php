<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_api\Unit\StackMiddleware;

use Drupal\xs_api\StackMiddleware\ApiFormatSetter;
use Drupal\xs_api\XsApi;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Tests\xs_api\TestCase;

/**
 * @internal
 */
final class ApiFormatSetterTest extends TestCase
{
  public static function normalRequestFormatDataset(): array
  {
    return [
      ['/'],
      ['/api'],
      ['/api/foo'],
      ['/api/foo/api/datasets'],
    ];
  }

  public static function customRequestFormatDataset(): array
  {
    return [
      ['/api/datasets'],
      ['/api/datasets?foo=bar'],
      ['/api/datasets/foo'],
    ];
  }

  /**
   * @dataProvider normalRequestFormatDataset
   */
  public function testNoCustomRequestFormatIsSet(string $requestPath): void
  {
    $request = $this->getMockBuilder(Request::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getRequestUri', 'setRequestFormat'])
      ->getMock();

    $request->expects($this->once())
      ->method('getRequestUri')
      ->will($this->returnValue($requestPath));

    $request->expects($this->never())
      ->method('setRequestFormat');

    try {
      $setter = new ApiFormatSetter($this->mockHttpKernel());
      $setter->handle($request);
    } catch (Exception) {
      $this->fail();
    }
  }

  /**
   * @dataProvider customRequestFormatDataset
   */
  public function testCustomRequestFormatIsSet(string $requestPath): void
  {
    $request = $this->getMockBuilder(Request::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getRequestUri', 'getRequestFormat', 'setRequestFormat'])
      ->getMock();

    $request->expects($this->once())
      ->method('getRequestUri')
      ->will($this->returnValue($requestPath));

    $request->expects($this->once())
      ->method('setRequestFormat')
      ->with(XsApi::API_REQUEST_FORMAT);

    try {
      $setter = new ApiFormatSetter($this->mockHttpKernel());
      $setter->handle($request);
    } catch (Exception) {
      $this->fail();
    }
  }

  private function mockHttpKernel(): HttpKernelInterface
  {
    $httpKernel = $this->getMockBuilder(HttpKernelInterface::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['handle'])
      ->getMock();

    $httpKernel->expects($this->once())
      ->method('handle')
      ->will($this->returnValue(new Response()));

    return $httpKernel;
  }
}
