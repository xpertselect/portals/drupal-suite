<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_api\Unit\Event;

use Drupal\xs_api\Event\PreGenerationEvent;
use OpenApi\Annotations\OpenApi;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\StoppableEventInterface;
use Tests\xs_api\TestCase;

/**
 * @internal
 */
final class PreGenerationEventTest extends TestCase
{
  public function testEventIsStoppable(): void
  {
    $specification = new OpenApi([]);
    $event         = new PreGenerationEvent($specification);

    Assert::assertInstanceOf(StoppableEventInterface::class, $event);
  }

  public function testEventDoesNotStopPropagation(): void
  {
    $specification = new OpenApi([]);
    $event         = new PreGenerationEvent($specification);

    Assert::assertFalse($event->isPropagationStopped());
  }
}
