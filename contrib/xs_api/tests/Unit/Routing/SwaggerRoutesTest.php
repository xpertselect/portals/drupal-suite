<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_api\Unit\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\xs_api\Routing\SwaggerRoutes;
use Drupal\xs_api\XsApi;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\Assert;
use Symfony\Component\Routing\Route;
use Tests\xs_api\TestCase;

/**
 * @internal
 */
final class SwaggerRoutesTest extends TestCase
{
  public function testFactoryMethodUsesCorrectSetting(): void
  {
    $config = Mockery::mock(ImmutableConfig::class, function(MockInterface $mock) {
      $mock->shouldReceive('get')
        ->with('enabled')
        ->andReturn(TRUE);
    });

    /** @var ConfigFactoryInterface|MockInterface $configFactory */
    $configFactory = Mockery::mock(ConfigFactoryInterface::class, function(MockInterface $mock) use ($config) {
      $mock->shouldReceive('get')
        ->with(XsApi::SETTINGS_KEY)
        ->andReturn($config);
    });

    $router = SwaggerRoutes::create($configFactory);

    Assert::assertTrue($router->apiEnabled);
  }

  public function testFactoryMethodUsesDefaultWhenSettingIsMissing(): void
  {
    $config = Mockery::mock(ImmutableConfig::class, function(MockInterface $mock) {
      $mock->shouldReceive('get')
        ->with('enabled')
        ->andReturnNull();
    });

    /** @var ConfigFactoryInterface|MockInterface $configFactory */
    $configFactory = Mockery::mock(ConfigFactoryInterface::class, function(MockInterface $mock) use ($config) {
      $mock->shouldReceive('get')
        ->with(XsApi::SETTINGS_KEY)
        ->andReturn($config);
    });

    $router = SwaggerRoutes::create($configFactory);

    Assert::assertFalse($router->apiEnabled);
  }

  public function testWhenApiIsDisabledNoRoutesAreReturned(): void
  {
    $router = new SwaggerRoutes(FALSE);

    Assert::assertCount(0, $router->routes());
  }

  public function testWhenApiIsEnabledRoutesAreAdded(): void
  {
    $router = new SwaggerRoutes(TRUE);
    $routes = $router->routes();

    Assert::assertNotEmpty($router->routes());
    Assert::assertContainsOnlyInstancesOf(Route::class, $routes);
  }
}
