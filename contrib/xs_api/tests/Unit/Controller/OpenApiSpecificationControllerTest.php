<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_api\Unit\Controller;

use Drupal\xs_api\ApiSpecificationGenerator;
use Drupal\xs_api\Controller\OpenApiSpecificationController;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_api\TestCase;

/**
 * @internal
 */
final class OpenApiSpecificationControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('xs_api.api-specification-generator')
          ->andReturn(M::mock(ApiSpecificationGenerator::class));
      });

      Assert::assertInstanceOf(
        OpenApiSpecificationController::class,
        OpenApiSpecificationController::create($container)
      );
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
