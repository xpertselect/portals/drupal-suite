/* global drupalSettings: true, SwaggerUIBundle: false, SwaggerUIStandalonePreset: false */

(function (drupalSettings) {
    "use strict";

    window.onload = function () {
        window.ui = SwaggerUIBundle({
            url: drupalSettings.xs_api.oasLink,
            tagsSorter: "alpha",
            dom_id: "#" + drupalSettings.xs_api.swaggerContainer,
            validatorUrl: null,
            presets: [
                SwaggerUIBundle.presets.apis,
                SwaggerUIStandalonePreset.slice(1)
            ],
            plugins: [
                SwaggerUIBundle.plugins.DownloadUrl
            ],
            layout: "StandaloneLayout",
            defaultModelsExpandDepth: 0,
            tryItOutEnabled: false,
            supportedSubmitMethods: [],
            requestInterceptor: function (request) {
                request.headers["accept"] = "application/json, application/vnd.api+json";
                return request;
            }
        });
    };
})(drupalSettings);
