<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_openid\Unit;

use Drupal\xs_openid\XpertSelectOpenIdConnect;
use Mockery;
use RuntimeException;
use Tests\xs_openid\TestCase;

/**
 * @internal
 */
final class XpertSelectOpenIdConnectTest extends TestCase
{
  public function testGenerateUsernameThrowsExceptionWhenEmailIsMissing(): void
  {
    $this->expectException(RuntimeException::class);
    $this->expectExceptionMessage('There is no email to base the generated username on');

    /** @var XpertSelectOpenIdConnect $mock */
    $mock = Mockery::mock(XpertSelectOpenIdConnect::class)
      ->makePartial();

    $mock->generateUsername(NULL, [], NULL);
  }

  public function testGenerateUsernameThrowsExceptionWhenEmailIsEmpty(): void
  {
    $this->expectException(RuntimeException::class);
    $this->expectExceptionMessage('There is no email to base the generated username on');

    /** @var XpertSelectOpenIdConnect $mock */
    $mock = Mockery::mock(XpertSelectOpenIdConnect::class)
      ->makePartial();

    $mock->generateUsername(NULL, ['email' => ''], NULL);
  }

  public function testGenerateUsernameUsesEmailWhenAvailable(): void
  {
    /** @var XpertSelectOpenIdConnect $mock */
    $mock = Mockery::mock(XpertSelectOpenIdConnect::class)
      ->makePartial();

    $this->assertEquals(
      'foo',
      $mock->generateUsername(NULL, ['email' => 'foo'], NULL)
    );
  }
}
