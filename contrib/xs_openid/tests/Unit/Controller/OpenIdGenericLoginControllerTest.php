<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_openid\Unit\Controller;

use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use Drupal\xs_openid\Controller\OpenIdGenericLoginController;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_openid\TestCase;

/**
 * @internal
 */
final class OpenIdGenericLoginControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('plugin.manager.openid_connect_client')
          ->andReturn(M::mock(OpenIDConnectClientManager::class));

        $mock->shouldReceive('get')
          ->with('openid_connect.claims')
          ->andReturn(M::mock(OpenIDConnectClaims::class));
      });

      Assert::assertInstanceOf(
        OpenIdGenericLoginController::class,
        OpenIdGenericLoginController::create($container)
      );
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
