<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_openid\Unit;

use Drupal;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\xs_openid\Plugin\Menu\OpenIdGenericLogoutMenuLink;
use Mockery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\xs_openid\TestCase;

/**
 * @internal
 */
final class ModuleTest extends TestCase
{
  public function testLinkIsAlteredWhenOpenIdIsEnabled(): void
  {
    $this->mockOpenIdConfig(TRUE);

    $links = [];

    xs_openid_menu_links_discovered_alter($links);

    $this->assertIsArray($links);
    $this->assertArrayHasKey('user.logout', $links);

    $this->assertIsArray($links['user.logout']);
    $this->assertArrayHasKey('class', $links['user.logout']);

    $this->assertEquals(OpenIdGenericLogoutMenuLink::class, $links['user.logout']['class']);
  }

  public function testLinkIsNotAlteredWhenOpenIdIsDisabled(): void
  {
    $this->mockOpenIdConfig(FALSE);

    $links = [];

    xs_openid_menu_links_discovered_alter($links);

    $this->assertIsArray($links);
    $this->assertArrayNotHasKey('user.logout', $links);
  }

  private function mockOpenIdConfig(bool $expected): void
  {
    $mockedContainer = Mockery::mock(ContainerInterface::class);
    $mockedFactory   = Mockery::mock(ConfigFactory::class);
    $mockedConfig    = Mockery::mock(ImmutableConfig::class);

    $mockedConfig->shouldReceive('get')
      ->with('enabled')
      ->andReturn($expected);

    $mockedFactory->shouldReceive('get')
      ->with('openid_connect.settings.generic')
      ->andReturn($mockedConfig);

    $mockedContainer->shouldReceive('get')
      ->with('config.factory')
      ->andReturn($mockedFactory);

    Drupal::setContainer($mockedContainer);
  }
}
