<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_openid\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\xs_openid\XpertSelectOpenIdConnect;
use Drupal\xs_openid\XsOpenidServiceProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Definition;

/**
 * @internal
 */
final class XsOpenIdServiceProviderTest extends TestCase
{
  public function testNothingIsAlteredWhenServiceIsMissing(): void
  {
    $builder = $this->getMockBuilder(ContainerBuilder::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['has', 'getDefinition'])
      ->getMock();

    $builder->expects($this->once())
      ->method('has')
      ->with(XsOpenidServiceProvider::SERVICE_TARGET)
      ->will($this->returnValue(FALSE));

    $builder->expects($this->never())
      ->method('getDefinition');

    $provider = new XsOpenidServiceProvider();
    $provider->alter($builder);
  }

  public function testDefinitionIsAlteredWhenPresent(): void
  {
    $definition = $this->getMockBuilder(Definition::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['setClass'])
      ->getMock();

    $definition->expects($this->once())
      ->method('setClass')
      ->with(XpertSelectOpenIdConnect::class);

    $builder = $this->getMockBuilder(ContainerBuilder::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['has', 'getDefinition'])
      ->getMock();

    $builder->expects($this->once())
      ->method('has')
      ->with(XsOpenidServiceProvider::SERVICE_TARGET)
      ->will($this->returnValue(TRUE));

    $builder->expects($this->once())
      ->method('getDefinition')
      ->with(XsOpenidServiceProvider::SERVICE_TARGET)
      ->will($this->returnValue($definition));

    $provider = new XsOpenidServiceProvider();
    $provider->alter($builder);
  }
}
