<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_openid;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class XsOpenidServiceProvider.
 *
 * Drupal service provider that alters the openid_connect.openid_connect service in order to inject a XpertSelect
 * specific extension of the class.
 */
final class XsOpenidServiceProvider extends ServiceProviderBase
{
  /**
   * The service to alter.
   */
  public const SERVICE_TARGET = 'openid_connect.openid_connect';

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void
  {
    if (!$container->has(self::SERVICE_TARGET)) {
      return;
    }

    $definition = $container->getDefinition(self::SERVICE_TARGET);
    $definition->setClass(XpertSelectOpenIdConnect::class);
  }
}
