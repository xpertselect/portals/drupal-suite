<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_openid\Plugin\Menu;

use Drupal\user\Plugin\Menu\LoginLogoutMenuLink;

/**
 * Class OpenIdGenericLogoutMenuLink.
 *
 * Alters the standard Drupal user menu to directly point to the OpenID authentication endpoint rather than the generic
 * authentication page.
 */
final class OpenIdGenericLogoutMenuLink extends LoginLogoutMenuLink
{
  /**
   * {@inheritdoc}
   */
  public function getRouteName(): string
  {
    if ($this->currentUser->isAuthenticated()) {
      return 'user.logout';
    }

    return 'xs_openid.open_id_generic_login';
  }
}
