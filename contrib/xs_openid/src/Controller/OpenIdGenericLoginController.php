<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_openid\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\Plugin\OpenIDConnectClientInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OpenIDGenericLoginController.
 *
 * Drupal controller for authenticating users via OAuth.
 */
final class OpenIdGenericLoginController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var OpenIDConnectClientManager $openIdConnectClientManager */
    $openIdConnectClientManager = $container->get('plugin.manager.openid_connect_client');

    /** @var OpenIDConnectClaims $openIdConnectClaims */
    $openIdConnectClaims = $container->get('openid_connect.claims');

    return new self($openIdConnectClientManager, $openIdConnectClaims);
  }

  /**
   * OpenIDGenericLoginController constructor.
   *
   * @param OpenIDConnectClientManager $pluginManager The plugin manager
   * @param OpenIDConnectClaims        $claims        The OpenID Connect claims
   */
  public function __construct(protected readonly OpenIDConnectClientManager $pluginManager,
                              protected readonly OpenIDConnectClaims $claims)
  {
  }

  /**
   * Authenticate a user.
   *
   * @return Response The appropriate HTTP response
   *
   * @throws PluginException Thrown on any error from the plugin manager
   */
  public function login(): Response
  {
    if ($this->currentUser()->isAuthenticated()) {
      return $this->redirect('user.page');
    }

    $client = $this->getOpenIdClient();
    $this->prepareSession();

    return $client->authorize($this->claims->getScopes($client));
  }

  /**
   * Create and return a configured OpenId client.
   *
   * @return OpenIDConnectClientInterface The created client
   *
   * @throws PluginException Thrown on any error from the plugin manager
   */
  private function getOpenIdClient(): OpenIDConnectClientInterface
  {
    /** @var OpenIDConnectClientInterface $client */
    $client = $this->pluginManager->createInstance(
      'generic',
      $this->config('openid_connect.settings.generic')->get('settings')
    );

    if (!$client instanceof OpenIDConnectClientInterface) {
      throw new RuntimeException('plugin \'generic\' did not resolve to an OpenIDConnectClientInterface instance');
    }

    return $client;
  }

  /**
   * Prepare the `$_SESSION` variable for the OpenID authentication process.
   */
  private function prepareSession(): void
  {
    $_SESSION['openid_connect_op'] = 'login';
  }
}
