<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_openid;

use Drupal\openid_connect\OpenIDConnect;
use RuntimeException;

/**
 * Class XpertSelectOpenIdConnect.
 *
 * Extends the `\Drupal\openid_connect\OpenIDConnect` class to alter the username generation. This clas forces the
 * username to equal the email address, rather than using one of the other OpenID claims to determine the username.
 */
final class XpertSelectOpenIdConnect extends OpenIDConnect
{
  /**
   * {@inheritdoc}
   */
  public function generateUsername($sub, array $userinfo, $client_name): string
  {
    if (empty($userinfo['email'])) {
      throw new RuntimeException('There is no email to base the generated username on');
    }

    return $userinfo['email'];
  }
}
