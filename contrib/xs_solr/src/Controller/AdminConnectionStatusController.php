<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Controller;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\xs_solr\Solr\SolrClient;
use Drupal\xs_solr\Solr\SolrConfigUtilities;
use Drupal\xs_solr\XsSolr;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminConnectionStatusController.
 *
 * Enables the display of a statusPage showing the status of the Apache Solr connection.
 */
final class AdminConnectionStatusController extends ControllerBase
{
  use SolrConfigUtilities;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SolrClient $client */
    $client = $container->get('xs_solr.client');

    /** @var RendererInterface $renderer */
    $renderer = $container->get('renderer');

    return new self($client, $renderer);
  }

  /**
   * AdminConnectionStatusController constructor.
   *
   * @param SolrClient        $solrClient The client for communicating with Solr
   * @param RendererInterface $renderer   The Drupal template renderer
   */
  public function __construct(private readonly SolrClient $solrClient, private readonly RendererInterface $renderer)
  {
  }

  /**
   * Retrieves all the information necessary for the Solr statusPage and returns
   * it as a Drupal render array.
   *
   * @return array<string, MarkupInterface|string> The render array
   *
   * @throws Exception Thrown on any rendering error
   */
  public function statusPage(): array
  {
    $solrConfig = $this->config(XsSolr::SETTINGS_KEY);
    $endpoint   = $this->getSolrEndpointFromConfig($solrConfig);

    if ($endpoint && $status = $this->solrClient->checkConnection()) {
      $endpoint .= ' (HTTP ' . $status . ')';
    }

    $build                  = [];
    $build['status_header'] = [
      '#markup' => $this->t('Connection details'),
      '#prefix' => '<h2>',
      '#suffix' => '</h2>',
    ];
    $build['solr_status'] = [
      '#theme'  => 'table',
      '#header' => [
        ['colspan' => 2, 'data' => $this->t('Apache Solr - Connection')],
      ],
      '#rows'   => [
        [$this->t('Solr endpoint'), !empty($endpoint)
          ? $endpoint
          : $this->t('Not configured'), ],
        [$this->t('Authentication'), !is_null($this->getAuthenticationFromConfig($solrConfig))
          ? 'BasicAuth'
          : $this->t('None'), ],
      ],
    ];

    return [
      '#type'   => '#markup',
      '#markup' => $this->renderer->render($build),
    ];
  }
}
