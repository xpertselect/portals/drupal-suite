<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;

/**
 * Trait GuzzleErrorUtilities.
 *
 * Provides utilities for interacting with errors coming from a Guzzle HTTP client.
 */
trait GuzzleErrorUtilities
{
  /**
   * Transforms a GuzzleException into a string representation.
   *
   * @param GuzzleException $exception The Exception to transform
   *
   * @return string The string representation
   */
  public function guzzleExceptionToMessage(GuzzleException $exception): string
  {
    if ($exception instanceof RequestException && !is_null($response = $exception->getResponse())) {
      return sprintf('Request: %s; Status: %s; Body: %s',
        $this->psrRequestAsString($exception->getRequest()),
        $response->getStatusCode(),
        $response->getBody()->getContents()
      );
    }

    return $exception->getMessage();
  }

  /**
   * Renders the complete URL of a given PSR request.
   *
   * @param RequestInterface $request The request to transform
   *
   * @return string The complete URL of the request
   */
  private function psrRequestAsString(RequestInterface $request): string
  {
    $psrUri = $request->getUri();
    $port   = $psrUri->getPort() ? ':' . $psrUri->getPort() : '';

    $uri    = sprintf(
      '%s %s://%s%s%s',
      strtoupper($request->getMethod()),
      $psrUri->getScheme(),
      $psrUri->getHost(),
      $port,
      $psrUri->getPath()
    );

    if (strlen($psrUri->getQuery()) > 0) {
      $uri = $uri . '?' . $psrUri->getQuery();
    }

    return $uri;
  }
}
