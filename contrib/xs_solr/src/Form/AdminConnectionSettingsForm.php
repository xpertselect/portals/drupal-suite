<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_general\ManagedSettingsTrait;
use Drupal\xs_solr\XsSolr;

/**
 * Class AdminConnectionSettingsForm.
 *
 * Form for all settings pertaining to connecting to Apache Solr.
 */
final class AdminConnectionSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_solr_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    if ($this->isManaged(XsSolr::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $config = $this->config(XsSolr::SETTINGS_KEY);

    $form['solr_connection'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Apache Solr - Connection'),
      '#description' => $this->t('The settings for connecting to the Solr cluster (or standalone installation).'),
      '#open'        => TRUE,
    ];

    $form['solr_connection']['endpoint'] = [
      '#type'          => 'url',
      '#title'         => $this->t('Solr endpoint'),
      '#default_value' => $config->get('solr_endpoint'),
      '#placeholder'   => 'https://',
    ];

    $form['solr_authentication'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Apache Solr - Authentication'),
      '#description' => $this->t('The BasicAuth authentication settings for communicating with the Solr cluster.'),
      '#open'        => TRUE,
    ];

    $form['solr_authentication']['basic_auth'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable BasicAuth authentication'),
      '#default_value' => $config->get('solr_basic_auth'),
      '#return_value'  => 'checked',
    ];

    $form['solr_authentication']['username'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Username'),
      '#default_value' => $config->get('solr_username'),
      '#states'        => [
        'visible' => [
          ':input[name="basic_auth"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['solr_authentication']['password'] = [
      '#type'          => 'password',
      '#title'         => $this->t('Password'),
      '#default_value' => $config->get('solr_password'),
      '#states'        => [
        'visible' => [
          ':input[name="basic_auth"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(XsSolr::SETTINGS_KEY, TRUE)) {
      return;
    }

    $authEnabled = $form_state->getValue('basic_auth') === 'checked';
    $username    = $authEnabled ? $form_state->getValue('username') : NULL;
    $password    = $authEnabled ? $form_state->getValue('password') : NULL;

    $config = $this->config(XsSolr::SETTINGS_KEY);
    $config->set('solr_endpoint', $form_state->getValue('endpoint'));
    $config->set('solr_basic_auth', $authEnabled);
    $config->set('solr_username', $username);

    if (!$authEnabled || !empty($password)) {
      // Update the password if:
      // - BasicAuth is disabled (set to null)
      // - BasicAuth is enabled and the password is not empty
      $config->set('solr_password', $password);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsSolr::SETTINGS_KEY];
  }
}
