<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_solr\XsSolr;
use GuzzleHttp\RequestOptions as RO;
use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * Class SolrClientFactory.
 *
 * Factory for creating SolrClient instances.
 */
final class SolrClientFactory
{
  use SolrConfigUtilities;

  /**
   * Static entrypoint for creating SolrClient instances. This method internally creates an instance of this factory and
   * delegates the call to the createClient method.
   *
   * @param ClientFactory                 $clientFactory   The HTTP client factory
   * @param LoggerChannelFactoryInterface $loggerFactory   The logger factory
   * @param ConfigFactoryInterface        $configFactory   The Drupal config factory
   * @param EventDispatcherInterface      $eventDispatcher The event dispatcher
   *
   * @return SolrClient The created instance
   *
   * @see SolrClientFactory::createSolrClient()
   */
  public static function create(ClientFactory $clientFactory, LoggerChannelFactoryInterface $loggerFactory,
                                ConfigFactoryInterface $configFactory,
                                EventDispatcherInterface $eventDispatcher): SolrClient
  {
    static $factory = NULL;

    if (is_null($factory)) {
      $factory = new SolrClientFactory($configFactory->get(XsSolr::SETTINGS_KEY));
    }

    return $factory->createSolrClient($clientFactory, $loggerFactory, $eventDispatcher);
  }

  /**
   * SolrClientFactory constructor.
   *
   * @param ImmutableConfig $config The xs_solr configuration
   */
  public function __construct(private readonly ImmutableConfig $config)
  {
  }

  /**
   * Creates a SolrClient instance based on the given factories.
   *
   * @param ClientFactory                 $clientFactory   The HTTP client factory
   * @param LoggerChannelFactoryInterface $loggerFactory   The logger factory
   * @param EventDispatcherInterface      $eventDispatcher The event dispatcher
   *
   * @return SolrClient The created instance
   */
  public function createSolrClient(ClientFactory $clientFactory, LoggerChannelFactoryInterface $loggerFactory,
                                   EventDispatcherInterface $eventDispatcher): SolrClient
  {
    return new SolrClient(
      $clientFactory->fromOptions($this->getGuzzleClientOptions()),
      $loggerFactory->get(XsSolr::LOG_CHANNEL),
      $eventDispatcher
    );
  }

  /**
   * Determine the array of options to give to the Guzzle client which will be used for communicating with a Apache Solr
   * instance.
   *
   * @return array<string, mixed> The Guzzle client options
   */
  public function getGuzzleClientOptions(): array
  {
    return [
      'base_uri'  => $this->getSolrEndpointFromConfig($this->config),
      RO::AUTH    => $this->getAuthenticationFromConfig($this->config),
      RO::HEADERS => ['User-Agent' => 'XpertSelect Portals (xpertselect-portals/xsp_drupal_suite - xs_solr)'],
    ];
  }
}
