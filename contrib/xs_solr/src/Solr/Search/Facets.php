<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class Facets.
 *
 * Represents the facets included in a Solr query response.
 */
final class Facets
{
  /**
   * The facet_fields response key.
   */
  private const FACET_FIELDS = 'facet_fields';

  /**
   * The facet_intervals response key.
   */
  private const FACET_INTERVALS = 'facet_intervals';

  /**
   * Facets constructor.
   *
   * @param array<string, array<string, mixed>> $facetData The facet data
   */
  public function __construct(private readonly array $facetData)
  {
  }

  /**
   * Return the raw facet data.
   *
   * @return array<string, array<string, mixed>> The raw facet data
   */
  public function getRawData(): array
  {
    return $this->facetData;
  }

  /**
   * Check if the facet data contains `facet_fields`.
   *
   * @return bool Whether 'facet_fields' are present
   */
  public function hasFacetFields(): bool
  {
    return array_key_exists(self::FACET_FIELDS, $this->facetData);
  }

  /**
   * Retrieve the facet fields part of the facet data.
   *
   * @return array<string, array<string, int>> The facet fields
   */
  public function getFacetFields(): array
  {
    if (!$this->hasFacetFields()) {
      return [];
    }

    return $this->facetData[self::FACET_FIELDS];
  }

  /**
   * Retrieve the names of the fields for which facet entries are present.
   *
   * @return array<int, string> The field names
   */
  public function getFacetFieldNames(): array
  {
    return array_keys($this->getFacetFields());
  }

  /**
   * Check if there is facet data for the given field.
   *
   * @param string $field The field to check
   *
   * @return bool Whether there is facet data for the field
   */
  public function hasFacetField(string $field): bool
  {
    if (!$this->hasFacetFields()) {
      return FALSE;
    }

    return array_key_exists($field, $this->getFacetFields());
  }

  /**
   * Retrieve the entries for the facet for the given field.
   *
   * @param string $field The field to check
   *
   * @return array<string, int> The facet entries for the field
   */
  public function getFacetField(string $field): array
  {
    if (!$this->hasFacetField($field)) {
      return [];
    }

    return $this->getFacetFields()[$field];
  }

  /**
   * Check if the facet data contains `facet_intervals`.
   *
   * @return bool Whether 'facet_intervals' are present
   */
  public function hasFacetIntervals(): bool
  {
    return array_key_exists(self::FACET_INTERVALS, $this->facetData);
  }

  /**
   * Retrieve the facet intervals part of the facet data.
   *
   * @return array<string, array<string, int>> The facet intervals
   */
  public function getFacetIntervals(): array
  {
    if (!$this->hasFacetIntervals()) {
      return [];
    }

    return $this->facetData[self::FACET_INTERVALS];
  }

  /**
   * Retrieve the names of the fields for which facet interval entries are present.
   *
   * @return array<int, string> The field names
   */
  public function getFacetIntervalNames(): array
  {
    return array_keys($this->getFacetIntervals());
  }

  /**
   * Check if there is facet interval data for the given field.
   *
   * @param string $field The field to check
   *
   * @return bool Whether there is facet data for the field
   */
  public function hasFacetInterval(string $field): bool
  {
    if (!$this->hasFacetIntervals()) {
      return FALSE;
    }

    return array_key_exists($field, $this->getFacetIntervals());
  }

  /**
   * Retrieve the entries for the interval facet for the given field.
   *
   * @param string $field The field to check
   *
   * @return array<string, int> The facet entries for the field
   */
  public function getFacetInterval(string $field): array
  {
    if (!$this->hasFacetInterval($field)) {
      return [];
    }

    return $this->getFacetIntervals()[$field];
  }
}
