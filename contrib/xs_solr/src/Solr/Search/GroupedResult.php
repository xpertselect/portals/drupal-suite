<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class GroupedResult.
 *
 * Represents a grouped result set of a Solr response.
 */
final class GroupedResult
{
  /**
   * GroupedResult constructor.
   *
   * @param array<string, array<string, array<string, mixed>>> $groupedResultData The grouped result data
   */
  public function __construct(private readonly array $groupedResultData)
  {
  }

  /**
   * Return the raw grouped result data.
   *
   * @return array<string, array<string, array<string, mixed>>> The raw grouped data
   */
  public function getRawData(): array
  {
    return $this->groupedResultData;
  }

  /**
   * Retrieve the grouped result sets for the given field.
   *
   * @param string $fieldName The field name used for the grouping
   *
   * @return array<int, Group> The result groups
   */
  public function getGroupsFor(string $fieldName): array
  {
    if (!array_key_exists($fieldName, $this->groupedResultData)) {
      return [];
    }

    if (!array_key_exists('groups', $this->groupedResultData[$fieldName])) {
      return [];
    }

    return array_values(array_map(function($groupData) {
      return new Group($groupData);
    }, $this->groupedResultData[$fieldName]['groups']));
  }
}
