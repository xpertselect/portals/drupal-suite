<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class Group.
 *
 * Represents an individual group of a grouped query response.
 */
final class Group
{
  /**
   * Group constructor.
   *
   * @param array{groupValue: string, doclist: array<string, mixed>} $groupData The group data
   */
  public function __construct(private readonly array $groupData)
  {
  }

  /**
   * Return the raw group data.
   *
   * @return array{groupValue: string, doclist: array<string, mixed>} The raw data
   */
  public function getRawData(): array
  {
    return $this->groupData;
  }

  /**
   * Return the value of the `groupValue` key or an empty string if the key is missing or empty.
   *
   * @return string The group value
   */
  public function getGroupValue(): string
  {
    return $this->groupData['groupValue'] ?? '';
  }

  /**
   * Return the result set contained within this response group or null if there is none.
   *
   * @return null|ResultSet The contained result set or null if there is none
   */
  public function getDocList(): ?ResultSet
  {
    if (!array_key_exists('doclist', $this->groupData)) {
      return NULL;
    }

    return new ResultSet($this->groupData['doclist']);
  }
}
