<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class QueryResponse.
 *
 * Represents the response from Solr for a query against a SelectComponent.
 */
final class QueryResponse
{
  /**
   * The responseHeader key of a Solr response.
   */
  private const RESPONSE_HEADER = 'responseHeader';

  /**
   * The response key of a Solr response.
   */
  private const RESPONSE = 'response';

  /**
   * The facets key of a Solr response.
   */
  private const FACETS = 'facet_counts';

  /**
   * The spellcheck key of a Solr response.
   */
  private const SPELLCHECK = 'spellcheck';

  /**
   * The highlighting key of a Solr response.
   */
  private const HIGHLIGHTING = 'highlighting';

  /**
   * The grouped key of a Solr response.
   */
  private const GROUPED = 'grouped';

  /**
   * The suggest key of a Solr response.
   */
  private const SUGGEST = 'suggest';

  /**
   * QueryResponse constructor.
   *
   * @param array<string, array<string, mixed>> $response The response from Solr
   */
  public function __construct(private readonly array $response = [])
  {
  }

  /**
   * Return the raw response from Solr.
   *
   * @return array<string, array<string, mixed>> The raw (but JSON decoded) Solr response
   */
  public function getRawResponse(): array
  {
    return $this->response;
  }

  public function hasResponseHeader(): bool
  {
    return $this->hasKey(self::RESPONSE_HEADER);
  }

  /**
   * Check if the response contains a result set.
   *
   * @return bool Whether a result set is present
   */
  public function hasResultSet(): bool
  {
    return $this->hasKey(self::RESPONSE);
  }

  /**
   * Check if the response contains facet data.
   *
   * @return bool Whether facet data is present
   */
  public function hasFacets(): bool
  {
    return $this->hasKey(self::FACETS);
  }

  /**
   * Check if the response contains spellcheck data.
   *
   * @return bool Whether spellcheck data is present
   */
  public function hasSpellcheck(): bool
  {
    return $this->hasKey(self::SPELLCHECK);
  }

  /**
   * Check if the response contains highlighting data.
   *
   * @return bool Whether highlighting data is present
   */
  public function hasHighlighting(): bool
  {
    return $this->hasKey(self::HIGHLIGHTING);
  }

  /**
   * Check if the response contains grouped data.
   *
   * @return bool Whether grouped data is present
   */
  public function hasGroupedResults(): bool
  {
    return $this->hasKey(self::GROUPED);
  }

  /**
   * Check if the response contains suggest data.
   *
   * @return bool Whether suggest data is present
   */
  public function hasSuggestResults(): bool
  {
    return $this->hasKey(self::SUGGEST);
  }

  /**
   * Retrieve the responseHeader component of a Solr response, if it is present.
   *
   * @return null|array<string, mixed> The responseHeader component, or null if it is missing
   */
  public function getResponseHeader(): ?array
  {
    if (!$this->hasResponseHeader()) {
      return NULL;
    }

    return $this->response[self::RESPONSE_HEADER];
  }

  /**
   * Retrieve the result set of a Solr query response.
   *
   * @return null|ResultSet The result set or null if there is none
   */
  public function getResultSet(): ?ResultSet
  {
    if (!$this->hasResultSet()) {
      return NULL;
    }

    return new ResultSet($this->response[self::RESPONSE]);
  }

  /**
   * Retrieve the facets of a Solr query response.
   *
   * @return null|Facets The facets or null if there are none
   */
  public function getFacets(): ?Facets
  {
    if (!$this->hasFacets()) {
      return NULL;
    }

    return new Facets($this->response[self::FACETS]);
  }

  /**
   * Retrieve the spellcheck component of a Solr query response.
   *
   * @return null|Spellcheck The spellcheck component or null if there is none
   */
  public function getSpellcheck(): ?Spellcheck
  {
    if (!$this->hasSpellcheck()) {
      return NULL;
    }

    return new Spellcheck($this->response[self::SPELLCHECK]);
  }

  /**
   * Retrieve the highlighting component of a Solr query response.
   *
   * @return null|Highlighting The highlighting component or null if there is none
   */
  public function getHighlighting(): ?Highlighting
  {
    if (!$this->hasHighlighting()) {
      return NULL;
    }

    return new Highlighting($this->response[self::HIGHLIGHTING]);
  }

  /**
   * Retrieve the grouped result set component of a Solr query response.
   *
   * @return null|GroupedResult The grouped result set component or null if there is none
   */
  public function getGroupedResults(): ?GroupedResult
  {
    if (!$this->hasGroupedResults()) {
      return NULL;
    }

    return new GroupedResult($this->response[self::GROUPED]);
  }

  /**
   * Retrieve the suggest result set component of a Solr query response.
   *
   * @return null|SuggestResult The suggest result set component or null if there is none
   */
  public function getSuggestResults(): ?SuggestResult
  {
    if (!$this->hasSuggestResults()) {
      return NULL;
    }

    return new SuggestResult($this->response[self::SUGGEST]);
  }

  /**
   * Check if the Solr response has the given key.
   *
   * @param string $key The key to check
   *
   * @return bool Whether the key is present
   */
  private function hasKey(string $key): bool
  {
    return array_key_exists($key, $this->response);
  }
}
