<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class ResultSet.
 *
 * Represents a result set for a response given from a Solr SelectHandler endpoint.
 */
final class ResultSet
{
  /**
   * The documents contained within the result set.
   *
   * @var null|array<int, Document>
   */
  private ?array $documents;

  /**
   * ResultSet constructor.
   *
   * @param array<string, array<string, mixed>> $response The result set received from Solr
   */
  public function __construct(private readonly array $response = [])
  {
    $this->documents = NULL;
  }

  /**
   * Return the raw result set.
   *
   * @return array<string, array<string, mixed>> The raw (but JSON decoded) result set
   */
  public function getRawResponse(): array
  {
    return $this->response;
  }

  /**
   * Returns the numFound of the response. Will default to 0 if the response has no numFound data.
   *
   * @return int The numFound of the response
   */
  public function numFound(): int
  {
    return intval($this->response['numFound'] ?? 0);
  }

  /**
   * Returns the starting point of the result set. Will default to 0 if the response has no starting point data.
   *
   * @return int The starting point of the result set
   */
  public function start(): int
  {
    return intval($this->response['start'] ?? 0);
  }

  /**
   * Return the documents of the result set. Will default to an empty array if the response contains no document data.
   *
   * @return array<int, Document> The documents of the result set
   */
  public function documents(): array
  {
    if (is_null($this->documents)) {
      $this->documents = array_values(array_map(function(array $documentData): Document {
        return new Document($documentData);
      }, $this->response['docs'] ?? []));
    }

    return $this->documents;
  }
}
