<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class Spellcheck.
 *
 * Represents the spellcheck component for a response given from a Solr SelectHandler endpoint.
 */
final class Spellcheck
{
  /**
   * Spellcheck constructor.
   *
   * @param array<string, mixed> $spellcheckData The spellcheck data from the Solr response
   */
  public function __construct(private readonly array $spellcheckData = [])
  {
  }

  /**
   * Return the raw spellcheck data.
   *
   * @return array<string, mixed> The spellcheck data
   */
  public function getRawData(): array
  {
    return $this->spellcheckData;
  }

  /**
   * Return the spellcheck suggestions.
   *
   * @return array<string, mixed> The suggestions from Solr
   */
  public function getSuggestions(): array
  {
    return array_key_exists('suggestions', $this->spellcheckData)
      ? $this->spellcheckData['suggestions']
      : [];
  }

  /**
   * Return the spellcheck collations. A collation is a suggestion that is a combination of separate suggestions.
   *
   * @return array<string, string> The collations
   */
  public function getCollations(): array
  {
    return array_key_exists('collations', $this->spellcheckData)
      ? $this->spellcheckData['collations']
      : [];
  }
}
