<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

use DateTime;
use Drupal\xs_solr\XsSolr;

/**
 * Class Document.
 *
 * Represent an individual document contained within a Solr result set.
 */
final class Document
{
  /**
   * Document constructor.
   *
   * @param array<string, mixed> $document A document array from a Solr response
   */
  public function __construct(private readonly array $document)
  {
  }

  /**
   * Check if the Document has a given field.
   *
   * @param string $field The field to check
   *
   * @return bool Whether the field is present
   */
  public function hasField(string $field): bool
  {
    return array_key_exists($field, $this->document);
  }

  /**
   * Return the names of all the fields of this Document.
   *
   * @return array<int, string> The field names
   */
  public function getFieldNames(): array
  {
    return array_keys($this->document);
  }

  /**
   * Return the contents of a field or NULL if the Document does not contain the field.
   *
   * @param string $field The field to retrieve
   *
   * @return mixed The value of the field
   */
  public function getField(string $field): mixed
  {
    if (!$this->hasField($field)) {
      return NULL;
    }

    return $this->document[$field];
  }

  /**
   * Retrieve a field containing an array value.
   *
   * @param string $field The field to retrieve
   *
   * @return null|array<int, mixed> The array value or null if the field is missing or is not an array
   */
  public function getArrayField(string $field): ?array
  {
    if (!$this->hasField($field)) {
      return NULL;
    }

    return is_array($this->document[$field]) ? $this->document[$field] : NULL;
  }

  /**
   * Retrieve a field containing a datetime value.
   *
   * @param string $field The field to retrieve
   *
   * @return null|DateTime The datetime value or null if the field is missing or is not a datetime
   */
  public function getDateTimeField(string $field): ?DateTime
  {
    $value = $this->getStringField($field);

    if (is_null($value)) {
      return NULL;
    }

    return DateTime::createFromFormat(XsSolr::DATETIME_FORMAT, $value) ?: NULL;
  }

  /**
   * Retrieve a field containing a float value.
   *
   * @param string $field The field to retrieve
   *
   * @return null|float The float value or null if the field is missing or not a float
   */
  public function getFloatField(string $field): ?float
  {
    if (!$this->hasField($field)) {
      return NULL;
    }

    return is_float($this->document[$field]) ? $this->document[$field] : NULL;
  }

  /**
   * Retrieve a field containing an integer value.
   *
   * @param string $field The field to retrieve
   *
   * @return null|int The integer value or null if the field is missing or not a integer
   */
  public function getIntField(string $field): ?int
  {
    if (!$this->hasField($field)) {
      return NULL;
    }

    return is_int($this->document[$field]) ? $this->document[$field] : NULL;
  }

  /**
   * Retrieve a field containing a string value.
   *
   * @param string $field The field to retrieve
   *
   * @return null|string The string value or null if the field is missing or not a string
   */
  public function getStringField(string $field): ?string
  {
    if (!$this->hasField($field)) {
      return NULL;
    }

    return is_string($this->document[$field]) ? $this->document[$field] : NULL;
  }

  /**
   * Retrieve a field containing a boolean value.
   *
   * @param string $field The field to retrieve
   *
   * @return null|bool The boolean value or null if the field is missing or not a boolean
   */
  public function getBoolField(string $field): ?bool
  {
    if (!$this->hasField($field)) {
      return NULL;
    }

    return is_bool($this->document[$field]) ? $this->document[$field] : NULL;
  }

  /**
   * Return the raw document data.
   *
   * @return array<string, mixed> The raw document
   */
  public function getRawDocument(): array
  {
    return $this->document;
  }
}
