<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class Suggestion.
 *
 * Represents an individual suggestion of a suggest query response.
 */
final class Suggestion
{
  /**
   * Suggestion constructor.
   *
   * @param array{
   *   term: string,
   *   weight: int,
   *   payload: string
   * } $suggestionData The suggestion data
   */
  public function __construct(private readonly array $suggestionData)
  {
  }

  /**
   * Return the raw suggestion.
   *
   * @return array{
   *   term: string,
   *   weight: int,
   *   payload: string
   * } The raw data
   */
  public function getRawData(): array
  {
    return $this->suggestionData;
  }

  /**
   * Returns the suggested term.
   *
   * @return string The suggested term
   */
  public function getTerm(): string
  {
    return $this->suggestionData['term'] ?? '';
  }

  /**
   * Returns the weight of the suggestion.
   *
   * @return int The weight of the suggestion
   */
  public function getWeight(): int
  {
    return $this->suggestionData['weight'] ?? 0;
  }

  /**
   * Returns the payload of the suggestion.
   *
   * @return string The payload of the suggestion
   */
  public function getPayload(): string
  {
    return $this->suggestionData['payload'] ?? '';
  }
}
