<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

/**
 * Class SuggestResult.
 *
 * Represents a suggest result set of a Solr response.
 */
final class SuggestResult
{
  /**
   * SuggestResult constructor.
   *
   * @param array<string, array<string, array<string, mixed>>> $suggestResultData The suggest result data
   */
  public function __construct(private readonly array $suggestResultData)
  {
  }

  /**
   * Return the raw suggest result data.
   *
   * @return array<string, array<string, array<string, mixed>>> The raw suggest data
   */
  public function getRawData(): array
  {
    return $this->suggestResultData;
  }

  /**
   * Return the suggest results for the given dictionary.
   *
   * @param string $dictionary The dictionary to get suggestions from
   *
   * @return Suggestion[] The suggest results
   */
  public function getSuggestionFrom(string $dictionary): array
  {
    if (!$this->hasDictionary($dictionary)) {
      return [];
    }

    $suggestions = array_values($this->suggestResultData[$dictionary])[0]['suggestions'] ?? [];

    return array_map(function($suggestion) {
      return new Suggestion($suggestion);
    }, $suggestions);
  }

  /**
   * Returns whether the suggest result data has the given dictionary.
   *
   * @param string $dictionary The dictionary to check for
   *
   * @return bool Whether the suggest result data has the given dictionary
   */
  public function hasDictionary(string $dictionary): bool
  {
    return array_key_exists($dictionary, $this->suggestResultData);
  }
}
