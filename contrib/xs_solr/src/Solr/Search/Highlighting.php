<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr\Search;

use RuntimeException;

/**
 * Class Highlighting.
 *
 * Represents the highlighting component of a Solr response.
 */
final class Highlighting
{
  /**
   * Highlighting constructor.
   *
   * @param array<string, array<string, array<int, string>>> $highlightingData
   */
  public function __construct(private readonly array $highlightingData = [])
  {
  }

  /**
   * Return the raw highlighting data.
   *
   * @return array<string, array<string, array<int, string>>> The raw highlighting data
   */
  public function getRawData(): array
  {
    return $this->highlightingData;
  }

  /**
   * Check if there is highlighting data for the given document and field combination.
   *
   * @param string $documentId The ID of the document to check
   * @param string $field      The field that should have highlighting
   *
   * @return bool Whether there is highlighting data
   */
  public function hasHighlightingFor(string $documentId, string $field): bool
  {
    if (!array_key_exists($documentId, $this->highlightingData)) {
      return FALSE;
    }

    if (!array_key_exists($field, $this->highlightingData[$documentId])) {
      return FALSE;
    }

    return !empty($this->highlightingData[$documentId][$field]);
  }

  /**
   * Get all the highlighting entries for a given document and field combination. Will return an empty array if there
   * are no entries.
   *
   * @param string $documentId The ID of the document to check
   * @param string $field      The field that should have highlighting
   *
   * @return array<int, string> The highlighting entries
   */
  public function getHighlightingFor(string $documentId, string $field): array
  {
    if (!$this->hasHighlightingFor($documentId, $field)) {
      return [];
    }

    return $this->highlightingData[$documentId][$field];
  }

  /**
   * Retrieve the first highlighting entry for a given document and field combination.
   *
   * @param string $documentId The ID of the document to check
   * @param string $field      The field that should have highlighting
   *
   * @return string The first highlighting entry
   */
  public function getFirstHighlightingFor(string $documentId, string $field): string
  {
    $entries = $this->getHighlightingFor($documentId, $field);

    if (count($entries) < 1) {
      throw new RuntimeException(sprintf('There is no highlighting for document "%s", field "%s"',
        $documentId,
        $field
      ));
    }

    return $entries[0];
  }
}
