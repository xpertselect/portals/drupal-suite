<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr;

use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * Class SolrClient.
 *
 * Service for interacting with the HTTP API of an Apache Solr instance.
 */
final class SolrClient
{
  /**
   * The collection instances requested thus far.
   *
   * @var array<string, Collection>
   */
  private array $collections;

  /**
   * SolrClient constructor.
   *
   * @param ClientInterface          $httpClient      The client for making HTTP requests
   * @param LoggerChannelInterface   $logger          The logging implementation
   * @param EventDispatcherInterface $eventDispatcher The event dispatcher
   */
  public function __construct(private readonly ClientInterface $httpClient,
                              private readonly LoggerChannelInterface $logger,
                              private readonly EventDispatcherInterface $eventDispatcher)
  {
    $this->collections = [];
  }

  /**
   * Retrieve a Collection instance for the given name. All subsequent requests for the same named Collection
   * will return the same instance.
   *
   * @param string $name The name of the collection (or core) in Apache Solr
   *
   * @return Collection The Collection instance representing the Solr collection
   */
  public function collection(string $name): Collection
  {
    if (!array_key_exists($name, $this->collections)) {
      $this->collections[$name] = $this->makeCollection($name);
    }

    return $this->collections[$name];
  }

  /**
   * Determine the connection status by sending an HTTP GET request to the Solr endpoint.
   *
   * @return null|int The HTTP status, or NULL on any error
   */
  public function checkConnection(): ?int
  {
    try {
      $response = $this->httpClient->request('GET', '', []);

      return $response->getStatusCode();
    } catch (GuzzleException $e) {
      if ($e instanceof RequestException && !is_null($response = $e->getResponse())) {
        return $response->getStatusCode();
      }

      $this->logger->error(sprintf('Failed to check the connection with Apache Solr; %s', $e->getMessage()));

      return NULL;
    }
  }

  /**
   * Makes a Collection instance for the given Solr collection (or core).
   *
   * @param string $name The name of the Solr collection (or core)
   *
   * @return Collection The created instance
   */
  private function makeCollection(string $name): Collection
  {
    return new Collection($name, $this->httpClient, $this->logger, $this->eventDispatcher);
  }
}
