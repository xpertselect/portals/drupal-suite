<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr;

use Drupal\xs_solr\Solr\Search\Document;
use Drupal\xs_solr\Solr\Search\Highlighting;

trait SolrHighlightedFieldTrait
{
  /**
   * Retrieve a snippet from the search response with highlighting.
   *
   * @param Document          $document     The search result document
   * @param string            $fieldName    The field name to get the snippet from
   * @param string            $indexId      The index of the search result
   * @param null|Highlighting $highlighting A highlighting object for retrieving highlighting
   *
   * @return string The snippet with highlighting
   */
  public function getSolrHighlightedSnippetFromField(Document $document, string $fieldName, string $indexId, ?Highlighting $highlighting): string
  {
    if (isset($highlighting) && $highlighting->hasHighlightingFor($indexId, $fieldName)) {
      return $highlighting->getFirstHighlightingFor($indexId, $fieldName);
    }
    $snippet = $document->getStringField($fieldName) ?? '';

    return strlen($snippet) >= 400 ? substr($snippet, 0, 400) . '&hellip;' : $snippet;
  }
}
