<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_solr\Event\IndexUpdated;
use Drupal\xs_solr\GuzzleErrorUtilities;
use Drupal\xs_solr\Solr\Search\QueryResponse;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Query;
use GuzzleHttp\RequestOptions as RO;
use JsonException;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Collection.
 *
 * Represent a collection (or core) in Apache Solr.
 */
final class Collection
{
  use GuzzleErrorUtilities;

  /**
   * Default Solr query parameters to include in each request.
   */
  private const DEFAULT_PARAMETERS = [
    'wt'         => 'json',
    'omitHeader' => TRUE,
    'json.nl'    => 'map',
  ];

  /**
   * The max amount of chars the params of a get request can have before switching to a post request.
   */
  private const MAX_GET_REQUEST_LENGTH = 4096;

  /**
   * Collection constructor.
   *
   * @param string                   $name            The name of the Solr collection (or core)
   * @param ClientInterface          $httpClient      The client for making HTTP requests
   * @param LoggerChannelInterface   $logger          The logging implementation
   * @param EventDispatcherInterface $eventDispatcher The event dispatcher to dispatch updated index events
   */
  public function __construct(private readonly string $name, private readonly ClientInterface $httpClient,
                              private readonly LoggerChannelInterface $logger,
                              private readonly EventDispatcherInterface $eventDispatcher)
  {
  }

  /**
   * Return the name of the Solr collection (or core).
   *
   * @return string The collection name
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * Send a query to Solr and return the response. All parameters are optional and have sensible defaults.
   *
   * @param string               $handler              The Solr requestHandler that should be given the query
   * @param string               $query                The `q` parameter of the query
   * @param array<int, string>   $filters              Any filters (`fq`) that should be included
   * @param int                  $start                From which index of the result-set to return results
   * @param int                  $rows                 The amount of documents to return
   * @param array<string, mixed> $additionalParameters Any additional parameters to include in the request
   *
   * @return null|QueryResponse The response from Solr
   */
  public function query(string $handler = 'select', string $query = '*:*', array $filters = [], int $start = 0,
                        int $rows = 10, array $additionalParameters = []): ?QueryResponse
  {
    try {
      $params         = $this->createParametersFromArguments($query, $filters, $start, $rows, $additionalParameters);
      $requestOptions = $this->getRequestOptions($params);
      $requestMethod  = $this->getRequestMethod($requestOptions);

      $response = $this->httpClient->request($requestMethod, sprintf('%s/%s', $this->name, $handler), $requestOptions);

      return new QueryResponse($this->responseAsJson($response));
    } catch (GuzzleException $e) {
      $this->logger->error(sprintf(
        'Failed to query Apache Solr; %s',
        $this->guzzleExceptionToMessage($e)
      ));

      return NULL;
    } catch (JsonException $e) {
      $this->logger->error(sprintf('Invalid JSON received from Apache Solr; %s', $e->getMessage()));

      return NULL;
    }
  }

  /**
   * Send a suggest query to Solr and return the response.
   *
   * @param string $query      The `suggest.q` parameter of the suggest query
   * @param string $handler    The Solr requestHandler that should be given the query
   * @param string $dictionary The Solr dictionary component
   * @param int    $count      The maximum number of suggestions to return
   *
   * @return null|QueryResponse The response from Solr
   */
  public function suggest(string $query, string $handler, string $dictionary, int $count = 5): ?QueryResponse
  {
    try {
      $params = $this->convertBooleansToStrings(array_merge([
        'suggest'            => TRUE,
        'suggest.q'          => $query,
        'suggest.count'      => $count,
        'suggest.dictionary' => $dictionary,
      ], self::DEFAULT_PARAMETERS));

      $requestOptions = $this->getRequestOptions($params);
      $requestMethod  = $this->getRequestMethod($requestOptions);
      $response       = $this->httpClient->request($requestMethod, sprintf('%s/%s', $this->name, $handler), $requestOptions);

      return new QueryResponse($this->responseAsJson($response));
    } catch (GuzzleException $e) {
      $this->logger->error(sprintf(
        'Failed to get suggestions from Apache Solr; %s',
        $this->guzzleExceptionToMessage($e)
      ));

      return NULL;
    } catch (JsonException $e) {
      $this->logger->error(sprintf('Invalid JSON received from Apache Solr; %s', $e->getMessage()));

      return NULL;
    }
  }

  /**
   * Sends one or more documents to Apache Solr to update the index of the collection.
   *
   * @param array<int|string, array<string, mixed>> $documents The documents (one or more) to send to Apache Solr
   * @param bool                                    $commit    Whether to commit the changes to the Solr index
   *
   * @return bool Whether Apache Solr accepted all the documents
   */
  public function update(array $documents, bool $commit = TRUE): bool
  {
    try {
      $response = $this->httpClient->request('POST', $this->name . '/update', [
        RO::QUERY => Query::build($this->convertBooleansToStrings(['commit' => $commit])),
        RO::JSON  => $documents,
      ]);

      $updateAccepted = 200 === $response->getStatusCode();

      if ($updateAccepted && $commit) {
        $this->eventDispatcher->dispatch(new IndexUpdated($this->name));
      }

      return $updateAccepted;
    } catch (GuzzleException $e) {
      $this->logger->error(sprintf(
        'Failed to send update instructions to Apache Solr; %s',
        $this->guzzleExceptionToMessage($e)
      ));

      return FALSE;
    }
  }

  /**
   * Creates an array of parameters based on the query arguments.
   *
   * @param string               $query                The `q` parameter of the query
   * @param array<int, string>   $filters              Any filters (`fq`) that should be included
   * @param int                  $start                From which index of the result-set to return results
   * @param int                  $rows                 The amount of documents to return
   * @param array<string, mixed> $additionalParameters Any additional parameters to include in the request
   *
   * @return array<string, mixed> The HTTP GET parameters
   */
  public function createParametersFromArguments(string $query, array $filters, int $start, int $rows,
                                                array $additionalParameters = []): array
  {
    if (empty($query)) {
      $query = '*:*';
    }

    $parameters = ['q' => $query, 'start' => $start, 'rows' => $rows];
    $filters    = array_filter($filters, function($element) {
      return !empty($element);
    });

    if (count($filters) > 0) {
      $parameters['fq'] = implode(' AND ', $filters);
    }

    return $this->convertBooleansToStrings(array_merge(
      $additionalParameters,
      self::DEFAULT_PARAMETERS,
      $parameters
    ));
  }

  /**
   * Return the JSON decoded body from a given response object.
   *
   * @param ResponseInterface $response The response to interpret as JSON
   *
   * @return mixed The decoded JSON
   *
   * @throws JsonException Thrown on any error while decoding the JSON
   */
  private function responseAsJson(ResponseInterface $response): mixed
  {
    return json_decode(
      $response->getBody()->getContents(),
      TRUE,
      flags: JSON_THROW_ON_ERROR
    );
  }

  /**
   * Convert boolean HTTP GET parameters to their string representation of 'true' or 'false, rather than the default PHP
   * behaviour of '1' and '0'.
   *
   * @param array<string, mixed> $elements The elements to check
   *
   * @return array<string, mixed> The original elements with the boolean elements as strings
   */
  private function convertBooleansToStrings(array $elements): array
  {
    return array_map(function($element) {
      if (is_bool($element)) {
        $element = $element ? 'true' : 'false';
      }

      return $element;
    }, $elements);
  }

  /**
   * Format the request options for a solr request.
   *
   * @param array<string, mixed> $params The solr search params to format for the request
   *
   * @return array<string, string|array<string, mixed>> The formatted request options
   */
  private function getRequestOptions(array $params): array
  {
    $queryParams = Query::build($params);

    if (strlen($queryParams) > self::MAX_GET_REQUEST_LENGTH) {
      return [
        RO::JSON => [
          'params' => $params,
        ],
      ];
    }

    return [
      RO::QUERY => $queryParams,
    ];
  }

  /**
   * Get the method of the solr request based on the data.
   *
   * @param array<string, string|array<string, mixed>> $requestOptions The request options from which the method should be based
   *
   * @return string The method as a string
   */
  private function getRequestMethod(array $requestOptions): string
  {
    return isset($requestOptions[RO::QUERY]) ? 'GET' : 'POST';
  }
}
