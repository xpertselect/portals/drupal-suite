<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Solr;

use Drupal\Core\Config\Config;

/**
 * Trait SolrConfigUtilities.
 *
 * Provides utilities for interacting with the Solr configuration stored in Drupal.
 */
trait SolrConfigUtilities
{
  /**
   * Determine the HTTP endpoint of Apache Solr.
   *
   * @param Config $config The Solr connection configuration
   *
   * @return string The Solr endpoint
   */
  public function getSolrEndpointFromConfig(Config $config): string
  {
    /** @var string $endpoint */
    $endpoint = $config->get('solr_endpoint') ?? '';

    if (!str_ends_with($endpoint, '/')) {
      $endpoint = $endpoint . '/';
    }

    return $endpoint;
  }

  /**
   * Determine the value of the `RequestOptions::AUTH` Guzzle option.
   *
   * @param Config $config The Solr connection configuration
   *
   * @return null|string[] The determined value
   */
  public function getAuthenticationFromConfig(Config $config): ?array
  {
    if (!$config->get('solr_basic_auth')) {
      return NULL;
    }

    $username = $config->get('solr_username');
    $password = $config->get('solr_password');

    if (empty($username) || !is_string($username)) {
      return NULL;
    }

    if (empty($password) || !is_string($password)) {
      return NULL;
    }

    return [$username, $password];
  }
}
