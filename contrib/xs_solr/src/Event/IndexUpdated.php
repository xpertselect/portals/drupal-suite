<?php

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr\Event;

use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class IndexUpdated.
 *
 * An event that should be fired when the index of a Solr collection was updated.
 */
final class IndexUpdated extends StoppableEvent
{
  /**
   * IndexUpdated constructor.
   *
   * @param string $name The name of the collection that was updated
   */
  public function __construct(private readonly string $name)
  {
  }

  /**
   * Gets the name of the collection that was updated.
   *
   * @return string The name of the collection that was updated
   */
  public function getName(): string
  {
    return $this->name;
  }
}
