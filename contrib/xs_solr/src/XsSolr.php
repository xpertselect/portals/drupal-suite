<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_solr;

/**
 * Class XsSolr.
 *
 * Simple PHP class holding various constants used by the `xs_solr` Drupal module.
 */
final class XsSolr
{
  /**
   * The key identifying the `xs_solr` settings in Drupal.
   */
  public const SETTINGS_KEY = 'xs_solr.settings';

  /**
   * The log channel that should be used by this Drupal module.
   */
  public const LOG_CHANNEL = 'xs_solr';

  /**
   * Represents the datetime format used by Solr for storing datetime fields.
   */
  public const DATETIME_FORMAT = 'Y-m-d\TH:i:s\Z';
}
