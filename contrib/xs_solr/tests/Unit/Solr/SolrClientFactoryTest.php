<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_solr\Solr\SolrClient;
use Drupal\xs_solr\Solr\SolrClientFactory;
use Drupal\xs_solr\XsSolr;
use GuzzleHttp\Client;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class SolrClientFactoryTest extends TestCase
{
  public static function baseUriDataset(): array
  {
    return [
      [''],
      ['https://example.com'],
      ['https://example.com:8983'],
      ['https://example.com/'],
      ['https://example.com:8983/'],
      ['https://example.com/foo'],
      ['https://example.com:8983/foo'],
      ['https://example.com/foo/'],
      ['https://example.com:8983/foo/'],
    ];
  }

  public static function nullAuthDataset(): array
  {
    return [
      [NULL, NULL, NULL],
      ['', '', ''],
      [FALSE, FALSE, FALSE],
      [NULL, 'foo', 'bar'],
      [FALSE, 'foo', 'bar'],
      [TRUE, NULL, NULL],
      [TRUE, '', ''],
      [TRUE, 'foo', ''],
      [TRUE, '', 'bar'],
    ];
  }

  public function testCreate(): void
  {
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('solr_endpoint')
          ->andReturn('https://example.com');

        $mock->shouldReceive('get')
          ->with('solr_basic_auth')
          ->andReturnFalse();
      });

      $mock->shouldReceive('get')
        ->with(XsSolr::SETTINGS_KEY)
        ->andReturn($config);
    });

    $clientFactory = M::mock(ClientFactory::class, function(MI $mock) {
      $mock->shouldReceive('fromOptions')
        ->andReturn(M::mock(Client::class));
    });

    $loggerFactory = M::mock(LoggerChannelFactoryInterface::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with(XsSolr::LOG_CHANNEL)
        ->andReturn(M::mock(LoggerChannelInterface::class));
    });

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    Assert::assertInstanceOf(
      SolrClient::class,
      SolrClientFactory::create($clientFactory, $loggerFactory, $configFactory, $eventDispatcher)
    );
  }

  public function testCreateClient(): void
  {
    $config = M::mock(ImmutableConfig::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with('solr_endpoint')
        ->andReturn('https://example.com');

      $mock->shouldReceive('get')
        ->with('solr_basic_auth')
        ->andReturnFalse();
    });

    $clientFactory = M::mock(ClientFactory::class, function(MI $mock) {
      $mock->shouldReceive('fromOptions')
        ->andReturn(M::mock(Client::class));
    });

    $loggerFactory = M::mock(LoggerChannelFactoryInterface::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with(XsSolr::LOG_CHANNEL)
        ->andReturn(M::mock(LoggerChannelInterface::class));
    });

    $factory = new SolrClientFactory($config);

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    Assert::assertInstanceOf(
      SolrClient::class,
      $factory->createSolrClient($clientFactory, $loggerFactory, $eventDispatcher)
    );
  }

  /**
   * @dataProvider baseUriDataset
   */
  public function testGuzzleOptionBaseUriEndsWithTrailingSlash(string $configValue): void
  {
    $config = M::mock(ImmutableConfig::class, function(MI $mock) use ($configValue) {
      $mock->shouldReceive('get')
        ->with('solr_endpoint')
        ->andReturn($configValue);

      $mock->shouldReceive('get')
        ->with('solr_basic_auth')
        ->andReturnFalse();
    });

    $factory       = new SolrClientFactory($config);
    $guzzleOptions = $factory->getGuzzleClientOptions();

    Assert::assertArrayHasKey('base_uri', $guzzleOptions);
    Assert::assertStringEndsWith('/', $guzzleOptions['base_uri']);
  }

  /**
   * @dataProvider nullAuthDataset
   */
  public function testGuzzleOptionAuthIsNullForGivenConfigDataset($basicAuth, $username, $password): void
  {
    $config = M::mock(ImmutableConfig::class, function(MI $mock) use ($basicAuth, $username, $password) {
      $mock->shouldReceive('get')
        ->with('solr_endpoint')
        ->andReturn('https://example.com');

      $mock->shouldReceive('get')
        ->with('solr_basic_auth')
        ->andReturn($basicAuth);

      $mock->shouldReceive('get')
        ->with('solr_username')
        ->andReturn($username);

      $mock->shouldReceive('get')
        ->with('solr_password')
        ->andReturn($password);
    });

    $factory       = new SolrClientFactory($config);
    $guzzleOptions = $factory->getGuzzleClientOptions();

    Assert::assertArrayHasKey('auth', $guzzleOptions);
    Assert::assertNull($guzzleOptions['auth']);
  }

  public function testGuzzleOptionAuthIsSetGivenTheRightConfig(): void
  {
    $config = M::mock(ImmutableConfig::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with('solr_endpoint')
        ->andReturn('https://example.com');

      $mock->shouldReceive('get')
        ->with('solr_basic_auth')
        ->andReturnTrue();

      $mock->shouldReceive('get')
        ->with('solr_username')
        ->andReturn('foo');

      $mock->shouldReceive('get')
        ->with('solr_password')
        ->andReturn('bar');
    });

    $factory       = new SolrClientFactory($config);
    $guzzleOptions = $factory->getGuzzleClientOptions();

    Assert::assertArrayHasKey('auth', $guzzleOptions);
    Assert::assertIsArray($guzzleOptions['auth']);
    Assert::assertCount(2, $guzzleOptions['auth']);
  }
}
