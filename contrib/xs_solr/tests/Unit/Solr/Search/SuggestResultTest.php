<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Suggestion;
use Drupal\xs_solr\Solr\Search\SuggestResult;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class SuggestResultTest extends TestCase
{
  public static function getSuggestionFromDataset(): array
  {
    return [
      ['foo'],
      ['baz'],
      ['lorem'],
    ];
  }

  public function testRawDataCanBeRetrieved(): void
  {
    $suggestData   = ['foo' => ['bar' => []]];
    $suggestResult = new SuggestResult($suggestData);

    Assert::assertEquals($suggestData, $suggestResult->getRawData());
  }

  /**
   * @dataProvider getSuggestionFromDataset
   */
  public function testGetSuggestionFromReturnsEmptyArray(string $dictionary): void
  {
    $suggestResult = new SuggestResult([
      'bar' => [
        'baz' => [],
      ],
      'ipsum' => [
        'baz' => [],
      ],
    ]);

    Assert::assertFalse($suggestResult->hasDictionary($dictionary));
    Assert::assertCount(0, $suggestResult->getSuggestionFrom($dictionary));
  }

  public function testGetSuggestionFromReturnsSuggestions(): void
  {
    $suggestResult = new SuggestResult([
      'bar' => [
        'baz' => [
          'suggestions' => [
            [
              'term' => 'lorem-ipsum',
            ],
          ],
        ],
      ],
    ]);

    $suggestions = $suggestResult->getSuggestionFrom('bar');

    Assert::assertTrue($suggestResult->hasDictionary('bar'));
    Assert::assertCount(1, $suggestions);
    Assert::assertContainsOnlyInstancesOf(Suggestion::class, $suggestions);
  }
}
