<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Facets;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class FacetsTest extends TestCase
{
  public static function facetEntriesDataset(): array
  {
    return [
      [FALSE, 'foo', []],
      [TRUE, 'bar', ['baz' => 100]],
    ];
  }

  public function testRawDataCanBeRetrieved(): void
  {
    $facetsData = ['foo' => ['bar' => 'baz']];
    $facets     = new Facets($facetsData);

    Assert::assertEquals($facetsData, $facets->getRawData());
  }

  public function testHasFacetFieldsReturnsFalseWhenMissing(): void
  {
    $facets = new Facets(['foo' => ['bar' => 'baz']]);

    Assert::assertFalse($facets->hasFacetFields());
  }

  public function testHasFacetFieldsReturnsTrueWhenPresent(): void
  {
    $facets = new Facets(['facet_fields' => ['bar' => 'baz']]);

    Assert::assertTrue($facets->hasFacetFields());
    Assert::assertEquals(['bar' => 'baz'], $facets->getFacetFields());
  }

  public function testGetFacetFieldsReturnsEmptyArrayWhenMissing(): void
  {
    $facets = new Facets(['foo' => ['bar' => 'baz']]);

    Assert::assertCount(0, $facets->getFacetFields());
  }

  public function testGetFacetFieldsReturnsFacetFields(): void
  {
    $facets = new Facets(['facet_fields' => ['bar' => 'baz']]);

    Assert::assertTrue($facets->hasFacetFields());
    Assert::assertCount(1, $facets->getFacetFields());
    Assert::assertEquals(['bar' => 'baz'], $facets->getFacetFields());
  }

  public function testGetFacetFieldNamesReturnsTheNames(): void
  {
    $facets = new Facets(['facet_fields' => ['bar' => 'baz']]);

    Assert::assertEquals(['bar'], $facets->getFacetFieldNames());
  }

  /**
   * @dataProvider facetEntriesDataset
   */
  public function testHasAndGetFacetField(bool $exists, string $field, array $facetData): void
  {
    $facets = new Facets([
      'facet_fields' => [
        'bar' => [
          'baz' => 100,
        ],
      ],
    ]);

    Assert::assertEquals($exists, $facets->hasFacetField($field));
    Assert::assertEquals($facetData, $facets->getFacetField($field));
  }

  public function testHasFacetFieldFailsWhenFacetFieldsAreMissing(): void
  {
    $facets = new Facets(['foo' => []]);

    Assert::assertFalse($facets->hasFacetField('lorem'));
  }

  public function testHasFacetIntervalsReturnsFalseWhenMissing(): void
  {
    $facets = new Facets(['foo' => ['bar' => 'baz']]);

    Assert::assertFalse($facets->hasFacetIntervals());
  }

  public function testHasFacetIntervalsReturnsTrueWhenPresent(): void
  {
    $facets = new Facets(['facet_intervals' => ['bar' => 'baz']]);

    Assert::assertTrue($facets->hasFacetIntervals());
    Assert::assertEquals(['bar' => 'baz'], $facets->getFacetIntervals());
  }

  public function testGetFacetIntervalsReturnsEmptyArrayWhenMissing(): void
  {
    $facets = new Facets(['foo' => ['bar' => 'baz']]);

    Assert::assertCount(0, $facets->getFacetIntervals());
  }

  public function testGetFacetIntervalsReturnsFacetIntervals(): void
  {
    $facets = new Facets(['facet_intervals' => ['bar' => 'baz']]);

    Assert::assertTrue($facets->hasFacetIntervals());
    Assert::assertCount(1, $facets->getFacetIntervals());
    Assert::assertEquals(['bar' => 'baz'], $facets->getFacetIntervals());
  }

  public function testGetFacetIntervalNamesReturnsTheNames(): void
  {
    $facets = new Facets(['facet_intervals' => ['bar' => 'baz']]);

    Assert::assertEquals(['bar'], $facets->getFacetIntervalNames());
  }

  /**
   * @dataProvider facetEntriesDataset
   */
  public function testHasAndGetFacetInterval(bool $exists, string $field, array $facetData): void
  {
    $facets = new Facets([
      'facet_intervals' => [
        'bar' => [
          'baz' => 100,
        ],
      ],
    ]);

    Assert::assertEquals($exists, $facets->hasFacetInterval($field));
    Assert::assertEquals($facetData, $facets->getFacetInterval($field));
  }

  public function testHasFacetIntervalFailsWhenFacetIntervalsAreMissing(): void
  {
    $facets = new Facets(['foo' => []]);

    Assert::assertFalse($facets->hasFacetInterval('lorem'));
  }
}
