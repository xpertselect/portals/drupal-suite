<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Document;
use Drupal\xs_solr\Solr\Search\ResultSet;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class ResultSetTest extends TestCase
{
  public function testRawResponseCanBeRetrieved(): void
  {
    $responseData = ['foo' => ['bar' => 'baz']];
    $resultSet    = new ResultSet($responseData);

    Assert::assertEquals($responseData, $resultSet->getRawResponse());
  }

  public function testClassUsesSensibleFallbacksWhenDataIsMissing(): void
  {
    $resultSet = new ResultSet();

    Assert::assertEquals(0, $resultSet->numFound());
    Assert::assertEquals(0, $resultSet->start());
    Assert::assertCount(0, $resultSet->documents());
  }

  public function testGettersUseResponseData(): void
  {
    $resultSet = new ResultSet(['numFound' => 300, 'start' => 25, 'docs' => []]);

    Assert::assertEquals(300, $resultSet->numFound());
    Assert::assertEquals(25, $resultSet->start());
    Assert::assertCount(0, $resultSet->documents());
  }

  public function testDocumentsAreConverted(): void
  {
    $resultSet = new ResultSet(['docs' => [['foo' => 'bar']]]);

    Assert::assertCount(1, $resultSet->documents());
    Assert::assertContainsOnlyInstancesOf(Document::class, $resultSet->documents());
  }
}
