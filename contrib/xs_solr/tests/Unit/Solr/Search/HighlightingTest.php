<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Highlighting;
use PHPUnit\Framework\Assert;
use RuntimeException;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class HighlightingTest extends TestCase
{
  public static function hasHighlightingForDataset(): array
  {
    return [
      [FALSE, 'lorem', 'ipsum'],
      [FALSE, 'foo', 'ipsum'],
      [TRUE, 'foo', 'bar'],
    ];
  }

  public static function getHighlightingForDataset(): array
  {
    return [
      [[], 'lorem', 'ipsum'],
      [[], 'foo', 'ipsum'],
      [['baz', 'lorem'], 'foo', 'bar'],
    ];
  }

  public function testRawDataCanBeRetrieved(): void
  {
    $highlightingData = ['foo' => ['bar']];
    $highlighting     = new Highlighting($highlightingData);

    Assert::assertEquals($highlightingData, $highlighting->getRawData());
  }

  /**
   * @dataProvider hasHighlightingForDataset
   */
  public function testHasHighlightingFor(bool $expected, string $documentId, string $field): void
  {
    $highlighting = new Highlighting([
      'foo' => [
        'bar' => ['baz', 'lorem'],
      ],
    ]);

    Assert::assertEquals($expected, $highlighting->hasHighlightingFor($documentId, $field));
  }

  /**
   * @dataProvider getHighlightingForDataset
   */
  public function testGetHighlightingFor(array $expected, string $documentId, string $field): void
  {
    $highlighting = new Highlighting([
      'foo' => [
        'bar' => ['baz', 'lorem'],
      ],
    ]);

    $result = $highlighting->getHighlightingFor($documentId, $field);

    Assert::assertIsArray($result);
    Assert::assertEquals($expected, $result);
  }

  public function testGetFirstHighlightingForThrowsRuntimeExceptionWhenNoEntries(): void
  {
    $this->expectException(RuntimeException::class);

    $highlighting = new Highlighting([
      'foo' => [
        'bar' => ['baz', 'lorem'],
      ],
    ]);

    $highlighting->getFirstHighlightingFor('lorem', 'ipsum');
  }

  public function testGetFirstHighlightingReturnsFirstEntry(): void
  {
    $highlighting = new Highlighting([
      'foo' => [
        'bar' => ['baz', 'lorem'],
      ],
    ]);

    Assert::assertEquals('baz', $highlighting->getFirstHighlightingFor('foo', 'bar'));
  }
}
