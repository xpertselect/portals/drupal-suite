<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Group;
use Drupal\xs_solr\Solr\Search\ResultSet;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class GroupTest extends TestCase
{
  public static function getGroupValueDataset(): array
  {
    return [
      [[], ''],
      [['groupValue' => NULL], ''],
      [['groupValue' => ''], ''],
      [['groupValue' => 'foo'], 'foo'],
    ];
  }

  public function testRawDataCanBeRetrieved(): void
  {
    $groupData = ['groupValue' => 'foo', 'doclist' => []];
    $group     = new Group($groupData);

    Assert::assertEquals($groupData, $group->getRawData());
  }

  /**
   * @dataProvider getGroupValueDataset
   */
  public function testGetGroupValue(array $groupData, string $expected): void
  {
    $group = new Group($groupData);

    Assert::assertEquals($expected, $group->getGroupValue());
  }

  public function testGetDocListReturnsNullWhenMissing(): void
  {
    $group = new Group(['groupValue' => 'foo']);

    Assert::assertNull($group->getDocList());
  }

  public function testGetDocListReturnsResultSet(): void
  {
    $group   = new Group(['groupValue' => 'foo', 'doclist' => ['foo' => 'bar']]);
    $docList = $group->getDocList();

    Assert::assertInstanceOf(ResultSet::class, $docList);
    Assert::assertEquals(['foo' => 'bar'], $docList->getRawResponse());
  }
}
