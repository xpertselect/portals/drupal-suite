<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Spellcheck;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class SpellcheckTest extends TestCase
{
  public function testRawDataCanBeRetrieved(): void
  {
    $spellcheckData = ['foo' => ['bar']];
    $spellcheck     = new Spellcheck($spellcheckData);

    Assert::assertEquals($spellcheckData, $spellcheck->getRawData());
  }

  public function testGetSuggestionsReturnsEmptyArrayWhenMissing(): void
  {
    $spellcheck = new Spellcheck();

    Assert::assertCount(0, $spellcheck->getSuggestions());
  }

  public function testGetSuggestionsReturnsSuggestions(): void
  {
    $spellcheck = new Spellcheck([
      'suggestions' => [
        'foo' => ['bar'],
      ],
    ]);

    Assert::assertCount(1, $spellcheck->getSuggestions());
  }

  public function testGetCollationsReturnsEmptyArrayWhenMissing(): void
  {
    $spellcheck = new Spellcheck();

    Assert::assertCount(0, $spellcheck->getCollations());
  }

  public function testGetCollationsReturnsCollations(): void
  {
    $spellcheck = new Spellcheck([
      'collations' => [
        'foo' => 'bar',
      ],
    ]);

    Assert::assertCount(1, $spellcheck->getCollations());
  }
}
