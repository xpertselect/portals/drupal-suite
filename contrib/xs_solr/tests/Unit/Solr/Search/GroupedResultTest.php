<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Group;
use Drupal\xs_solr\Solr\Search\GroupedResult;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class GroupedResultTest extends TestCase
{
  public static function getGroupsForDataset(): array
  {
    return [
      ['foo'],
      ['bar'],
      ['lorem'],
    ];
  }

  public function testRawDataCanBeRetrieved(): void
  {
    $groupData     = ['foo' => ['bar' => []]];
    $groupedResult = new GroupedResult($groupData);

    Assert::assertEquals($groupData, $groupedResult->getRawData());
  }

  /**
   * @dataProvider getGroupsForDataset
   */
  public function testGetGroupsForReturnsEmptyArray(string $field): void
  {
    $groupedResult = new GroupedResult([
      'bar' => [
        'baz' => [],
      ],
      'lorem' => [
        'groups' => [],
      ],
    ]);

    Assert::assertCount(0, $groupedResult->getGroupsFor($field));
  }

  public function testGetGroupsReturnsGroups(): void
  {
    $groupedResult = new GroupedResult([
      'lorem' => [
        'groups' => [
          ['groupValue' => 'my-group', 'doclist' => []],
        ],
      ],
    ]);

    $groups = $groupedResult->getGroupsFor('lorem');

    Assert::assertCount(1, $groups);
    Assert::assertContainsOnlyInstancesOf(Group::class, $groups);
  }
}
