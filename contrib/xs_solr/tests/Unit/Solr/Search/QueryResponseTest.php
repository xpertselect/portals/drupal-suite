<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Facets;
use Drupal\xs_solr\Solr\Search\GroupedResult;
use Drupal\xs_solr\Solr\Search\Highlighting;
use Drupal\xs_solr\Solr\Search\QueryResponse;
use Drupal\xs_solr\Solr\Search\ResultSet;
use Drupal\xs_solr\Solr\Search\Spellcheck;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class QueryResponseTest extends TestCase
{
  public function testRawResponseCanBeRetrieved(): void
  {
    $responseData  = ['foo' => ['bar']];
    $queryResponse = new QueryResponse($responseData);

    Assert::assertEquals($responseData, $queryResponse->getRawResponse());
  }

  public function testHasMethodsReturnFalseOnEmptyResponse(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasResponseHeader());
    Assert::assertFalse($queryResponse->hasResultSet());
    Assert::assertFalse($queryResponse->hasFacets());
    Assert::assertFalse($queryResponse->hasSpellcheck());
    Assert::assertFalse($queryResponse->hasHighlighting());
    Assert::assertFalse($queryResponse->hasGroupedResults());
  }

  public function testGetResponseHeaderWhenHeaderIsMissingReturnsNull(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasResponseHeader());
    Assert::assertNull($queryResponse->getResponseHeader());
  }

  public function testGetResponseHeaderReturnsResponseHeader(): void
  {
    $queryResponse = new QueryResponse(['responseHeader' => ['foo' => 'bar']]);

    Assert::assertTrue($queryResponse->hasResponseHeader());
    Assert::assertEquals(['foo' => 'bar'], $queryResponse->getResponseHeader());
  }

  public function testGetResultSetWhenResponseSetIsMissingReturnsNull(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasResultSet());
    Assert::assertNull($queryResponse->getResultSet());
  }

  public function testGetResultSetReturnsResultSet(): void
  {
    $queryResponse = new QueryResponse(['response' => ['foo' => 'bar']]);

    Assert::assertTrue($queryResponse->hasResultSet());
    Assert::assertInstanceOf(ResultSet::class, $queryResponse->getResultSet());
  }

  public function testGetFacetsWhenFacetsAreMissingReturnsNull(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasFacets());
    Assert::assertNull($queryResponse->getFacets());
  }

  public function testGetFacetsReturnsFacets(): void
  {
    $queryResponse = new QueryResponse(['facet_counts' => ['foo' => 'bar']]);

    Assert::assertTrue($queryResponse->hasFacets());
    Assert::assertInstanceOf(Facets::class, $queryResponse->getFacets());
  }

  public function testGetSpellcheckWhenSpellcheckIsMissingReturnsNull(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasSpellcheck());
    Assert::assertNull($queryResponse->getSpellcheck());
  }

  public function testGetSpellcheckReturnsSpellcheck(): void
  {
    $queryResponse = new QueryResponse(['spellcheck' => ['foo' => ['bar']]]);

    Assert::assertTrue($queryResponse->hasSpellcheck());
    Assert::assertInstanceOf(Spellcheck::class, $queryResponse->getSpellcheck());
  }

  public function testGetHighlightingWhenHighlightingIsMissingReturnsNull(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasHighlighting());
    Assert::assertNull($queryResponse->getHighlighting());
  }

  public function testGetHighlightingReturnsHighlighting(): void
  {
    $queryResponse = new QueryResponse(['highlighting' => ['foo' => ['bar']]]);

    Assert::assertTrue($queryResponse->hasHighlighting());
    Assert::assertInstanceOf(Highlighting::class, $queryResponse->getHighlighting());
  }

  public function testGetGroupedResultsWhenGroupedResultsAreMissingReturnsNull(): void
  {
    $queryResponse = new QueryResponse();

    Assert::assertFalse($queryResponse->hasGroupedResults());
    Assert::assertNull($queryResponse->getGroupedResults());
  }

  public function testGetGroupedResultsReturnsGroups(): void
  {
    $queryResponse = new QueryResponse(['grouped' => ['foo' => ['bar' => []]]]);

    Assert::assertTrue($queryResponse->hasGroupedResults());
    Assert::assertInstanceOf(GroupedResult::class, $queryResponse->getGroupedResults());
  }
}
