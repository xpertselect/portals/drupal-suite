<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use DateTime;
use Drupal\xs_solr\Solr\Search\Document;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class DocumentTest extends TestCase
{
  public function testRawDocumentCanBeRetrieved(): void
  {
    $responseData = ['foo' => ['bar' => 'baz']];
    $document     = new Document($responseData);

    Assert::assertEquals($responseData, $document->getRawDocument());
  }

  public function testHasField(): void
  {
    $document = new Document(['foo' => 'bar', 'baz' => NULL]);

    Assert::assertTrue($document->hasField('foo'));
    Assert::assertTrue($document->hasField('baz'));
    Assert::assertFalse($document->hasField('lorem'));
  }

  public function testGetFieldNames(): void
  {
    $document = new Document(['foo' => 'bar', 'baz' => NULL]);

    Assert::assertCount(2, $document->getFieldNames());
    Assert::assertNotContains('lorem', $document->getFieldNames());
  }

  public function testGetField(): void
  {
    $document = new Document(['baz' => NULL, 'foo' => []]);

    Assert::assertNull($document->getField('bar'));
    Assert::assertNull($document->getField('baz'));
    Assert::assertNotNull($document->getField('foo'));
  }

  public function testGetArrayField(): void
  {
    $document = new Document(['foo' => [], 'baz' => 'lorem']);

    Assert::assertNull($document->getArrayField('bar'));
    Assert::assertNull($document->getArrayField('baz'));
    Assert::assertIsArray($document->getArrayField('foo'));
  }

  public function testGetDateTimeField(): void
  {
    $document = new Document(['foo' => '2023-01-01', 'baz' => '2023-01-01T00:00:00Z', 'bar' => FALSE]);

    Assert::assertNull($document->getDateTimeField('foo'));
    Assert::assertNull($document->getDateTimeField('bar'));
    Assert::assertInstanceOf(DateTime::class, $document->getDateTimeField('baz'));
  }

  public function testGetFloatField(): void
  {
    $document = new Document(['foo' => [], 'bar' => 1.2345, 'lorem' => 1]);

    Assert::assertNull($document->getFloatField('foo'));
    Assert::assertNull($document->getFloatField('baz'));
    Assert::assertNull($document->getFloatField('lorem'));
    Assert::assertIsFloat($document->getFloatField('bar'));
  }

  public function testGetIntField(): void
  {
    $document = new Document(['foo' => [], 'bar' => 1.2345, 'lorem' => 1]);

    Assert::assertNull($document->getIntField('foo'));
    Assert::assertNull($document->getIntField('baz'));
    Assert::assertNull($document->getIntField('bar'));
    Assert::assertIsInt($document->getIntField('lorem'));
  }

  public function testGetStringField(): void
  {
    $document = new Document(['foo' => [], 'bar' => 'baz']);

    Assert::assertNull($document->getStringField('foo'));
    Assert::assertNull($document->getStringField('lorem'));
    Assert::assertIsString($document->getStringField('bar'));
  }

  public function testGetBoolField(): void
  {
    $document = new Document(['foo' => [], 'bar' => FALSE, 'baz' => TRUE]);

    Assert::assertNull($document->getBoolField('foo'));
    Assert::assertNull($document->getBoolField('lorem'));
    Assert::assertIsBool($document->getBoolField('bar'));
    Assert::assertIsBool($document->getBoolField('baz'));
  }
}
