<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr\Search;

use Drupal\xs_solr\Solr\Search\Suggestion;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class SuggestionTest extends TestCase
{
  public static function getTermDataset(): array
  {
    return [
      [[], ''],
      [['term' => NULL], ''],
      [['term' => ''], ''],
      [['term' => 'foo'], 'foo'],
    ];
  }

  public static function getWeightDataset(): array
  {
    return [
      [[], 0],
      [['weight' => NULL], 0],
      [['weight' => 0], 0],
    ];
  }

  public static function getPayloadDataset(): array
  {
    return [
      [[], ''],
      [['payload' => NULL], ''],
      [['payload' => ''], ''],
      [['payload' => 'foo'], 'foo'],
    ];
  }

  public function testRawDataCanBeRetrieved(): void
  {
    $suggestionData = ['term' => 'lorem-ipsum'];
    $suggestion     = new Suggestion($suggestionData);

    Assert::assertEquals($suggestionData, $suggestion->getRawData());
  }

  /**
   * @dataProvider getTermDataset
   */
  public function testGetTerm(array $suggestionData, string $expected): void
  {
    $suggestion = new Suggestion($suggestionData);

    Assert::assertEquals($expected, $suggestion->getTerm());
  }

  /**
   * @dataProvider getWeightDataset
   */
  public function testGetWeight(array $suggestionData, int $expected): void
  {
    $suggestion = new Suggestion($suggestionData);

    Assert::assertEquals($expected, $suggestion->getWeight());
  }

  /**
   * @dataProvider getPayloadDataset
   */
  public function testGetPayload(array $suggestionData, string $expected): void
  {
    $suggestion = new Suggestion($suggestionData);

    Assert::assertEquals($expected, $suggestion->getPayload());
  }
}
