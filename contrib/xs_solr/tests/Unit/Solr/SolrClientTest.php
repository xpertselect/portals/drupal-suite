<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_solr\Solr\SolrClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class SolrClientTest extends TestCase
{
  public static function errorStatusCodesDataset(): array
  {
    return [
      [400],
      [401],
      [403],
      [404],
      [409],
      [422],
      [500],
      [501],
      [502],
      [503],
    ];
  }

  public function testCollectionReturnsTheSameInstance(): void
  {
    $httpClient      = M::mock(ClientInterface::class);
    $logger          = M::mock(LoggerChannelInterface::class);
    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    $solrClient = new SolrClient($httpClient, $logger, $eventDispatcher);

    Assert::assertSame($solrClient->collection('foo'), $solrClient->collection('foo'));
    Assert::assertNotSame($solrClient->collection('bar'), $solrClient->collection('foo'));
  }

  public function testCollectionNameMatches(): void
  {
    $httpClient      = M::mock(ClientInterface::class);
    $logger          = M::mock(LoggerChannelInterface::class);
    $eventDispatcher = M::mock(EventDispatcherInterface::class);
    $solrClient      = new SolrClient($httpClient, $logger, $eventDispatcher);
    $collection      = $solrClient->collection('Foo');

    Assert::assertEquals('Foo', $collection->getName());
  }

  public function testCheckConnectionReturnsNullOnClientError(): void
  {
    $httpClient = M::mock(ClientInterface::class, function(MI $mock) {
      $mock->shouldReceive('request')->andThrow(TransferException::class);
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldReceive('error')->andReturns();
    });

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    $solrClient = new SolrClient($httpClient, $logger, $eventDispatcher);

    Assert::assertNull($solrClient->checkConnection());
  }

  /**
   * @dataProvider errorStatusCodesDataset
   */
  public function testCheckConnectionReturnsStatusCodeForRequestExceptionsWithResponses(int $status): void
  {
    $httpClient = M::mock(ClientInterface::class, function(MI $mock) use ($status) {
      $mock->shouldReceive('request')
        ->andThrow(M::mock(RequestException::class, function(MI $mock) use ($status) {
          $mock->shouldReceive('getResponse')
            ->andReturn(M::mock(ResponseInterface::class, function(MI $mock) use ($status) {
              $mock->shouldReceive('getStatusCode')->andReturn($status);
            }));
        }));
    });

    $solrClient = new SolrClient(
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );

    Assert::assertEquals($status, $solrClient->checkConnection());
  }

  public function testCheckConnectionReturns200WhenSuccessful(): void
  {
    $httpClient = M::mock(ClientInterface::class, function(MI $mock) {
      $mock->shouldReceive('request')
        ->andReturn(M::mock(ResponseInterface::class, function(MI $mock) {
          $mock->shouldReceive('getStatusCode')->andReturn(200);
        }));
    });

    $solrClient = new SolrClient(
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );

    Assert::assertEquals(200, $solrClient->checkConnection());
  }
}
