<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Solr;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_solr\Event\IndexUpdated;
use Drupal\xs_solr\Solr\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class CollectionTest extends TestCase
{
  public static function createGetParametersDataset(): array
  {
    return [
      [
        '*:*', [], 0, 10, [],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10],
      ],
      [
        '', [], 0, 10, [],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10],
      ],
      [
        '*:*', [''], 0, 10, [],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10],
      ],
      [
        '*:*', [], 0, 10, ['json.nl' => 'foo'],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10],
      ],
      [
        '*:*', [], 0, 10, ['omitHeader' => FALSE],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10],
      ],
      [
        '*:*', ['foo="bar"'], 0, 10, [],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10, 'fq' => 'foo="bar"'],
      ],
      [
        '*:*', ['', 'foo="bar"'], 0, 10, [],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10, 'fq' => 'foo="bar"'],
      ],
      [
        '*:*', ['foo="bar"', 'lorem="ipsum"'], 0, 10, [],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10, 'fq' => 'foo="bar" AND lorem="ipsum"'],
      ],
      [
        '*:*', [], 0, 10, ['facet' => TRUE],
        ['wt' => 'json', 'omitHeader' => 'true', 'json.nl' => 'map', 'q' => '*:*', 'start' => 0, 'rows' => 10, 'facet' => 'true'],
      ],
    ];
  }

  public function testNameCanBeRetrieved(): void
  {
    $name            = 'lorem_ipsum';
    $httpClient      = M::mock(Client::class);
    $logger          = M::mock(LoggerChannelInterface::class);
    $eventDispatcher = M::mock(EventDispatcherInterface::class);
    $collection      = new Collection($name, $httpClient, $logger, $eventDispatcher);

    Assert::assertEquals($name, $collection->getName());
  }

  public function testQuerySendsRequestsToCorrectEndpoint(): void
  {
    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getBody')
          ->andReturn($this->mockPsrStreamInterface('{"foo": "bar"}'));
      });

      $mock->shouldReceive('request')
        ->with('GET', 'lorem_ipsum/select', [
          'query' => 'wt=json&omitHeader=true&json.nl=map&q=%2A%3A%2A&start=0&rows=10',
        ])
        ->andReturn($response);
    });

    $collection = new Collection(
      'lorem_ipsum',
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );
    $queryResponse = $collection->query();

    Assert::assertNotNull($queryResponse);
    Assert::assertEquals(['foo' => 'bar'], $queryResponse->getRawResponse());
  }

  public function testQueryGetParamsCorrectly(): void
  {
    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getBody')
          ->andReturn($this->mockPsrStreamInterface('{"foo": "bar"}'));
      });

      $mock->shouldReceive('request')
        ->with('GET', 'lorem_ipsum/select', [
          'query' => 'facet=true&facet.field=facet_access_rights&facet.field=facet_format&facet.field=facet_license&' .
            'facet.field=facet_status&facet.field=facet_tags&facet.field=facet_theme&facet.field=facet_organization&' .
            'f.facet_access_rights.facet.limit=-1&f.facet_access_rights.facet.sort=index&' .
            'f.facet_format.facet.limit=-1&f.facet_format.facet.sort=index&' .
            'fl=id%2Cmachine_name%2Ctitle%2Cdescription%2Cvisibility%2Corganization%2Ccreated&' .
            'wt=json&omitHeader=true&json.nl=map&q=%2A%3A%2A&start=0&rows=10',
        ])
        ->andReturn($response);
    });

    $collection = new Collection(
      'lorem_ipsum',
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );
    $queryResponse = $collection->query(additionalParameters: [
      'facet'       => TRUE,
      'facet.field' => [
        'facet_access_rights',
        'facet_format',
        'facet_license',
        'facet_status',
        'facet_tags',
        'facet_theme',
        'facet_organization',
      ],
      'f.facet_access_rights.facet.limit' => -1,
      'f.facet_access_rights.facet.sort'  => 'index',
      'f.facet_format.facet.limit'        => -1,
      'f.facet_format.facet.sort'         => 'index',
      'fl'                                => 'id,machine_name,title,description,visibility,organization,created',
    ]);

    Assert::assertNotNull($queryResponse);
    Assert::assertEquals(['foo' => 'bar'], $queryResponse->getRawResponse());
  }

  public function testQueryReturnsNullOnGuzzleException(): void
  {
    $httpClient = M::mock(Client::class, function(MI $mock) {
      $mock->shouldReceive('request')->andThrow(M::mock(TransferException::class, function(MI $mock) {
        $mock->shouldReceive('getMessage')
          ->andReturn('lorem ipsum dolor sit amet');
      }));
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldReceive('error')->andReturns();
    });

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    $collection    = new Collection('lorem_ipsum', $httpClient, $logger, $eventDispatcher);
    $queryResponse = $collection->query();

    Assert::assertNull($queryResponse);
  }

  public function testQueryReturnsNullOnInvalidJson(): void
  {
    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getBody')
          ->andReturn($this->mockPsrStreamInterface('{"foo" => "bar"}'));
      });

      $mock->shouldReceive('request')->andReturn($response);
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldReceive('error')->andReturns();
    });

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    $collection    = new Collection('lorem_ipsum', $httpClient, $logger, $eventDispatcher);
    $queryResponse = $collection->query();

    Assert::assertNull($queryResponse);
  }

  public function testSuggestSendsRequestsToCorrectEndpoint(): void
  {
    $query      = 'baz';
    $handler    = 'suggest';
    $count      = 5;
    $dictionary = 'dictionary';

    $httpClient = M::mock(Client::class, function(MI $mock) use ($handler) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getBody')
          ->andReturn($this->mockPsrStreamInterface('{"foo": "bar"}'));
      });

      $mock->shouldReceive('request')
        ->with('GET', 'lorem_ipsum/' . $handler, [
          'query' => 'suggest=true&suggest.q=baz&suggest.count=5&suggest.dictionary=dictionary&wt=json&omitHeader=true&json.nl=map',
        ])
        ->andReturn($response);
    });

    $collection = new Collection(
      'lorem_ipsum', $httpClient,
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );
    $queryResponse = $collection->suggest($query, $handler, $dictionary, $count);

    Assert::assertNotNull($queryResponse);
    Assert::assertEquals(['foo' => 'bar'], $queryResponse->getRawResponse());
  }

  public function testSuggestReturnsNullOnGuzzleException(): void
  {
    $httpClient = M::mock(Client::class, function(MI $mock) {
      $mock->shouldReceive('request')->andThrow(M::mock(TransferException::class, function(MI $mock) {
        $mock->shouldReceive('getMessage')
          ->andReturn('lorem ipsum dolor sit amet');
      }));
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldReceive('error')->andReturns();
    });

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    $collection    = new Collection('lorem_ipsum', $httpClient, $logger, $eventDispatcher);
    $queryResponse = $collection->suggest('', '', '');

    Assert::assertNull($queryResponse);
  }

  public function testSuggestReturnsNullOnInvalidJson(): void
  {
    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getBody')
          ->andReturn($this->mockPsrStreamInterface('{"foo" => "bar"}'));
      });

      $mock->shouldReceive('request')->andReturn($response);
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldReceive('error')->andReturns();
    });

    $eventDispatcher = M::mock(EventDispatcherInterface::class);

    $collection    = new Collection('lorem_ipsum', $httpClient, $logger, $eventDispatcher);
    $queryResponse = $collection->suggest('', '', '');

    Assert::assertNull($queryResponse);
  }

  /**
   * @dataProvider createGetParametersDataset
   */
  public function testCreateGetParameters(string $query, array $filters, int $start, int $rows,
                                          array $additionalParameters, array $expectedOutput): void
  {
    $collection = new Collection(
      'lorem_ipsum',
      M::mock(Client::class),
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );

    Assert::assertEqualsCanonicalizing(
      $expectedOutput,
      $collection->createParametersFromArguments($query, $filters, $start, $rows, $additionalParameters)
    );
  }

  public function testCreateGetParametersConvertsBooleans(): void
  {
    $collection = new Collection(
      'lorem_ipsum',
      M::mock(Client::class),
      M::mock(LoggerChannelInterface::class),
      M::mock(EventDispatcherInterface::class)
    );

    $parameters = $collection->createParametersFromArguments('*:*', [], 0, 10);

    Assert::assertArrayHasKey('omitHeader', $parameters);
    Assert::assertSame('true', $parameters['omitHeader']);
    Assert::assertNotSame(TRUE, $parameters['omitHeader']);
  }

  public function testUpdateDispatchesEventOn200(): void
  {
    $eventWasDispatched = FALSE;
    $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
      $mock->shouldReceive('dispatch')
        ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
          $eventWasDispatched = TRUE;

          Assert::assertInstanceOf(IndexUpdated::class, $event);
          Assert::assertEquals('lorem_ipsum', $event->getName());
        });
    });

    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getStatusCode')
          ->andReturn(200);
      });

      $mock->shouldReceive('request')
        ->andReturn($response);
    });

    $collection = new Collection(
      'lorem_ipsum',
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      $eventDispatcher
    );

    $collection->update([]);

    Assert::assertTrue($eventWasDispatched);
  }

  public function testUpdateDoesNotDispatchEventOnNon200(): void
  {
    $eventDispatcher = new class () implements EventDispatcherInterface {
      public bool $hasDispatched = FALSE;

      public function dispatch(object $event): void
      {
        $this->hasDispatched = TRUE;
      }
    };

    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getStatusCode')
          ->andReturn(500);
      });

      $mock->shouldReceive('request')
        ->andReturn($response);
    });

    $collection = new Collection(
      'lorem_ipsum',
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      $eventDispatcher
    );

    $collection->update([]);

    Assert::assertFalse($eventDispatcher->hasDispatched);
  }

  public function testUpdateDoesNotDispatchEventOnCommitIsFalse(): void
  {
    $eventDispatcher = new class () implements EventDispatcherInterface {
      public bool $hasDispatched = FALSE;

      public function dispatch(object $event): void
      {
        $this->hasDispatched = TRUE;
      }
    };

    $httpClient = M::mock(Client::class, function(MI $mock) {
      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getStatusCode')
          ->andReturn(200);
      });

      $mock->shouldReceive('request')
        ->andReturn($response);
    });

    $collection = new Collection(
      'lorem_ipsum',
      $httpClient,
      M::mock(LoggerChannelInterface::class),
      $eventDispatcher
    );

    $collection->update([], FALSE);

    Assert::assertFalse($eventDispatcher->hasDispatched);
  }
}
