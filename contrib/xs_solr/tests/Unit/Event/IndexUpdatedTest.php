<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Event;

use Drupal\xs_solr\Event\IndexUpdated;
use PHPUnit\Framework\Assert;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class IndexUpdatedTest extends TestCase
{
  public function testIndexUpdatedEventGetName(): void
  {
    $name  = 'foo';
    $event = new IndexUpdated($name);

    Assert::assertEquals($name, $event->getName());
  }
}
