<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit;

use Drupal\xs_solr\GuzzleErrorUtilities;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Request;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class GuzzleErrorUtilitiesTest extends TestCase
{
  public function testExceptionMessageIsReturnedWhenNoResponseIsPresent(): void
  {
    $class = new class () {
      use GuzzleErrorUtilities;
    };

    Assert::assertEquals(
      'lorem ipsum',
      $class->guzzleExceptionToMessage(new TransferException('lorem ipsum'))
    );
  }

  public function testExceptionMessageIsReturnedWhenRequestExceptionHasNoResponse(): void
  {
    $class = new class () {
      use GuzzleErrorUtilities;
    };

    $exception = new RequestException('lorem ipsum', M::mock(Request::class), NULL);

    Assert::assertEquals(
      'lorem ipsum',
      $class->guzzleExceptionToMessage($exception)
    );
  }

  public function testResponseIsUsedWhenRequestExceptionIncludesResponse(): void
  {
    $class = new class () {
      use GuzzleErrorUtilities;
    };

    $exception = M::mock(RequestException::class, function(MI $mock) {
      $request = M::mock(RequestInterface::class, function(MI $mock) {
        $uri = M::mock(UriInterface::class, function(MI $mock) {
          $mock->shouldReceive('getScheme')->andReturn('https');
          $mock->shouldReceive('getHost')->andReturn('example.com');
          $mock->shouldReceive('getPort')->andReturn(443);
          $mock->shouldReceive('getPath')->andReturn('/lorem');
          $mock->shouldReceive('getQuery')->andReturn('foo=bar');
        });

        $mock->shouldReceive('getMethod')->andReturn('get');
        $mock->shouldReceive('getUri')->andReturn($uri);
      });

      $response = M::mock(ResponseInterface::class, function(MI $mock) {
        $mock->shouldReceive('getStatusCode')
          ->andReturn(404);

        $mock->shouldReceive('getBody')
          ->andReturn($this->mockPsrStreamInterface('Lorem ipsum dolor sit amet'));
      });

      $mock->shouldReceive('getRequest')->andReturn($request);
      $mock->shouldReceive('getResponse')->andReturn($response);
    });

    Assert::assertEquals(
      'Request: GET https://example.com:443/lorem?foo=bar; Status: 404; Body: Lorem ipsum dolor sit amet',
      $class->guzzleExceptionToMessage($exception)
    );
  }
}
