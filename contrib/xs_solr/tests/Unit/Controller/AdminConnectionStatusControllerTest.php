<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr\Unit\Controller;

use Drupal\Core\Render\RendererInterface;
use Drupal\xs_solr\Controller\AdminConnectionStatusController;
use Drupal\xs_solr\Solr\SolrClient;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\xs_solr\TestCase;

/**
 * @internal
 */
final class AdminConnectionStatusControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    $container = M::mock(ContainerInterface::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with('xs_solr.client')
        ->andReturn(M::mock(SolrClient::class));

      $mock->shouldReceive('get')
        ->with('renderer')
        ->andReturn(M::mock(RendererInterface::class));
    });

    Assert::assertInstanceOf(
      AdminConnectionStatusController::class,
      AdminConnectionStatusController::create($container)
    );
  }
}
