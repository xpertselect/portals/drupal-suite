<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_solr;

use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Psr\Http\Message\StreamInterface;

/**
 * @internal
 */
abstract class TestCase extends BaseTestCase
{
  protected function mockPsrStreamInterface(string $body): StreamInterface
  {
    return M::mock(StreamInterface::class, function(MI $mock) use ($body) {
      $mock->shouldReceive('getContents')
        ->andReturn($body);
    });
  }
}
