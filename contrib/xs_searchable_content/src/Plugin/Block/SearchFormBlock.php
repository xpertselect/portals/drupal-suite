<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\xs_searchable_content\Form\SearchForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchFormBlock.
 *
 * Provides a 'Search content' Block.
 *
 * @Block(
 *   id = "xs_searchable_content_block",
 *   admin_label = @Translation("Search block"),
 * )
 */
final class SearchFormBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self
  {
    /** @var FormBuilderInterface $formBuilder */
    $formBuilder = $container->get('form_builder');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $formBuilder
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param FormBuilderInterface $formBuilder The form builder for creating the search form that is displayed by this block
   */
  public function __construct(array $configuration,
                              string $plugin_id,
                              $plugin_definition,
                              private readonly FormBuilderInterface $formBuilder)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      'elements'    => $this->formBuilder->getForm(SearchForm::class),
      '#attributes' => [
        'class' => [
          'navbar__search',
          'col-lg-3',
        ],
      ],
    ];
  }
}
