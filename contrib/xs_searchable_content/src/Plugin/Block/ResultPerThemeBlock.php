<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\xs_search\Response\Response;
use Drupal\xs_search\SearchService;
use Drupal\xs_searchable_content\Event\PreparingResultsPerThemeBlock;
use Drupal\xs_searchable_content\Services\Search\ThemeBlockSearchProfile;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ResultPerThemeBlock.
 *
 * Provides a 'Results per theme' Block.
 *
 * @Block(
 *   id = "xs_searchable_content_results_per_theme_block",
 *   admin_label = @Translation("Results per theme"),
 * )
 */
final class ResultPerThemeBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, mixed $plugin_definition): self
  {
    /** @var SearchService $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var ThemeBlockSearchProfile $searchProfile */
    $searchProfile = $container->get('xs_searchable_content.result_per_theme_search_profile.content');

    /** @var AccountProxyInterface $currentUser */
    $currentUser = $container->get('current_user');

    /** @var ThemeManagerInterface $themeManager */
    $themeManager = $container->get('theme.manager');

    /** @var EventDispatcherInterface $dispatcher */
    $dispatcher = $container->get('event_dispatcher');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $searchService,
      $searchProfile,
      $currentUser,
      $themeManager->getActiveTheme()->getName(),
      $dispatcher
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param SearchService            $searchService The service for accessing the search-engine
   * @param ThemeBlockSearchProfile  $searchProfile The search profile used to query the available themes
   * @param AccountProxyInterface    $currentUser   The current user used for caching
   * @param string                   $activeTheme   The active Drupal theme
   * @param EventDispatcherInterface $dispatcher    An event dispatcher used for dispatching a PreparingResultsPerThemeBlock event
   */
  public function __construct(array $configuration,
                              string $plugin_id,
                              mixed $plugin_definition,
                              private readonly SearchService $searchService,
                              private ThemeBlockSearchProfile $searchProfile,
                              private readonly AccountProxyInterface $currentUser,
                              private readonly string $activeTheme,
                              private readonly EventDispatcherInterface $dispatcher)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $results = $this->getFacetResponseFromSolr();

    return [
      '#theme'             => 'results_per_theme_wrapper',
      '#active_theme'      => $this->activeTheme,
      '#results_per_theme' => $this->getThemes($results),
      '#results_total'     => $this->getTotalResultCount($results),
      '#cache'             => [
        'keys'     => [
          'xs_searchable_content',
          'search_results',
          'user:' . $this->currentUser->id(),
        ],
        'contexts' => ['user'],
        'tags'     => array_merge(
          [
            'search_results',
            'user:' . $this->currentUser->id(),
          ],
          $this->searchProfile->getTypes()
        ),
        'max-age'  => CacheBackendInterface::CACHE_PERMANENT,
      ],
      '#attributes' => [
        'class' => [
          'results-per-theme-container',
        ],
      ],
    ];
  }

  /**
   * Retrieve all the themes to show on the homepage from the search engine.
   */
  private function getFacetResponseFromSolr(): ?Response
  {
    $this->dispatcher->dispatch(new PreparingResultsPerThemeBlock($this->searchProfile));

    return $this->searchService->search(
      $this->searchProfile,
      '*:*',
      [],
      0,
      $this->searchProfile->sort()
    );
  }

  /**
   * Format the response of all the themes to show on the homepage from the search engine.
   *
   * @param ?Response $results A solr response containing all the relevant theme information
   *
   * @return array<string, int> The themes and their results counts
   */
  private function getThemes(?Response $results): array
  {
    $facets = $results?->getSolrResponse()?->getFacets();

    if (is_null($facets)) {
      return [];
    }

    $themes = [];

    foreach ($facets->getFacetField('facet_theme') as $name => $count) {
      if (FALSE !== mb_strpos($name, '|')) {
        continue;
      }

      $themes[$name] = $count;
    }

    return $themes;
  }

  /**
   * Get the total number of results from the search engine.
   *
   * @param ?Response $results A solr response containing all the relevant theme information
   *
   * @return int The total amount of results found
   */
  private function getTotalResultCount(?Response $results): int
  {
    $resultSet = $results?->getSolrResponse()?->getResultSet();

    return is_null($resultSet) ? 0 : $resultSet->numFound();
  }
}
