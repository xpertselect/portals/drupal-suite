<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface NodeToDocumentServiceInterface.
 *
 * Contract for defining how a node should be mapped to the schema of the Solr search engine. Each implementation should
 * only provide a mapping for a single type of nodes (unless several types of nodes exist with identical mappings).
 */
interface NodeToDocumentServiceInterface
{
  /**
   * Get the document ID for a given Node. The document ID is the ID of the node in the search engine (which may not be
   * same as the nid!).
   *
   * @param EntityInterface $node The node to get the ID for
   *
   * @return string The ID of the node in the search engine
   */
  public function getSolrDocumentIdentifier(EntityInterface $node): string;

  /**
   * Extract the contents from a given node, map it to the schema of the search engine and return the mapped data.
   *
   * @param EntityInterface $node The node to map
   *
   * @return array<string, string|array<int, string|int>> The contents of the node mapped to the search-engine fields
   */
  public function mapNodeToSolrDocument(EntityInterface $node): array;
}
