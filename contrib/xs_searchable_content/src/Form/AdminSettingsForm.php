<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_general\ManagedSettingsTrait;
use Drupal\xs_search\XsSearch;
use Drupal\xs_searchable_content\XsSearchableContent;

/**
 * Class AdminSettingsForm.
 *
 * Provides a Drupal form for managing the configuration of the `drupal/xs_searchable_content` module.
 */
final class AdminSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  private const FACETS_COUNT_KEY = 'facetsCount';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_searchable_content_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    if ($this->isManaged(XsSearchableContent::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $config = $this->config(XsSearchableContent::SETTINGS_KEY);

    $form['searchable_content'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Index configuration'),
      '#description' => $this->t('Configure the interaction between drupal/xs_searchable_content and the search-engine.'),
      '#open'        => TRUE,

      'schema_field_content_type' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Content-type field'),
        '#description'   => $this->t('Describes which field in the search-engine contains the name of the Drupal content-type.'),
        '#default_value' => $config->get('schema_field_content_type'),
        '#required'      => TRUE,
      ],

      'index_batch_size' => [
        '#type'          => 'number',
        '#title'         => $this->t('Batch size'),
        '#description'   => $this->t('Describes up to how many documents are sent to the search-engine per request during the indexation of a Drupal content-type.'),
        '#default_value' => $config->get('index_batch_size') ?? XsSearchableContent::DEFAULT_INDEX_BATCH_SIZE,
        '#required'      => TRUE,
        '#min'           => 1,
      ],
    ];

    $form['searchable_content_facets'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Facet mapping'),
      '#description' => $this->t('Configure the mapping from the facet machine name to the label.'),
      '#open'        => TRUE,

      'facet_mapping' => [
        '#type'   => 'container',
        '#tree'   => TRUE,
        '#prefix' => '
          <div id="facets-mapping-wrapper">
            <table>
              <thead>
                <tr>
                  <th>' . $this->t('Facet name') . '</th>
                  <th>' . $this->t('Facet label') . '</th>
                </tr>
              </thead>
              <tbody>
        ',
        '#suffix' => '
              </tbody>
            </table>
          </div>
        ',
      ],
    ];

    $this->addFacetMappingFormElements($form, $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void
  {
    $values = $form_state->cleanValues()->getValue('facet_mapping', []);
    $names  = [];

    foreach ($values as $index => $contents) {
      if (empty($contents['name']) xor empty($contents['label'])) {
        $form_state->setErrorByName(
          'facet_mapping][' . $index,
          $this->t('Name and label are required')->render()
        );
      }

      if (!empty($contents['name'])) {
        $name = $contents['name'];

        if (in_array($name, $names)) {
          $form_state->setErrorByName(
            'facet_mapping][' . $index,
            $this->t('This name is already in use')->render()
          );
        }

        $names[] = $name;
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(XsSearch::SETTINGS_KEY, TRUE)) {
      return;
    }

    $config = $this->config(XsSearchableContent::SETTINGS_KEY);
    $config->set('schema_field_content_type', $form_state->getValue('schema_field_content_type'));
    $config->set('index_batch_size', $form_state->getValue('index_batch_size'));
    $config->save();

    $facetMapping = $form_state->cleanValues()->getValue('facet_mapping', []);
    $newConfig    = [];

    foreach ($facetMapping as $facet) {
      if (empty($facet['name']) || empty($facet['label'])) {
        continue;
      }

      $newConfig[$facet['name']] = [
        'name'   => $facet['name'],
        'label'  => $facet['label'],
      ];
    }

    ksort($newConfig);

    $config = $this->config(XsSearchableContent::FACET_MAPPING_KEY);
    $config->set('facet_mapping', $newConfig);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Submit handler for the "add-one-more" button. Increments the max counter and causes a rebuild.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   */
  public function addOne(array &$form, FormStateInterface $form_state): void
  {
    $form_state->set(self::FACETS_COUNT_KEY, $form_state->get(self::FACETS_COUNT_KEY) + 1);
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons. Selects and returns the fieldset with the names in it.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   *
   * @return array<string, mixed> The modified $form
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state): array
  {
    return $form['searchable_content_facets']['facet_mapping'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsSearchableContent::SETTINGS_KEY, XsSearchableContent::FACET_MAPPING_KEY];
  }

  /**
   * @param array<string, mixed> $form       an associative array containing the structure of the form
   * @param FormStateInterface   $form_state the current state of the form
   */
  private function addFacetMappingFormElements(array &$form, FormStateInterface $form_state): void
  {
    $facetConfig = $this->config(XsSearchableContent::FACET_MAPPING_KEY);

    /** @var array<int, array{name:string, label: string}> $facetMapping */
    $facetMapping = array_values($facetConfig->get('facet_mapping') ?? []);
    /** @var null|int $facetsCount */
    $facetsCount  = $form_state->get(self::FACETS_COUNT_KEY);

    if (is_null($facetsCount)) {
      $facetsCount = count($facetMapping) + 1;
      $form_state->set(self::FACETS_COUNT_KEY, $facetsCount);
    }

    for ($i = 0; $i < $facetsCount; ++$i) {
      $form['searchable_content_facets']['facet_mapping'][$i] = [
        'name' => [
          '#type'          => 'textfield',
          '#title'         => $this->t('Facet name'),
          '#title_display' => 'invisible',
          '#default_value' => $facetMapping[$i]['name'] ?? NULL,
          '#prefix'        => '<tr><td>',
          '#suffix'        => '</td>',
        ],
        'label' => [
          '#type'          => 'textfield',
          '#title'         => $this->t('Facet label'),
          '#title_display' => 'invisible',
          '#default_value' => $facetMapping[$i]['label'] ?? NULL,
          '#prefix'        => '<td>',
          '#suffix'        => '</td></tr>',
        ],
      ];
    }

    $form['searchable_content_facets']['row']['#type'] = 'actions';
    $form['searchable_content_facets']['row']['add']   = [
      '#type'                    => 'submit',
      '#value'                   => $this->t('Add another facet mapping'),
      '#submit'                  => ['::addOne'],
      '#limit_validation_errors' => [],
      '#ajax'                    => [
        'callback' => '::addMoreCallback',
        'wrapper'  => 'facets-mapping-wrapper',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
  }
}
