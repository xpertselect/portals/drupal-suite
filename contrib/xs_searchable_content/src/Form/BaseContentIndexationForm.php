<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_searchable_content\NodeIndexationService;
use RuntimeException;

/**
 * Class BaseContentIndexationForm.
 *
 * Provides the base indexation form to index Drupal content-types.
 */
abstract class BaseContentIndexationForm extends FormBase
{
  /**
   * BaseContentIndexationForm constructor.
   *
   * @param NodeIndexationService  $nodeIndexationService The service for indexing nodes
   * @param EntityStorageInterface $nodeStorage           The storage holding all the nodes
   * @param int                    $batchSize             How many nodes to process per batch
   */
  public function __construct(protected NodeIndexationService $nodeIndexationService,
                              protected EntityStorageInterface $nodeStorage, protected int $batchSize)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return sprintf('xs_searchable_content_form_node_%s', $this->getContentTypeName());
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $form['node_table'] = [
      '#type'   => 'table',
      '#header' => [
        ['colspan' => 2, 'data' => $this->t('Content-type "@type"', ['@type' => $this->getContentTypeName()])],
      ],
      '#rows'   => [
        [$this->t('Drupal nodes'), count($this->getContentIdentifiersToIndex())],
        [$this->t('Search-engine'), $this->nodeIndexationService->getIndexedNodesCount($this->getContentTypeName())],
      ],
    ];

    $form['index'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Index content-type'),
      '#description' => $this->t('Use this form to index all the nodes of this content-type into the search-engine.'),
      '#open'        => TRUE,

      'submit'       => [
        '#type'  => 'submit',
        '#value' => 'Index nodes',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $this->nodeIndexationService->deleteContentTypeFromIndex($this->getContentTypeName());

    batch_set($this->createNodeIndexationBatchDefinition());
  }

  /**
   * Return the machine-name of the content-type that should be indexed.
   *
   * @return string The machine-name of the content-type
   */
  abstract protected function getContentTypeName(): string;

  /**
   * Return the name of the Drupal service that implements the `NodeToDocumentServiceInterface` for this content-type.
   *
   * @return string The name of the Drupal service
   */
  abstract protected function getNodeToDocumentServiceName(): string;

  /**
   * Get all the content identifiers of the objects that should be re-indexed.
   *
   * @return int[] The Node IDs
   */
  protected function getContentIdentifiersToIndex(): array
  {
    $nodes = $this->nodeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $this->getContentTypeName(), '=')
      ->condition('status', 1, '=')
      ->execute();

    if (!is_array($nodes)) {
      throw new RuntimeException('Unexpected response from database');
    }

    return $nodes;
  }

  /**
   * Creates a Drupal batch job definition for indexing all nodes of the current type into the search-engine.
   *
   * @return array<string, mixed> The Drupal batch job definition
   */
  private function createNodeIndexationBatchDefinition(): array
  {
    $batches    = array_chunk($this->getContentIdentifiersToIndex(), max($this->batchSize, 1));
    $batchCount = count($batches);
    $operations = [];

    foreach ($batches as $index => $batch) {
      $operations[] = [
        '\Drupal\xs_searchable_content\NodeIndexationBatchService::processBatch', [
          $batch,
          $this->getNodeToDocumentServiceName(),
          $batchCount - 1 === $index,
        ],
      ];
    }

    return [
      'title'            => $this->t('Indexing nodes of @type ...', ['@type' => $this->getContentTypeName()]),
      'init_message'     => $this->t('Initializing node indexation batch job'),
      'progress_message' => $this->t('Processed node indexation batch @current (total @total).'),
      'error_message'    => $this->t('An error occurred while indexing the nodes.'),
      'finished'         => '\Drupal\xs_searchable_content\NodeIndexationBatchService::finalizeBatch',
      'operations'       => $operations,
    ];
  }
}
