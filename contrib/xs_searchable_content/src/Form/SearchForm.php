<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class SearchForm.
 *
 * Provides a simple form for performing search actions.
 */
final class SearchForm extends FormBase
{
  /**
   * The characters to strip from the entered search query.
   *
   * @var string[]
   */
  private array $blockedCharacters = [
    '/',
    ':',
    '(',
    ')',
    '[',
    ']',
    '{',
    '}',
    '=',
    '&',
    '!',
    '*',
    '%',
  ];

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string
  {
    return 'xs_searchable_content_search_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $inputQuery = $this->getRequest()->get('query');

    if ('-' === $inputQuery) {
      $inputQuery = NULL;
    }

    $form['query'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Search'),
      '#title_display' => 'invisible',
      '#default_value' => $inputQuery ?: NULL,
      '#attributes'    => [
        'placeholder' => $this->t('Search'),
        'aria-label'  => $this->t('Search phrase'),
      ],
    ];

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['#attached']['library'][] = 'xs_searchable_content/search-block-scripts';

    $form['#attached']['drupalSettings']['xs_searchable_content']['autocomplete']['url']
      = Url::fromRoute('xs_searchable_content.api.suggest_content', [
        'query' => 'QUERY_PLACEHOLDER',
      ])->toString();

    $form['#attached']['drupalSettings']['xs_searchable_content']['search']['url']
      = Url::fromRoute('xs_searchable_content.web.search', [
        'query'       => 'QUERY_PLACEHOLDER',
        'filters'     => '-',
        'page_number' => 1,
      ])->toString();

    $form['#cache'] = ['max-age' => 0];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $query = str_replace($this->blockedCharacters, '', $form_state->getValue('query'));

    if ('' === trim($query)) {
      $query = '-';
    }

    $form_state->setRedirectUrl(Url::fromRoute(
      'xs_searchable_content.web.search', [
        'query'       => $query,
        'filters'     => '-',
        'page_number' => 1,
      ]
    ));
  }
}
