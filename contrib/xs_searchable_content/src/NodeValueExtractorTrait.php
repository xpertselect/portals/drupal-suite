<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content;

use DateTime;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\node\NodeInterface;
use Drupal\xs_solr\XsSolr;
use InvalidArgumentException;

trait NodeValueExtractorTrait
{
  /**
   * Gets the string value for a given field and node.
   *
   * @param NodeInterface $node  The node to get a value from
   * @param string        $field The field to get the value for
   *
   * @return null|string The value for a given field and node or NULL in case the field is invalid
   */
  private function getFieldStringValue(NodeInterface $node, string $field): ?string
  {
    try {
      return $node->get($field)->getString();
    } catch (InvalidArgumentException) {
      return NULL;
    }
  }

  /**
   * Gets the string value for a given rich text field and node.
   *
   * @param NodeInterface $node            The node to get a value from
   * @param string        $field           The field to get the value for
   * @param bool          $clearFormatting Whether to clean to resulting string from formatting
   *
   * @return null|string The value for a given rich text field and node or NULL in case the field is invalid or the
   *                     data is missing
   */
  private function getRichTextStringValue(NodeInterface $node, string $field, bool $clearFormatting = TRUE): ?string
  {
    try {
      $value = $node->get($field)->first()?->getValue()['value'] ?? NULL;

      if (is_null($value)) {
        return NULL;
      }

      $value = strval($value);

      if (FALSE === $clearFormatting) {
        return $value;
      }

      $value = html_entity_decode($value);
      $value = strip_tags($value);

      return trim($value);
    } catch (InvalidArgumentException|MissingDataException) {
      return NULL;
    }
  }

  /**
   * Gets the dates as a string and format them as a ISO8601 timestamp for a given date field and node.
   *
   * @param NodeInterface $node  The node to get a value from
   * @param string        $field The field to get the value for
   *
   * @return null|string[] The values for a given date field and node or NULL in case the field is invalid
   */
  private function getFieldDateValues(NodeInterface $node, string $field): ?array
  {
    try {
      return array_filter(array_map(function(array $value): ?string {
        $dateString = $value['value'] ?? NULL;

        if (is_null($dateString)) {
          return NULL;
        }

        $dateTime = DateTime::createFromFormat(XsSearchableContent::NODE_DATE_FORMAT, $dateString) ?: NULL;

        return $dateTime?->format(XsSolr::DATETIME_FORMAT);
      }, $node->get($field)->getValue()));
    } catch (InvalidArgumentException) {
      return NULL;
    }
  }

  /**
   * Gets the string value for a given field and node and return an array that contains the string value split on the
   * given delimiter.
   *
   * @param NodeInterface $node      The node to get a value from
   * @param string        $field     The field to get the value for
   * @param string        $separator The delimiter to split the string value on
   *
   * @return null|string[] The values for a given field and node or NULL in case the field is invalid
   *
   * @see self::getFieldStringValue()
   */
  private function getFieldStringValueAsAnArray(NodeInterface $node, string $field, string $separator = ','): ?array
  {
    if (0 === strlen($separator)) {
      $separator = ',';
    }

    return !is_null($value = $this->getFieldStringValue($node, $field))
      ? array_filter(explode($separator, $value))
      : NULL;
  }
}
