<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Dictionary;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_search\XsSearch;
use Drupal\xs_searchable_content\XsSearchableContent;
use Drupal\xs_solr\Solr\Collection;
use Drupal\xs_solr\Solr\SolrClient;

/**
 * Class DictionaryIndexationService.
 *
 * A Drupal service that enables dictionaries to be indexed into an Apache Solr search-engine.
 */
final class DictionaryIndexationService
{
  /**
   * Creates a DictionaryIndexationService instance based on the given input.
   *
   * @param SolrClient                    $solrClient    The SolrClient to assign to the service
   * @param ConfigFactoryInterface        $configFactory The configuration factory to retrieve the config
   * @param LoggerChannelFactoryInterface $loggerFactory The logging factory to retrieve the logger
   *
   * @return DictionaryIndexationService The created implementation
   */
  public static function create(SolrClient $solrClient, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $loggerFactory): DictionaryIndexationService
  {
    return new DictionaryIndexationService(
      $solrClient->collection($configFactory->get(XsSearch::SETTINGS_KEY)->get('search_collection') ?? ''),
      $configFactory->get(XsSearchableContent::SETTINGS_KEY)->get('schema_field_content_type') ?? '',
      $loggerFactory->get(XsSearchableContent::LOG_CHANNEL)
    );
  }

  /**
   * DictionaryIndexationService constructor.
   *
   * @param Collection             $solrCollection   The collection in which dictionaries are (or should be) indexed
   * @param string                 $contentTypeField The name of the field that describes the type of dictionary in the
   *                                                 search-engine
   * @param LoggerChannelInterface $logger           The logging implementation to use
   */
  public function __construct(private readonly Collection $solrCollection, private readonly string $contentTypeField,
                              private readonly LoggerChannelInterface $logger)
  {
  }

  /**
   * Add (or update existing) dictionaries to the search engine.
   *
   * @param array<int, array<string, mixed>>     $dictionaries                The dictionaries to index in the search engine
   * @param DictionaryToDocumentServiceInterface $dictionaryToDocumentService The service that translates dictionaries to documents
   * @param bool                                 $commit                      Whether to commit the changes in the search-engine
   */
  public function indexDictionaries(array $dictionaries, DictionaryToDocumentServiceInterface $dictionaryToDocumentService,
                                    bool $commit = TRUE): void
  {
    if (!$this->isHealthy()) {
      return;
    }

    $documents = array_map(function(array $dictionary) use ($dictionaryToDocumentService) {
      return $dictionaryToDocumentService->mapDictionaryToSolrDocument($dictionary);
    }, $dictionaries);

    $solrResponse = $this->solrCollection->update(array_values($documents), $commit);

    if (!$solrResponse) {
      $this->logger->error(sprintf(
        'Failed to index one or more documents (total %s) in the search-engine',
        count($documents)
      ));
    }
  }

  /**
   * Remove a given dictionary from the search engine.
   *
   * @param array                                $dictionary                  The dictionary to remove from the search engine
   * @param DictionaryToDocumentServiceInterface $dictionaryToDocumentService The service that translates dictionaries to documents
   * @param string                               $contentType                 The content type of the dictionary
   * @param bool                                 $commit                      Whether to commit the changes in the search-engine
   */
  public function deleteDictionaryFromIndex(array $dictionary,
                                            DictionaryToDocumentServiceInterface $dictionaryToDocumentService,
                                            string $contentType,
                                            bool $commit = TRUE): void
  {
    if (!$this->isHealthy()) {
      return;
    }

    $dictionaryId       = $dictionary['id'] ?? 'unknown';
    $documentIdentifier = $dictionaryToDocumentService->getSolrDocumentIdentifier($dictionary);
    $solrResponse       = $this->solrCollection->update([
      'delete' => [
        'query' => sprintf(
          '(id:"%s" OR machine_name:"%s") AND %s:%s',
          $documentIdentifier,
          $documentIdentifier,
          $this->contentTypeField,
          $contentType
        ),
      ],
    ], $commit);

    if ($solrResponse) {
      $this->logger->info(sprintf('Dictionary "%s" has been removed from the search-engine', $dictionaryId));
    } else {
      $this->logger->error(sprintf('Failed to remove dictionary "%s" from the search-engine', $dictionaryId));
    }
  }

  /**
   * Query the search engine to determine how many documents in the index match the given content-type.
   *
   * @param string $contentType The machine_name of the content-type
   *
   * @return int The amount of documents in the index that match the content-type
   */
  public function getIndexedDictionariesCount(string $contentType): int
  {
    if (!$this->isHealthy()) {
      return 0;
    }

    $solrResponse = $this->solrCollection->query(
      filters: [sprintf('%s:"%s"', $this->contentTypeField, $contentType)],
      rows: 0
    );

    $numFound = $solrResponse?->getResultSet()?->numFound();

    if (is_null($numFound)) {
      $this->logger->warning(sprintf(
        'Failed to determine how many dictionaries of type "%s" are indexed in the search-engine',
        $contentType
      ));
    }

    return $numFound ?? 0;
  }

  /**
   * Remove all documents from the index of this content-type.
   *
   * @param string $contentType The machine_name of the content-type
   * @param bool   $commit      Whether to commit the changes in the search-engine
   */
  public function deleteContentTypeFromIndex(string $contentType, bool $commit = TRUE): void
  {
    if (!$this->isHealthy()) {
      return;
    }

    $solrResponse = $this->solrCollection->update([
      'delete' => ['query' => sprintf('%s:"%s"', $this->contentTypeField, $contentType)],
    ], $commit);

    if ($solrResponse) {
      $this->logger->info(sprintf('Content-type "%s" has been cleared from the search-engine', $contentType));
    } else {
      $this->logger->error(sprintf('Failed to delete content-type "%s" from the search-engine', $contentType));
    }
  }

  /**
   * Determine if this service is in a healthy state.
   *
   * @return bool If the service is healthy
   */
  private function isHealthy(): bool
  {
    $errorCount = 0;

    if (empty($this->solrCollection->getName())) {
      $this->logger->warning('DictionaryIndexationService is in an unhealthy state; solr_collection is empty.');
      ++$errorCount;
    }

    if (empty($this->contentTypeField)) {
      $this->logger->warning('DictionaryIndexationService is in an unhealthy state; content_type_field is empty.');
      ++$errorCount;
    }

    return 0 === $errorCount;
  }
}
