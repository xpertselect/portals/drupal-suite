<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Dictionary;

/**
 * Interface DictionaryToDocumentServiceInterface.
 *
 * Contract for defining how a dictionary should be mapped to the schema of the Solr search engine. Each implementation should
 * only provide a mapping for a single type of dictionaries (unless several types of dictionaries exist with identical mappings).
 */
interface DictionaryToDocumentServiceInterface
{
  /**
   * Get the document ID for a given dictionaries. The document ID is the ID of the node in the search engine (which may not be
   * same as the $dictionary['id']!).
   *
   * @param array<string, mixed> $dictionary The dictionary to get the ID for
   *
   * @return string The ID of the dictionary in the search engine
   */
  public function getSolrDocumentIdentifier(array $dictionary): string;

  /**
   * Extract the contents from a given dictionary, map it to the schema of the search engine and return the mapped data.
   *
   * @param array<string, mixed> $dictionary The dictionary to map
   *
   * @return array<string, mixed> The contents of the dictionary mapped to the search-engine fields
   */
  public function mapDictionaryToSolrDocument(array $dictionary): array;
}
