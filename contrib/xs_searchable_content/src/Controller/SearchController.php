<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Drupal\xs_lists\ListClient;
use Drupal\xs_search\PaginationService;
use Drupal\xs_search\QueryString\ParsedQuery;
use Drupal\xs_search\QueryString\QueryStringParser;
use Drupal\xs_search\Response\CorrectedSpellingResponse;
use Drupal\xs_search\Response\Response;
use Drupal\xs_search\SearchService;
use Drupal\xs_searchable_content\Event\PreparingSearchQuery;
use Drupal\xs_searchable_content\Services\Search\SearchFiltersTrait;
use Drupal\xs_searchable_content\Services\Search\SearchProfile;
use Drupal\xs_searchable_content\XsSearchableContent;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SearchController.
 *
 * Provides search functionality.
 */
class SearchController extends ControllerBase
{
  use SearchFiltersTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchService $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SearchProfile $searchProfile */
    $searchProfile = $container->get('xs_searchable_content.search_profile.content');

    /** @var EventDispatcherInterface $dispatcher */
    $dispatcher = $container->get('event_dispatcher');

    /** @var QueryStringParser $parser */
    $parser = $container->get('xs_search.query_string_parser');

    /** @var ListClient $listClient */
    $listClient = $container->get('xs_lists.list_client');

    /** @var ThemeManagerInterface $themeManager */
    $themeManager = $container->get('theme.manager');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    return new self(
      $searchService,
      $searchProfile,
      $dispatcher,
      $parser,
      $listClient,
      $themeManager->getActiveTheme()->getName(),
      'xs_searchable_content.web.search',
      'xs_searchable_content.web.search.index',
      $configFactory->get(XsSearchableContent::SETTINGS_KEY)->get('schema_field_content_type') ?? '',
      $configFactory->get(XsSearchableContent::FACET_MAPPING_KEY)->get('facet_mapping')        ?? []
    );
  }

  /**
   * SearchController constructor.
   *
   * @param SearchService                                     $searchService           The service providing the search functionality
   * @param SearchProfile                                     $searchProfile           The profile detailing how the search actions should be set
   * @param EventDispatcherInterface                          $dispatcher              An event dispatcher used for dispatching a searchContentEvent
   * @param QueryStringParser                                 $parser                  The query string parser
   * @param ListClient                                        $listClient              The list client for translating facets
   * @param string                                            $activeTheme             The active Drupal theme
   * @param string                                            $searchUrl               The search url
   * @param string                                            $searchHomeUrl           the search home url
   * @param string                                            $contentTypeField        The field in which the content-type is stored in the index
   * @param array<string, array{name: string, label: string}> $facetTranslationMapping The mapping for transforming a facet name into its label
   */
  public function __construct(private readonly SearchService $searchService,
                              protected SearchProfile $searchProfile,
                              protected readonly EventDispatcherInterface $dispatcher,
                              protected readonly QueryStringParser $parser, protected readonly ListClient $listClient,
                              protected readonly string $activeTheme, protected readonly string $searchUrl,
                              protected readonly string $searchHomeUrl, protected readonly string $contentTypeField,
                              protected readonly array $facetTranslationMapping)
  {
  }

  /**
   * Executes a search action and renders the result on the screen.
   *
   * @param Request $request     The HTTP request
   * @param string  $query       The query given in the URL
   * @param string  $filters     The filters given in the URL
   * @param int     $page_number The pageNumber given in the URL
   *
   * @return array<string, mixed> The render array
   */
  public function search(Request $request, string $query, string $filters, int $page_number): array
  {
    $this->dispatcher->dispatch(new PreparingSearchQuery($this->searchProfile));

    $parsedQuery = $this->parser->parse($query, $filters, $page_number, $this->searchProfile);
    $start       = ($parsedQuery->getPageNumber() - 1) * $this->searchProfile->resultsPerPage();
    $results     = $this->searchService->search(
      $this->searchProfile,
      $parsedQuery->getQuery(),
      $parsedQuery->getFlattenedFilters(),
      $start,
      $this->searchProfile->sort($parsedQuery),
      $request->get('no_autocorrect') !== '1'
    );

    if (is_null($results)) {
      throw new NotFoundHttpException();
    }

    $solrResponse   = $results->getSolrResponse();
    $searchResponse = $solrResponse->getResultSet();
    $facetResponse  = $solrResponse->getFacets();

    if (is_null($searchResponse) || is_null($facetResponse) || $start > $searchResponse->numFound()) {
      throw new NotFoundHttpException();
    }

    return [
      '#theme'                          => 'search_results_wrapper',
      '#active_theme'                   => $this->activeTheme,
      '#search_current'                 => $this->parser,
      '#search_result_count'            => $searchResponse->numFound(),
      '#search_results'                 => $this->searchProfile->getFormattedDocuments($searchResponse, $solrResponse->getHighlighting()),
      '#search_facets'                  => $this->createFacetJSON($parsedQuery, $results, $facetResponse->getRawData()['facet_fields']),
      '#active_filters'                 => $this->createActiveFiltersList($parsedQuery, $results, $facetResponse->getRawData()['facet_fields']),
      '#search_facets_string'           => $this->cleanFacetFilterString($this->getReusableActiveFilterString($parsedQuery)),
      '#search_query'                   => $results->getQuery(),
      '#search_query_string'            => $this->getReusableQuery($parsedQuery, $results),
      '#search_highlighting'            => $solrResponse->getHighlighting(),
      '#search_pagination'              => new PaginationService(
        $parsedQuery->getPageNumber(),
        $searchResponse->numFound(),
        $this->searchProfile->resultsPerPage()
      ),
      '#search_suggestion'     => $results->getSuggestion(),
      '#search_original_query' => $results instanceof CorrectedSpellingResponse
        ? $results->getOriginalQuery()
        : NULL,
      '#search_original_result_count' => $results instanceof CorrectedSpellingResponse
        ? $results->getOriginalCount()
        : 0,
      '#clear_filters_url'   => Url::fromRoute($this->searchUrl, [
        'query'       => $this->getReusableQuery($parsedQuery, $results),
        'filters'     => '-',
        'page_number' => 1,
      ])->toString(),
      '#attached'            => [
        'http_header' => $this->getSearchHtmlHeaders($request),
      ],
      '#cache'               => [
        'keys'     => [
          'search_results',
          'search_query:' . $parsedQuery->getQuery(),
          'search_suggestion:' . ($results->getSuggestion() ?? ''),
          'search_filters:' . $parsedQuery->getFiltersAsString(),
          'search_page:' . $parsedQuery->getPageNumber(),
          'user:' . $this->currentUser()->id(),
        ],
        'contexts' => ['user'],
        'tags'     => array_merge(
          [
            'search_results',
            'user:' . $this->currentUser()->id(),
          ],
          $this->getCacheTypes($parsedQuery)
        ),
        'max-age'  => CacheBackendInterface::CACHE_PERMANENT,
      ],
    ];
  }

  /**
   * Determine the pageTitle from the given input.
   *
   * @param string $query       The query given in the URL
   * @param string $filters     The filters given in the URL
   * @param int    $page_number The pageNumber given in the URL
   *
   * @return string The title for this page
   */
  public function getTitle(string $query, string $filters, int $page_number): string
  {
    $parsedQuery = $this->parser->parse($query, $filters, $page_number, $this->searchProfile);
    $title       = '';

    if (!$parsedQuery->hasDefaultQuery()) {
      $title .= $this->t('Search "@query"', ['@query' => $query]);
    }

    if ($parsedQuery->hasFilters()) {
      $filters = count($parsedQuery->getFilters());

      if (1 === $filters) {
        $title .= $this->t('@filters Filter, ', ['@filters' => $filters]);
      } else {
        $title .= $this->t('@filters Filters, ', ['@filters' => $filters]);
      }
    }

    $title .= $this->t('Page @page', ['@page' => $parsedQuery->getPageNumber()]);

    return $title;
  }

  /**
   * Transforms the given Solr facets from the format:.
   *
   * ```
   * {
   *   {facet field 1}: {
   *     {facet value 1}: {facet count 1},
   *     {facet value 2}: {facet count 2},
   *     ..
   *   },
   *   ..
   * }
   * ```
   *
   * Into the following JSON encoded array:
   *
   * ```
   * [
   *   {
   *     'title': {translated facet 1},
   *     'facets': [
   *       {
   *         'title': {translated facet value 1} ({facet count 1}),
   *         'url': {url to search page with (or without) filter active}
   *         'active': {boolean, true if filter currently active}
   *       },
   *       {
   *         'title': {translated facet value 2} ({facet count 2}),
   *         'url': {url to search page with (or without) filter active}
   *         'active': {boolean, true if filter currently active}
   *       },
   *       ..
   *     }
   *   }
   * ]
   * ```
   *
   * @param ParsedQuery                       $parsedQuery      The incoming search query
   * @param Response                          $results          The results from Solr
   * @param array<string, array<string, int>> $facetsDictionary The JSON decoded Solr facets
   *
   * @return array<int, array<string, mixed>> The facet data ready for use by the template
   */
  protected function createFacetJSON(ParsedQuery $parsedQuery, Response $results, array $facetsDictionary): array
  {
    $facetMapping     = array_flip($this->searchProfile->facetMapping());
    $activeQuery      = $this->getReusableQuery($parsedQuery, $results);
    $activeFilters    = $this->getReusableActiveFilterString($parsedQuery);
    $facets           = [];

    foreach ($facetsDictionary as $facetName => $facetList) {
      if (empty($facetList)) {
        continue;
      }

      $list         = $this->listClient->list($facetName);
      $currentFacet = [
        'title'  => $this->t($this->translateFacetName($facetName), [], ['context' => 'facet']),
        'facets' => [],
      ];

      foreach ($facetList as $facetKey => $facetCount) {
        $splitKey = explode('|', strval($facetKey));
        $facetKey = $splitKey[count($splitKey) - 1];

        if ($list->entryHasField($facetKey, 'parent') && 'facet_theme' === $facetName) {
          continue;
        }

        $isActive        = FALSE;
        $newFilter       = $activeFilters;
        $translatedValue = $list->identifierToLabel($facetKey);
        $filterPattern   = $facetMapping[$facetName] . ':' . $translatedValue;

        if (str_contains($newFilter, $filterPattern . ';')) {
          $newFilter = str_replace($filterPattern . ';', '', $newFilter);

          if (empty($newFilter)) {
            $newFilter = '-';
          }

          $isActive = TRUE;
        } else {
          $newFilter .= $filterPattern;
        }

        $facetUrl = Url::fromRoute($this->searchUrl, [
          'query'       => $activeQuery,
          'filters'     => $newFilter,
          'page_number' => 1,
        ])->toString();

        $currentFacet['facets'][] = [
          'label'  => ucfirst($translatedValue),
          'amount' => $facetCount,
          'url'    => $facetUrl,
          'active' => $isActive,
        ];
      }

      $facets[] = $currentFacet;
    }

    return $facets;
  }

  /**
   * Formats active facets.
   *
   * @param ParsedQuery                       $parsedQuery      The incoming search query
   * @param Response                          $results          The results from Solr
   * @param array<string, array<string, int>> $facetsDictionary The JSON decoded Solr facets
   *
   * @return array<string, GeneratedUrl|string> The facet data ready for use by the template
   */
  protected function createActiveFiltersList(ParsedQuery $parsedQuery, Response $results,
                                             array $facetsDictionary): array
  {
    $facetMapping  = array_flip($this->searchProfile->facetMapping());
    $activeFilters = $this->getReusableActiveFilterString($parsedQuery);
    $activeQuery   = $this->getReusableQuery($parsedQuery, $results);
    $facets        = [];

    foreach ($facetsDictionary as $facetName => $facetList) {
      if (empty($facetList)) {
        continue;
      }

      $list = $this->listClient->list($facetName);

      foreach ($facetList as $facetKey => $facetCount) {
        $splitKey = explode('|', strval($facetKey));
        $facetKey = $splitKey[count($splitKey) - 1];

        if ($list->getTitle() === 'donl:Theme' && $list->entryHasField($facetKey, 'parent')) {
          continue;
        }

        $translatedValue = $list->identifierToLabel($facetKey);
        $filterPattern   = $facetMapping[$facetName] . ':' . $translatedValue;

        $newFilter = str_replace($filterPattern . ';', '', $activeFilters);

        if (str_contains($activeFilters, $filterPattern . ';')) {
          $facets[ucfirst($translatedValue)] = Url::fromRoute($this->searchUrl, [
            'query'       => $activeQuery,
            'filters'     => $this->cleanFacetFilterString($newFilter),
            'page_number' => 1,
          ])->toString();
        }
      }
    }

    return $facets;
  }

  /**
   * Determine the query to use for generating new search URLs.
   *
   * @param ParsedQuery $parsedQuery The incoming search query
   * @param Response    $results     The current search results being shown
   *
   * @return string The query-string
   */
  protected function getReusableQuery(ParsedQuery $parsedQuery, Response $results): string
  {
    $query = $results instanceof CorrectedSpellingResponse
      ? $results->getQuery()
      : $parsedQuery->getQuery();

    if (empty($query) || $parsedQuery->hasDefaultQuery()) {
      $query = '-';
    }

    return $query;
  }

  /**
   * Translates the machine name of a facet to its proper English variant.
   *
   * @param string $facet The facet to translate
   *
   * @return string The translated facet, if no translation could be made the original facet will be returned
   */
  protected function translateFacetName(string $facet): string
  {
    if (array_key_exists($facet, $this->facetTranslationMapping)) {
      return $this->facetTranslationMapping[$facet]['label'];
    }

    return $facet;
  }

  /**
   * Clean the facetFilterString by ensuring that it has a sensible default when empty and removing any trailing ';'
   * characters.
   *
   * @param string $facetFilterString The string to clean
   *
   * @return string The clean string
   */
  protected function cleanFacetFilterString(string $facetFilterString): string
  {
    if (empty($facetFilterString)) {
      return '-';
    }

    if (FALSE !== mb_strpos($facetFilterString, ';', -1)) {
      $facetFilterString = mb_substr($facetFilterString, 0, -1);
    }

    return $facetFilterString;
  }

  /**
   * Determine and return the HTML headers to append to response.
   *
   * @param Request $request The incoming HTTP request
   *
   * @return array<int, string[]> The HTML headers to append to the response
   */
  protected function getSearchHtmlHeaders(Request $request): array
  {
    $routeMatch = RouteMatch::createFromRequest($request);

    return $routeMatch->getRouteName() !== $this->searchHomeUrl
      ? [['X-Robots-Tag', 'noindex']]
      : [];
  }

  /**
   * Get all types or the contents of the type filter.
   *
   * @param ParsedQuery $parsedQuery The parsed query
   *
   * @return string[] All types or the contents of the type filter
   */
  private function getCacheTypes(ParsedQuery $parsedQuery): array
  {
    if ($parsedQuery->hasFilters()) {
      $filters = $parsedQuery->getFilters();

      foreach ($filters as $filter) {
        if (sprintf('facet_%s', $this->contentTypeField) === $filter['field']) {
          return [$filter['value']];
        }
      }
    }

    return $this->searchProfile->getTypes();
  }
}
