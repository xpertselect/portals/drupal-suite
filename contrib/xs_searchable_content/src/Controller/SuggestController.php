<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\xs_search\SearchService;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Drupal\xs_searchable_content\Services\Search\SuggestProfile;
use Drupal\xs_solr\Solr\Search\Document;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SuggestController.
 *
 * Provides suggest functionality for searchable content types.
 */
final class SuggestController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchService $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SuggestProfile $searchProfile */
    $searchProfile = $container->get('xs_searchable_content.suggest_profile.content');

    /** @var EventDispatcherInterface $dispatcher */
    $dispatcher = $container->get('event_dispatcher');

    return new self($searchService, $searchProfile, $dispatcher);
  }

  /**
   * SuggestController constructor.
   *
   * @param SearchService            $searchService The service providing the search functionality
   * @param SuggestProfile           $searchProfile The profile detailing how the search actions should be set
   * @param EventDispatcherInterface $dispatcher    An event dispatcher used for dispatching a suggestContentEvent
   */
  public function __construct(private readonly SearchService $searchService, private SuggestProfile $searchProfile,
                              private readonly EventDispatcherInterface $dispatcher)
  {
  }

  /**
   * Executes a suggest action and returns a simple JSON response.
   *
   * @param string $query The query to return suggestions for
   *
   * @return JsonResponse The results in JSON
   */
  public function suggestContentOSDD(string $query): JsonResponse
  {
    return new JsonResponse([
      $query,
      array_map(function(array $suggestion): string {
        return strip_tags($suggestion['title']);
      }, $this->getSuggestions($query)),
    ]);
  }

  /**
   * Executes a suggest action and returns a JSON response.
   *
   * @param string $query The query to return suggestions for
   *
   * @return JsonResponse The results in JSON
   */
  public function suggestContent(string $query): JsonResponse
  {
    return new JsonResponse($this->getSuggestions($query));
  }

  /**
   * Autocomplete method.
   *
   * Returns a JsonResponse with autocomplete suggestions based on the given query.
   *
   * @param Request $request The incoming HTTP Request
   * @param string  $type    The type of autocomplete suggestions to retrieve
   *
   * @return JsonResponse The JsonResponse object with the autocomplete suggestions
   */
  public function autocomplete(Request $request, string $type): JsonResponse
  {
    $query = Xss::filter($request->query->get('q') ?? '');

    if (!$query) {
      return new JsonResponse([], 400);
    }

    $this->searchProfile->setAdditionalParameters(['type' => $type]);

    $docs = array_map(function(array $document) {
      return [
        'value' => strip_tags($document['title']),
        'label' => $document['title'],
      ];
    }, $this->getSuggestions($query));

    return new JsonResponse($docs);
  }

  /**
   * Returns the list of suggestions.
   *
   * @param string $query The query to return suggestions for
   *
   * @return array<int, array<string, mixed>> The list of suggestions
   */
  private function getSuggestions(string $query): array
  {
    $this->dispatcher->dispatch(new PreparingSuggestQuery($this->searchProfile));

    $result = $this->searchService->search(
      $this->searchProfile,
      $query,
      [],
      0,
      $this->searchProfile->sort(),
      FALSE,
    );

    $resultSet = $result?->getSolrResponse()->getResultSet();

    if (is_null($resultSet)) {
      return [];
    }

    return array_filter(array_map(function(Document $document) use ($result) {
      return $this->searchProfile->mapResponseDocument($document, $result);
    }, $resultSet->documents()));
  }
}
