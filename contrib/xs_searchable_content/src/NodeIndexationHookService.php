<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content;

use Drupal;
use Drupal\node\NodeInterface;

/**
 * Class NodeIndexationHookService.
 *
 * A Drupal service for triggering the NodeIndexationService when Node hooks are fired.
 */
final class NodeIndexationHookService
{
  /**
   * Trigger the NodeIndexationService after a Node has been created.
   *
   * @param NodeInterface $node               The node that was created
   * @param string        $mappingServiceName The mapping service name to map Node contents to the search schema
   * @param bool          $commit             Whether to commit the changes to the search-index
   * @param bool          $indexUnpublished   Whether to index unpublished nodes
   */
  public static function onCreationHook(NodeInterface $node, string $mappingServiceName, bool $commit = TRUE, bool $indexUnpublished = FALSE): void
  {
    if (!$node->isPublished() && !$indexUnpublished) {
      return;
    }

    /** @var NodeIndexationService $nodeIndexationService */
    $nodeIndexationService = Drupal::service('xs_searchable_content.node_indexation_service');

    /** @var NodeToDocumentServiceInterface $nodeToDocumentService */
    $nodeToDocumentService = Drupal::service($mappingServiceName);

    $nodeIndexationService->indexNodes([$node], $nodeToDocumentService, $commit);
  }

  /**
   * Trigger the NodeIndexationService after a Node has been updated.
   *
   * @param NodeInterface $node               The node that was updated
   * @param string        $mappingServiceName The mapping service name to map Node contents to the search schema
   * @param bool          $commit             Whether to commit the changes to the search-index
   * @param bool          $indexUnpublished   Whether to index unpublished nodes
   */
  public static function onUpdateHook(NodeInterface $node, string $mappingServiceName, bool $commit = TRUE, bool $indexUnpublished = FALSE): void
  {
    /** @var NodeIndexationService $nodeIndexationService */
    $nodeIndexationService = Drupal::service('xs_searchable_content.node_indexation_service');

    /** @var NodeToDocumentServiceInterface $nodeToDocumentService */
    $nodeToDocumentService = Drupal::service($mappingServiceName);

    if (!$node->isPublished() && !$indexUnpublished) {
      $nodeIndexationService->deleteNodeFromIndex($node, $nodeToDocumentService, $commit);
    } else {
      $nodeIndexationService->indexNodes([$node], $nodeToDocumentService, $commit);
    }
  }

  /**
   * Trigger the NodeIndexationService after a Node has been deleted.
   *
   * @param NodeInterface $node               The node that was deleted
   * @param string        $mappingServiceName The mapping service name to map Node contents to the search schema
   * @param bool          $commit             Whether to commit the changes to the search-index
   */
  public static function onDeleteHook(NodeInterface $node, string $mappingServiceName, bool $commit = TRUE): void
  {
    /** @var NodeIndexationService $nodeIndexationService */
    $nodeIndexationService = Drupal::service('xs_searchable_content.node_indexation_service');

    /** @var NodeToDocumentServiceInterface $nodeToDocumentService */
    $nodeToDocumentService = Drupal::service($mappingServiceName);

    $nodeIndexationService->deleteNodeFromIndex($node, $nodeToDocumentService, $commit);
  }
}
