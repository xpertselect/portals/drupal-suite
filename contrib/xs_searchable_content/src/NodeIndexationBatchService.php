<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content;

use Drupal;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Class NodeIndexationBatchService.
 *
 * Provides the methods for processing a Node indexation Drupal batch job.
 */
final class NodeIndexationBatchService
{
  /**
   * Send a given batch of Node IDs to the search engine for indexation.
   *
   * @param int[]                $data               The Node IDs that should be processed in this batch
   * @param string               $mappingServiceName The mapping service name to map Node contents to the search schema
   * @param bool                 $commit             Whether to commit the changes to the search-index
   * @param array<string, mixed> $context            The batch context data
   */
  public static function processBatch(array $data, string $mappingServiceName, bool $commit, array &$context): void
  {
    /** @var EntityStorageInterface $nodeStorage */
    $nodeStorage = Drupal::service('entity_type.manager')->getStorage('node');

    /** @var NodeIndexationService $nodeIndexationService */
    $nodeIndexationService = Drupal::service('xs_searchable_content.node_indexation_service');

    /** @var NodeToDocumentServiceInterface $nodeToDocumentService */
    $nodeToDocumentService = Drupal::service($mappingServiceName);

    $nodeIndexationService->indexNodes($nodeStorage->loadMultiple($data), $nodeToDocumentService, $commit);

    $context['results'][] = count($data);
  }

  /**
   * Finalize the execution of the indexing batch job.
   *
   * @param mixed                    $success    Whether the batch execution was successful
   * @param array<string|int, mixed> $results    The results of each individual batch
   * @param array<string|int, mixed> $operations Any leftover batches, in case of an error
   */
  public static function finalizeBatch(mixed $success, array $results, array $operations): void
  {
    if ($success) {
      $sum = 0;

      foreach ($results as $result) {
        if (!is_int($result)) {
          continue;
        }

        $sum = $sum + $result;
      }

      Drupal::messenger()->addMessage(t('@count items were sent to the search engine for indexing.', [
        '@count' => $sum,
      ]));
    } else {
      Drupal::messenger()->addMessage(t('An error occurred during indexing. @count batches of items were not indexed.', [
        '@count' => count($operations),
      ]));
    }
  }
}
