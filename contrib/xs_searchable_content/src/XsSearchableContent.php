<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content;

/**
 * Class XsSearchableContent.
 *
 * Simple PHP class holding various constants used by the `xs_searchable_content` Drupal module.
 */
final class XsSearchableContent
{
  /**
   * The key identifying the `xs_searchable_content` settings in Drupal.
   *
   * @var string
   */
  public const SETTINGS_KEY = 'xs_searchable_content.settings';

  /**
   * The key identifying the `xs_searchable_content` facet settings in Drupal.
   *
   * @var string
   */
  public const FACET_MAPPING_KEY = 'xs_searchable_content.facet-mapping';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'xs_searchable_content';

  /**
   * The format used by date fields of a Drupal Node.
   */
  public const NODE_DATE_FORMAT = 'Y-m-d';

  /**
   * The default batch size to use for batch indexing content when, for
   * example, batch size is not configured in the settings.
   *
   * @var int
   */
  public const DEFAULT_INDEX_BATCH_SIZE = 25;
}
