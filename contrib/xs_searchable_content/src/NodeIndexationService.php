<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_search\XsSearch;
use Drupal\xs_solr\Solr\Collection;
use Drupal\xs_solr\Solr\SolrClient;

/**
 * Class NodeIndexationService.
 *
 * A Drupal service that enables nodes to be indexed into an Apache Solr search-engine.
 */
final class NodeIndexationService
{
  /**
   * Creates a NodeIndexationService instance based on the given input.
   *
   * @param SolrClient                    $solrClient    The SolrClient to assign to the service
   * @param ConfigFactoryInterface        $configFactory The configuration factory to retrieve the config
   * @param LoggerChannelFactoryInterface $loggerFactory The logging factory to retrieve the logger
   *
   * @return NodeIndexationService The created implementation
   */
  public static function create(SolrClient $solrClient, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $loggerFactory): NodeIndexationService
  {
    return new NodeIndexationService(
      $solrClient->collection($configFactory->get(XsSearch::SETTINGS_KEY)->get('search_collection') ?? ''),
      $configFactory->get(XsSearchableContent::SETTINGS_KEY)->get('schema_field_content_type') ?? '',
      $loggerFactory->get(XsSearchableContent::LOG_CHANNEL)
    );
  }

  /**
   * NodeIndexationService constructor.
   *
   * @param Collection             $solrCollection   The collection in which nodes are (or should be) indexed
   * @param string                 $contentTypeField The name of the field that describes the type of node in the
   *                                                 search-engine
   * @param LoggerChannelInterface $logger           The logging implementation to use
   */
  public function __construct(private readonly Collection $solrCollection, private readonly string $contentTypeField,
                              private readonly LoggerChannelInterface $logger)
  {
  }

  /**
   * Add (or update existing) nodes to the search engine.
   *
   * @param EntityInterface[]              $nodes                 The nodes to index in the search engine
   * @param NodeToDocumentServiceInterface $nodeToDocumentService The service that translates nodes to documents
   * @param bool                           $commit                Whether to commit the changes in the search-engine
   */
  public function indexNodes(array $nodes, NodeToDocumentServiceInterface $nodeToDocumentService,
                             bool $commit = TRUE): void
  {
    if (!$this->isHealthy()) {
      return;
    }

    $documents = array_map(function(EntityInterface $node) use ($nodeToDocumentService) {
      return $nodeToDocumentService->mapNodeToSolrDocument($node);
    }, $nodes);

    $solrResponse = $this->solrCollection->update(array_values($documents), $commit);

    if (!$solrResponse) {
      $this->logger->error(sprintf(
        'Failed to index one or more documents (total %s) in the search-engine',
        count($documents)
      ));
    }
  }

  /**
   * Remove a given node from the search engine.
   *
   * @param EntityInterface                $node                  The node to remove from the search engine
   * @param NodeToDocumentServiceInterface $nodeToDocumentService The service that translates nodes to documents
   * @param bool                           $commit                Whether to commit the changes in the search-engine
   */
  public function deleteNodeFromIndex(EntityInterface $node, NodeToDocumentServiceInterface $nodeToDocumentService,
                                      bool $commit = TRUE): void
  {
    if (!$this->isHealthy()) {
      return;
    }

    $nodeId       = $node->id() ?? 'unknown';
    $solrResponse = $this->solrCollection->update([
      'delete' => ['id' => $nodeToDocumentService->getSolrDocumentIdentifier($node)],
    ], $commit);

    if ($solrResponse) {
      $this->logger->info(sprintf('Node "%s" has been removed from the search-engine', $nodeId));
    } else {
      $this->logger->error(sprintf('Failed to remove node "%s" from the search-engine', $nodeId));
    }
  }

  /**
   * Query the search engine to determine how many documents in the index match the given content-type.
   *
   * @param string $contentType The machine_name of the content-type
   *
   * @return int The amount of documents in the index that match the content-type
   */
  public function getIndexedNodesCount(string $contentType): int
  {
    if (!$this->isHealthy()) {
      return 0;
    }

    $solrResponse = $this->solrCollection->query(
      filters: [sprintf('%s:"%s"', $this->contentTypeField, $contentType)],
      rows: 0
    );

    $numFound = $solrResponse?->getResultSet()?->numFound();

    if (is_null($numFound)) {
      $this->logger->warning(sprintf(
        'Failed to determine how many nodes of type "%s" are indexed in the search-engine',
        $contentType
      ));
    }

    return $numFound ?? 0;
  }

  /**
   * Remove all documents from the index of this content-type.
   *
   * @param string $contentType The machine_name of the content-type
   * @param bool   $commit      Whether to commit the changes in the search-engine
   */
  public function deleteContentTypeFromIndex(string $contentType, bool $commit = TRUE): void
  {
    if (!$this->isHealthy()) {
      return;
    }

    $solrResponse = $this->solrCollection->update([
      'delete' => ['query' => sprintf('%s:"%s"', $this->contentTypeField, $contentType)],
    ], $commit);

    if ($solrResponse) {
      $this->logger->info(sprintf('Content-type "%s" has been cleared from the search-engine', $contentType));
    } else {
      $this->logger->error(sprintf('Failed to delete content-type "%s" from the search-engine', $contentType));
    }
  }

  /**
   * Determine if this service is in a healthy state.
   *
   * @return bool If the service is healthy
   */
  private function isHealthy(): bool
  {
    $errorCount = 0;

    if (empty($this->solrCollection->getName())) {
      $this->logger->warning('NodeIndexationService is in an unhealthy state; solr_collection is empty.');
      ++$errorCount;
    }

    if (empty($this->contentTypeField)) {
      $this->logger->warning('NodeIndexationService is in an unhealthy state; content_type_field is empty.');
      ++$errorCount;
    }

    return 0 === $errorCount;
  }
}
