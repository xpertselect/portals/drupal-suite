<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Event;

use Drupal\xs_searchable_content\Services\Search\SearchProfile;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class PreparingSearchQuery.
 *
 * An event that is emitted before querying the search-engine.
 */
class PreparingSearchQuery extends StoppableEvent
{
  /**
   * PreparingSearchQuery constructor.
   *
   * @param SearchProfile $searchProfile The searchProfile for retrieving the search results
   */
  public function __construct(public SearchProfile &$searchProfile)
  {
  }
}
