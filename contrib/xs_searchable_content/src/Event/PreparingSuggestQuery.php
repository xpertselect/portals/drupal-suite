<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Event;

use Drupal\xs_searchable_content\Services\Search\SuggestProfile;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class PreparingSuggestQuery.
 *
 * An event that is emitted before retrieving the suggestions.
 */
final class PreparingSuggestQuery extends StoppableEvent
{
  /**
   * PreparingSuggestQuery constructor.
   *
   * @param SuggestProfile $searchProfile The searchProfile for retrieving the suggestions
   */
  public function __construct(public SuggestProfile &$searchProfile)
  {
  }
}
