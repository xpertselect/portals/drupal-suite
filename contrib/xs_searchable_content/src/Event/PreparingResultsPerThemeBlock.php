<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Event;

use Drupal\xs_searchable_content\Services\Search\ThemeBlockSearchProfile;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class PreparingResultsPerThemeBlock.
 *
 * An event that is emitted before retrieving the suggestions.
 */
final class PreparingResultsPerThemeBlock extends StoppableEvent
{
  /**
   * PreparingResultsPerThemeBlock constructor.
   *
   * @param ThemeBlockSearchProfile $searchProfile The searchProfile for retrieving the amount per theme
   */
  public function __construct(public ThemeBlockSearchProfile &$searchProfile)
  {
  }
}
