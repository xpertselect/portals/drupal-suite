<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Services\Search;

use Drupal\xs_search\QueryString\ParsedQuery;

/**
 * Trait SearchFiltersTrait.
 *
 * Trait that exposes methods for handling search filters.
 */
trait SearchFiltersTrait
{
  /**
   * Determine the filter string to use for generating new search URLs.
   *
   * @param ParsedQuery $query The incoming search query
   *
   * @return string The base filter-string
   */
  protected function getReusableActiveFilterString(ParsedQuery $query): string
  {
    $activeFilters = $query->getFiltersAsString();

    if (!empty($activeFilters)) {
      $activeFilters .= ';';
    }

    return $activeFilters;
  }
}
