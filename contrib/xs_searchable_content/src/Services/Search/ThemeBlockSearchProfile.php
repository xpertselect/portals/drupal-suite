<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Services\Search;

use Drupal\xs_search\Services\Search\MutableSearchProfile;

/**
 * Class ThemeBlockSearchProfile.
 *
 * Search profile for finding all the themes for the homepage theme block.
 */
final class ThemeBlockSearchProfile extends MutableSearchProfile
{
  use TypeFilterTrait;

  /**
   * ThemeBlockSearchProfile constructor.
   */
  public function __construct()
  {
    parent::__construct(
      resultsPerPage: 0,
      additionalParameters: [
        'facet'                     => TRUE,
        'f.facet_theme.facet.limit' => -1,
        'f.facet_theme.facet.sort'  => 'index',
        'spellcheck'                => FALSE,
      ],
    );
  }
}
