<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Services\Search;

use Closure;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\xs_search\QueryString\ParsedQuery;
use Drupal\xs_search\Response\Response;
use Drupal\xs_search\Services\Search\MutableSearchProfile;
use Drupal\xs_searchable_content\XsSearchableContent;
use Drupal\xs_solr\Solr\Search\Document;
use Drupal\xs_solr\Solr\Search\Highlighting;
use Drupal\xs_solr\Solr\Search\ResultSet;

/**
 * Class SearchProfile.
 *
 * Search profile for getting all types of content.
 */
class SearchProfile extends MutableSearchProfile
{
  use TypeFilterTrait;

  /**
   * A dictionary containing a Solr to output mapper for each content type.
   *
   * @var array<string, Closure>
   */
  private array $converters = [];

  /**
   * Factory for creating a SearchProfile instance.
   *
   * @param ConfigFactoryInterface $configFactory The config for getting the field in which the content-type is stored
   *                                              in the index
   *
   * @return self The created SearchProfile instance
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    return new self(
      $configFactory->get(XsSearchableContent::SETTINGS_KEY)->get('schema_field_content_type') ?? ''
    );
  }

  /**
   * SearchProfile constructor.
   *
   * @param string $contentTypeField The field in which the content-type is stored in the index
   */
  public function __construct(private readonly string $contentTypeField)
  {
    parent::__construct(
      facetFieldList: ['facet_type'],
      additionalParameters: [
        'hl'             => TRUE,
        'hl.fl'          => 'description',
        'hl.snippets'    => 1,
        'hl.fragsize'    => 400,
        'hl.simple.pre'  => '<em>',
        'hl.simple.post' => '</em>',
        'hl.encoder'     => 'html',

        'spellcheck'                   => TRUE,
        'spellcheck.dictionary'        => 'spellchecker',
        'spellcheck.count'             => 1,
        'spellcheck.collate'           => TRUE,
        'spellcheck.maxCollationTries' => 1,

        'facet'          => TRUE,
        'facet.mincount' => 1,
      ],
      sortCallback: function(?ParsedQuery $parsedQuery = NULL): string {
        if (!is_null($parsedQuery) && $parsedQuery->hasDefaultQuery()) {
          return 'modified DESC';
        }

        return 'score DESC';
      },
      facetMapping: [
        'type' => 'facet_type',
      ],
    );
  }

  /**
   * Maps a solr document to the desired output format.
   *
   * @param Document $document The document returned by the search engine
   * @param Response $result   The entire search result for retrieving highlighting
   *
   * @return null|array<string, string>
   */
  public function mapResponseDocument(Document $document, Response $result): ?array
  {
    if (!isset($this->converters[$document->getStringField($this->contentTypeField)])) {
      return NULL;
    }

    return $this->converters[$document->getStringField($this->contentTypeField)]($document, $result);
  }

  /**
   * Adds an output mapper for a specific content type.
   *
   * @param string  $type    The content type
   * @param Closure $closure A function to map a solr document into the desired output format
   */
  public function addResponseTypeConverter(string $type, Closure $closure): void
  {
    $this->converters[$type] = $closure;
  }

  /**
   * Formats a ResultSet to an array of Documents.
   *
   * @param ResultSet         $searchResponse The ResultSet to format
   * @param null|Highlighting $highlighting   The Highlighting for retrieving highlighted snippets
   *
   * @return array<int, array<string, string|Document|Url>> The formatted documents
   */
  public function getFormattedDocuments(ResultSet $searchResponse, ?Highlighting $highlighting): array
  {
    return array_values(array_map(function(array $documentData) use ($highlighting): array {
      return $this->formatResponse($documentData, $highlighting);
    }, $searchResponse->getRawResponse()['docs'] ?? []));
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    $filters    = parent::standardFilters();
    $typeFilter = $this->getTypeFilter($this->contentTypeField);

    if (!is_null($typeFilter)) {
      $filters[] = $typeFilter;
    }

    return $filters;
  }

  /**
   * Format a document returned by solr.
   *
   * @param array             $document     The solr document to format
   * @param null|Highlighting $highlighting The Highlighting for retrieving highlighted snippets
   *
   * @return array The formatted solr document
   */
  private function formatResponse(array $document, ?Highlighting $highlighting): array
  {
    return $this->converters[$document[$this->contentTypeField]]($document, $highlighting);
  }
}
