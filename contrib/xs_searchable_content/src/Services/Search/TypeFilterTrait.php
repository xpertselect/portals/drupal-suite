<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Services\Search;

trait TypeFilterTrait
{
  /**
   * All types that should be filtered on.
   *
   * @var array<int, string> The fields that should be retrieved
   */
  protected array $types = [];

  /**
   * Creates a filter string from the added types.
   *
   * @param string $contentTypeField The Field where the type is stored in
   *
   * @return null|string The filter string or null when no types are set
   */
  public function getTypeFilter(string $contentTypeField): ?string
  {
    if (empty($this->types)) {
      return NULL;
    }

    return sprintf(
      '%s:("%s")',
      $contentTypeField,
      implode('" OR "', $this->types)
    );
  }

  /**
   * Add types to be filtered on.
   *
   * @param array<int, string> $types The fields to add
   */
  public function addTypes(array $types): void
  {
    $this->types = array_unique(array_merge($this->types, $types));
  }

  /**
   * Get all types to be filtered on.
   *
   * @return array<int, string> All types
   */
  public function getTypes(): array
  {
    return $this->types;
  }
}
