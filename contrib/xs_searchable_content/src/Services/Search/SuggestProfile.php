<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Services\Search;

use Closure;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_search\QueryString\ParsedQuery;
use Drupal\xs_search\Response\Response;
use Drupal\xs_search\Services\Search\MutableSearchProfile;
use Drupal\xs_searchable_content\XsSearchableContent;
use Drupal\xs_solr\Solr\Search\Document;

/**
 * Class SuggestProfile.
 *
 * Search profile for getting suggestions.
 */
final class SuggestProfile extends MutableSearchProfile
{
  use TypeFilterTrait;

  /**
   * @var array<string, closure> A map containing a solr to output mapper for each content type
   */
  private array $converters = [];

  /**
   * Factory for creating a SuggestProfile instance.
   *
   * @param ConfigFactoryInterface $configFactory The config for getting the field in which the content-type is
   *                                              stored in the index
   *
   * @return self The created SuggestProfile instance
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    return new self(
      $configFactory->get(XsSearchableContent::SETTINGS_KEY)->get('schema_field_content_type') ?? ''
    );
  }

  /**
   * SuggestProfile constructor.
   *
   * @param string $contentTypeField the field in which the content-type is stored in the index
   */
  public function __construct(private readonly string $contentTypeField)
  {
    parent::__construct(
      requestHandler: 'suggest',
      resultsPerPage: 5,
      additionalParameters: [
        'hl'             => TRUE,
        'hl.method'      => 'unified',
        'hl.fl'          => 'suggestion',
        'hl.snippets'    => 1,
        'hl.fragsize'    => 250,
        'hl.simple.pre'  => '<em>',
        'hl.simple.post' => '</em>',
        'hl.encoder'     => 'html',
      ],
      sortCallback: function(?ParsedQuery $parsedQuery = NULL): string {
        if (!is_null($parsedQuery) && $parsedQuery->hasDefaultQuery()) {
          return 'modified DESC';
        }

        return 'score DESC, title DESC';
      }
    );
  }

  /**
   * Maps a solr document to the desired output format.
   *
   * @param Document $document The document returned by the search engine
   * @param Response $result   The entire search result for retrieving highlighting
   *
   * @return null|array<string, string>
   */
  public function mapResponseDocument(Document $document, Response $result): ?array
  {
    if (!isset($this->converters[$document->getStringField($this->contentTypeField)])) {
      return NULL;
    }

    return $this->converters[$document->getStringField($this->contentTypeField)]($document, $result);
  }

  /**
   * Adds an output mapper for a specific content type.
   *
   * @param string  $type    The content type
   * @param Closure $closure A function to map a solr document into the desired output format
   */
  public function addResponseTypeConverter(string $type, Closure $closure): void
  {
    $this->converters[$type] = $closure;
  }

  public function standardFilters(): array
  {
    $filters    = parent::standardFilters();
    $typeFilter = $this->getTypeFilter($this->contentTypeField);

    if (!is_null($typeFilter)) {
      $filters[] = $typeFilter;
    }

    return $filters;
  }
}
