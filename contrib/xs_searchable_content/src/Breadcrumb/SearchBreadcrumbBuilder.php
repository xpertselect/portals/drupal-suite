<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_searchable_content\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class SearchBreadcrumbBuilder.
 *
 * Provides the breadcrumbs for search pages.
 */
final class SearchBreadcrumbBuilder implements BreadcrumbBuilderInterface
{
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match): bool
  {
    return in_array($route_match->getRouteName(), [
      'xs_searchable_content.web.search.index',
      'xs_searchable_content.web.search',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match): Breadcrumb
  {
    $breadcrumbs = new Breadcrumb();
    $breadcrumbs->addCacheContexts(['route']);

    $breadcrumbs->addLink(Link::createFromRoute(
      $this->t('Home'),
      '<front>'
    ));

    $breadcrumbs->addLink(Link::createFromRoute(
      $this->t('Search'),
      'xs_searchable_content.web.search.index'
    ));

    return $breadcrumbs;
  }
}
