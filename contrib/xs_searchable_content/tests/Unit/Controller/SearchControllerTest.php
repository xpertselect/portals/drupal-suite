<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_searchable_content\Unit\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Theme\ActiveTheme;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\xs_lists\ListClient;
use Drupal\xs_search\QueryString\QueryStringParser;
use Drupal\xs_search\SearchService;
use Drupal\xs_searchable_content\Controller\SearchController;
use Drupal\xs_searchable_content\Services\Search\SearchProfile;
use Drupal\xs_searchable_content\XsSearchableContent;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_searchable_content\TestCase;

/**
 * @internal
 */
final class SearchControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $themeManager = M::mock(ThemeManagerInterface::class, function(MI $mock) {
          $mock->shouldReceive('getActiveTheme')
            ->andReturn(M::mock(ActiveTheme::class, function(MI $mock) {
              $mock->shouldReceive('getName')
                ->andReturn('lorem_ipsum');
            }));
        });

        $services = [
          'xs_search.search_service'                     => M::mock(SearchService::class),
          'xs_searchable_content.search_profile.content' => M::mock(SearchProfile::class),
          'event_dispatcher'                             => M::mock(EventDispatcherInterface::class),
          'xs_search.query_string_parser'                => M::mock(QueryStringParser::class),
          'xs_lists.list_client'                         => M::mock(ListClient::class),
          'theme.manager'                                => $themeManager,
          'config.factory'                               => M::mock(ConfigFactoryInterface::class, function(MI $configMock) {
            $configMock->shouldReceive('get')->with(XsSearchableContent::SETTINGS_KEY)
              ->andReturn(M::mock(ImmutableConfig::class, function(MI $immutableConfigMock) {
                $immutableConfigMock->shouldReceive('get')->with('schema_field_content_type')
                  ->andReturn('type');
              }));

            $configMock->shouldReceive('get')->with(XsSearchableContent::FACET_MAPPING_KEY)
              ->andReturn(M::mock(ImmutableConfig::class, function(MI $immutableConfigMock) {
                $immutableConfigMock->shouldReceive('get')->with('facet_mapping')
                  ->andReturn(['type' => ['name' => 'type', 'label' => 'Type']]);
              }));
          }),
        ];

        foreach ($services as $service => $implementation) {
          $mock->shouldReceive('get')->with($service)->andReturn($implementation);
        }
      });

      Assert::assertInstanceOf(SearchController::class, SearchController::create($container));
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
