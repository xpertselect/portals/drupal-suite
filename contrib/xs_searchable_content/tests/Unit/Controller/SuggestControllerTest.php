<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_searchable_content\Unit\Controller;

use Drupal\xs_search\SearchService;
use Drupal\xs_searchable_content\Controller\SuggestController;
use Drupal\xs_searchable_content\Services\Search\SuggestProfile;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_searchable_content\TestCase;

/**
 * @internal
 */
final class SuggestControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $services = [
          'xs_search.search_service'                      => M::mock(SearchService::class),
          'xs_searchable_content.suggest_profile.content' => M::mock(SuggestProfile::class),
          'event_dispatcher'                              => M::mock(EventDispatcherInterface::class),
        ];

        foreach ($services as $service => $implementation) {
          $mock->shouldReceive('get')->with($service)->andReturn($implementation);
        }
      });

      Assert::assertInstanceOf(SuggestController::class, SuggestController::create($container));
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
