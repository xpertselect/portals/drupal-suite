<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit\TwigExtension;

use Drupal\xs_general\TwigExtension\MarkdownTwigExtension;
use PHPUnit\Framework\Assert;
use Tests\xs_general\TestCase;
use Twig\TwigFilter;

/**
 * @internal
 */
final class MarkdownTwigExtensionTest extends TestCase
{
  public function testExposedFiltersAreTheCorrectClass(): void
  {
    $extension = MarkdownTwigExtension::create();
    $filters   = $extension->getFilters();

    Assert::assertNotEmpty($filters);
    Assert::assertContainsOnlyInstancesOf(TwigFilter::class, $filters);
  }

  public function testExtensionWhenInputIsNullReturnsEmptyString(): void
  {
    $extension = MarkdownTwigExtension::create();
    $actual    = $extension->convertMdToHTML(NULL);

    Assert::assertIsString($actual);
    Assert::assertEquals('', $actual);
  }
}
