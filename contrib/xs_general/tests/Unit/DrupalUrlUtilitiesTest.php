<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit;

use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\xs_general\DrupalUrlUtilities;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_general\TestCase;

/**
 * @internal
 */
final class DrupalUrlUtilitiesTest extends TestCase
{
  public function testDrupalUrlAsStringTransformsToString(): void
  {
    $subject = new class () {
      use DrupalUrlUtilities;
    };

    $url = M::mock(Url::class, function(MI $mock) {
      $mock->shouldReceive('toString')->andReturn('https://example.com');
    });

    $output = $subject->drupalUrlAsString($url);

    Assert::assertIsString($output);
    Assert::assertEquals('https://example.com', $output);
  }

  public function testDrupalUrlAsStringTransformsGeneratedUrls(): void
  {
    $subject = new class () {
      use DrupalUrlUtilities;
    };

    $url = M::mock(Url::class, function(MI $mock) {
      $generatedUrl = M::mock(GeneratedUrl::class, function(MI $mock) {
        $mock->shouldReceive('getGeneratedUrl')->andReturn('https://example.com');
      });

      $mock->shouldReceive('toString')->andReturn($generatedUrl);
    });

    $output = $subject->drupalUrlAsString($url);

    Assert::assertIsString($output);
    Assert::assertEquals('https://example.com', $output);
  }
}
