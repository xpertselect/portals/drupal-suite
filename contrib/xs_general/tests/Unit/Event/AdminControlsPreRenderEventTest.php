<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit\Event;

use Drupal\xs_general\Event\AdminControlsPreRenderEvent;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\StoppableEventInterface;
use Tests\xs_api\TestCase;

/**
 * @internal
 */
final class AdminControlsPreRenderEventTest extends TestCase
{
  public function testEventIsStoppable(): void
  {
    $controls = [];
    $event    = new AdminControlsPreRenderEvent($controls);

    Assert::assertInstanceOf(StoppableEventInterface::class, $event);
  }

  public function testEventDoesNotStopPropagation(): void
  {
    $controls = [];
    $event    = new AdminControlsPreRenderEvent($controls);

    Assert::assertFalse($event->isPropagationStopped());
  }

  public function testControlsArePassedByReference(): void
  {
    $controls = [];

    Assert::assertCount(0, $controls);

    $event                  = new AdminControlsPreRenderEvent($controls);
    $event->adminControls[] = ['foo'];

    Assert::assertCount(1, $controls);
  }
}
