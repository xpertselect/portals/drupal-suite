<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit;

use PHPUnit\Framework\Assert;
use Tests\xs_general\TestCase;

/**
 * @internal
 */
final class ModuleTest extends TestCase
{
  public static function templateDataset(): array
  {
    return [
      ['admin_controls_wrapper'],
    ];
  }

  /**
   * @dataProvider templateDataset
   */
  public function testTemplateIsExposedViaThemeHook(string $template): void
  {
    Assert::assertArrayHasKey($template, xs_general_theme([], 'theme', 'test_theme', '/dev/null'));
  }

  public function testNoActionWhenToolbarIsMissing(): void
  {
    $types = ['foo' => 'bar'];

    xs_general_element_info_alter($types);

    Assert::assertArrayNotHasKey('toolbar', $types);
  }

  public function testLibraryIsAddedToToolbar(): void
  {
    $types = ['foo' => 'bar', 'toolbar' => []];

    xs_general_element_info_alter($types);

    Assert::assertArrayHasKey('toolbar', $types);
    Assert::assertArrayHasKey('#attached', $types['toolbar']);
    Assert::assertArrayHasKey('library', $types['toolbar']['#attached']);
    Assert::assertCount(1, $types['toolbar']['#attached']['library']);
    Assert::assertEquals('xs_general/toolbar', $types['toolbar']['#attached']['library'][0]);
  }
}
