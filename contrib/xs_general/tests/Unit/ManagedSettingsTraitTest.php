<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\xs_general\ManagedSettingsTrait;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_general\TestCase;

/**
 * @internal
 */
final class ManagedSettingsTraitTest extends TestCase
{
  public $subjectUnderTest;

  public static function unmanagedSettingsDataset(): array
  {
    return [
      [NULL, 'foo'],
      [[], 'foo'],
      [['foo'], 'foo'],
      [['foo' => 'bar'], 'foo'],
      [['foo' => ['is_managed']], 'foo'],
      [['foo' => ['is_managed' => NULL]], 'foo'],
      [['foo' => ['is_managed' => FALSE]], 'foo'],
      [['foo' => ['is_managed' => 0]], 'foo'],
      [['foo' => ['is_managed' => []]], 'foo'],
    ];
  }

  public function setUp(): void
  {
    parent::setUp();

    $this->subjectUnderTest = new class () {
      use ManagedSettingsTrait;

      public function testIsManaged(string $settingsName): bool
      {
        return $this->isManaged($settingsName);
      }
    };
  }

  /**
   * @dataProvider unmanagedSettingsDataset
   */
  public function testSettingsAreUnmanagedWhenKeyIsMissingOrNegative(mixed $configValue, string $settingsName): void
  {
    global $config;

    $config = $configValue;

    Assert::assertFalse($this->subjectUnderTest->testIsManaged($settingsName));
  }

  public function testSettingsAreManagedWhenKeyIsSet(): void
  {
    global $config;

    $config = [
      'foo' => ['is_managed' => TRUE],
    ];

    Assert::assertTrue($this->subjectUnderTest->testIsManaged('foo'));
  }

  public function testMessengerIsNotCalledWhenAddMessageIsFalse(): void
  {
    global $config;

    $config           = ['foo' => ['is_managed' => TRUE]];
    $subjectUnderTest = new class () {
      use ManagedSettingsTrait;

      public function testIsManaged(string $settingsName): bool
      {
        return $this->isManaged($settingsName, FALSE);
      }

      public function messenger(): void
      {
        Assert::fail('messenger() was called unexpectedly.');
      }
    };

    Assert::assertTrue($subjectUnderTest->testIsManaged('foo'));
  }

  public function testMessengerIsCalledWhenRequested(): void
  {
    global $config;

    $config           = ['foo' => ['is_managed' => TRUE]];
    $subjectUnderTest = new class () {
      use ManagedSettingsTrait;

      public function __construct()
      {
        $this->messenger = M::mock(MessengerInterface::class, function(MI $mock) {
          $mock->shouldReceive('addError')
            ->andReturn();
        });
      }

      public function t(string $string): string
      {
        return $string;
      }

      public function testIsManaged(string $settingsName): bool
      {
        return $this->isManaged($settingsName, TRUE);
      }
    };

    Assert::assertTrue($subjectUnderTest->testIsManaged('foo'));
  }
}
