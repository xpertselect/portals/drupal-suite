<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit\EventSubscriber;

use Drupal\xs_general\EventSubscriber\AbstractWhiteScreenOfDeathExceptionSubscriber;
use Exception;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Tests\xs_general\TestCase;

/**
 * @internal
 */
final class AbstractWhiteScreenOfDeathExceptionSubscriberTest extends TestCase
{
  public function testSubscriberOnlyHandlesHtml(): void
  {
    $subscriber = new class () extends AbstractWhiteScreenOfDeathExceptionSubscriber {
      public function formats(): array
      {
        return $this->getHandledFormats();
      }

      protected function getCustomWhiteScreenOfDeathResponse(): ?Response
      {
        return NULL;
      }
    };

    Assert::assertCount(1, $subscriber->formats());
    Assert::assertEquals(['html'], $subscriber->formats());
  }

  public function testOnExceptionDoesNothingForHttpExceptions(): void
  {
    $subscriber = new class () extends AbstractWhiteScreenOfDeathExceptionSubscriber {
      protected function getCustomWhiteScreenOfDeathResponse(): ?Response
      {
        return NULL;
      }
    };

    $event = new ExceptionEvent(
      M::mock(HttpKernelInterface::class),
      M::mock(Request::class),
      0,
      $this->getMockBuilder(HttpExceptionInterface::class)->getMock()
    );

    Assert::assertNull($event->getResponse());

    $subscriber->onException($event);

    Assert::assertNull($event->getResponse());
  }

  public function testOnExceptionDoesNothingIfNoCustomResponseIsProvided(): void
  {
    $subscriber = new class () extends AbstractWhiteScreenOfDeathExceptionSubscriber {
      protected function getCustomWhiteScreenOfDeathResponse(): ?Response
      {
        return NULL;
      }
    };

    $event = new ExceptionEvent(
      M::mock(HttpKernelInterface::class),
      M::mock(Request::class),
      0,
      $this->getMockBuilder(Exception::class)->getMock()
    );

    Assert::assertNull($event->getResponse());

    $subscriber->onException($event);

    Assert::assertNull($event->getResponse());
  }

  public function testOnExceptionProvidesCustomResponseWhenAvailable(): void
  {
    $subscriber = new class () extends AbstractWhiteScreenOfDeathExceptionSubscriber {
      protected function getCustomWhiteScreenOfDeathResponse(): ?Response
      {
        return new Response('foo', 500);
      }
    };

    $event = new ExceptionEvent(
      M::mock(HttpKernelInterface::class),
      M::mock(Request::class),
      0,
      $this->getMockBuilder(Exception::class)->getMock()
    );

    Assert::assertNull($event->getResponse());

    $subscriber->onException($event);

    Assert::assertNotNull($event->getResponse());
  }
}
