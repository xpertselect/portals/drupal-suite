<?php

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_general\Unit\Controller;

use Drupal\xs_general\Controller\XpertSelectDashboardController;
use Drupal\xs_general\TwigExtension\MarkdownTwigExtension;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_general\TestCase;

/**
 * @internal
 */
class XpertSelectDashboardControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('xs_general.twig.markdown_extension')
          ->andReturn(M::mock(MarkdownTwigExtension::class));
      });

      Assert::assertInstanceOf(
        XpertSelectDashboardController::class,
        XpertSelectDashboardController::create($container)
      );
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
