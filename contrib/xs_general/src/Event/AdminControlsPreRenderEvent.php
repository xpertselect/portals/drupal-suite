<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general\Event;

use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class AdminControlsPreRenderEvent.
 *
 * Event that describes that the admin controls are ready to be rendered. This is the last chance for any listeners to
 * make any changes to the controls before they are rendered.
 *
 * Each 'adminControl' must be an array with the 'url', 'label', 'description' and 'type' keys.
 */
final class AdminControlsPreRenderEvent implements StoppableEventInterface
{
  /**
   * AdminControlsPreRenderEvent constructor.
   *
   * @param array<int, array<string, string>> $adminControls The controls the user should have
   */
  public function __construct(public array &$adminControls)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function isPropagationStopped(): bool
  {
    return FALSE;
  }
}
