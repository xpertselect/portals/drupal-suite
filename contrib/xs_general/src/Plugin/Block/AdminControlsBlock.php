<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\xs_general\Event\AdminControlsPreRenderEvent;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminControlsBlock.
 *
 * Provides a 'Admin controls' Block that displays a button with content management options.
 *
 * @Block(
 *   id = "xs_general_admin_controls_block",
 *   admin_label = @Translation("Admin controls"),
 * )
 */
final class AdminControlsBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id,
                                $plugin_definition): self
  {
    /** @var EventDispatcherInterface $dispatcher */
    $dispatcher = $container->get('event_dispatcher');

    /** @var ThemeManagerInterface $themeManager */
    $themeManager = $container->get('theme.manager');
    $theme        = $themeManager->getActiveTheme()->getName();

    /** @var AccountProxyInterface $currentUser */
    $currentUser = $container->get('current_user');

    return new self($configuration, $plugin_id, $plugin_definition, $theme, $dispatcher, $currentUser);
  }

  /**
   * {@inheritdoc}
   *
   * @param string                   $activeTheme The active Drupal theme
   * @param EventDispatcherInterface $dispatcher  Service for dispatching events
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition,
                              private readonly string $activeTheme,
                              private readonly EventDispatcherInterface $dispatcher,
                              private readonly AccountProxyInterface $currentUser)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#theme'        => 'admin_controls_wrapper',
      '#actions'      => $this->getControlsToInclude(),
      '#active_theme' => $this->activeTheme,
      '#cache'        => [
        'keys'    => [
          'xs_general',
          'admin-controls',
          'user:' . $this->currentUser->id(),
        ],
        'contexts' => ['user'],
        'tags'     => [
          'admin-controls',
          'user:' . $this->currentUser->id(),
        ],
        'max-age' => CacheBackendInterface::CACHE_PERMANENT,
      ],
      '#attached' => [
        'library' => [
          'xs_general/admin-controls',
        ],
      ],
    ];
  }

  /**
   * Determine and return the controls to include in the block.
   *
   * @return array<int, array> The controls
   */
  private function getControlsToInclude(): array
  {
    $controls = [];

    $this->dispatcher->dispatch(new AdminControlsPreRenderEvent($controls));

    return $controls;
  }
}
