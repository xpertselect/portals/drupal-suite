<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general;

use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;

/**
 * DrupalUrlUtilities.
 *
 * Provides utilities for interacting with Drupal URL instances.
 *
 * @see Url
 */
trait DrupalUrlUtilities
{
  /**
   * Transforms a given Drupal URL into its string representation.
   *
   * @param Url $url The URL to transform
   *
   * @return string The transformed URL
   */
  public function drupalUrlAsString(Url $url): string
  {
    $string = $url->toString();

    return $string instanceof GeneratedUrl
      ? $string->getGeneratedUrl()
      : $string;
  }
}
