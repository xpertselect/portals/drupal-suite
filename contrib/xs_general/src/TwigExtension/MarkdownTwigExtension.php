<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general\TwigExtension;

use Drupal\Component\Utility\Xss;
use League\CommonMark\CommonMarkConverter;
use League\CommonMark\ConverterInterface;
use League\CommonMark\Extension\Autolink\AutolinkExtension;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

/**
 * Class MarkdownTwigExtension.
 *
 * Contains a twig filter to convert Markdown to HTML.
 */
final class MarkdownTwigExtension extends AbstractExtension implements ExtensionInterface
{
  public static function create(): self
  {
    $converter = new CommonMarkConverter();
    $converter->getEnvironment()->addExtension(new AutolinkExtension());

    return new self($converter);
  }

  /**
   * MarkdownTwigExtension constructor.
   *
   * @param ConverterInterface $converter The converter implementation
   */
  public function __construct(private readonly ConverterInterface $converter)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array
  {
    return [
      new TwigFilter('markdown_to_html', [$this, 'convertMdToHTML'], ['is_safe' => ['all']]),
    ];
  }

  /**
   * Convert a given Markdown string to an HTML string.
   *
   * @param ?string $md The Markdown string to convert to HTML
   *
   * @return string The converted HTML string
   */
  public function convertMdToHTML(?string $md): string
  {
    if (is_null($md)) {
      return '';
    }

    if ($white = substr($md, 0, strspn($md, " \t\r\n\0\x0B"))) {
      $md = preg_replace("{^{$white}}m", '', $md) ?? $md;
    }

    return $this->converter->convert(Xss::filter($md))->getContent();
  }
}
