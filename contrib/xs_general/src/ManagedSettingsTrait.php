<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Trait ManagedSettingsTrait.
 *
 * Trait that exposes functionality for determining if a given Drupal settings key is managed via the `settings.php`
 * file.
 *
 * A module may, for instance, disable a configuration form when its settings are being managed by `settings.php` rather
 * than the Drupal database.
 */
trait ManagedSettingsTrait
{
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Determine if the settings by the given key are managed via the `settings.php` file.
   *
   * @return bool Whether the settings are managed
   */
  private function isManaged(string $settingsName, bool $addMessage = FALSE): bool
  {
    global $config;

    $isManaged = !empty($config[$settingsName]['is_managed']);

    if ($isManaged && $addMessage) {
      $this->messenger()
        ->addError($this->t('These settings are managed by your hosting provider. Contact an administrator to have them changed.'));
    }

    return $isManaged;
  }
}
