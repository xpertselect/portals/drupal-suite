<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class AbstractWhiteScreenOfDeathExceptionSubscriber.
 *
 * Attempts to render a 'pretty' HTTP 500 error page, rather than the standard Drupal 'White Screen of Death'. Note that
 * not all errors can be handled by this subscriber. PHP syntax errors will still result in a White Screen of Death for
 * example.
 *
 * To implement this feature, create a custom subscriber (and register it into Drupal as a service) that extends this
 * one and implement the `getCustomWhiteScreenOfDeathResponse` method to instruct the module where to find the custom
 * error page.
 *
 * To register a custom ExceptionSubscriber in a module, include the following in the `services.yml` file of the module.
 *
 * ```
 * services:
 *   my_module.wpod_exception_subscriber:
 *     class: \Drupal\my_module\EventSubscriber\CustomWhiteScreenOfDeathExceptionSubscriber
 *     tags:
 *       - name: event_subscriber
 * ```
 */
abstract class AbstractWhiteScreenOfDeathExceptionSubscriber extends HttpExceptionSubscriberBase
{
  /**
   * Render a custom HTTP 500 error page if the current installation offers one.
   *
   * It could be that Drupal Core itself is the cause of the event, in which case using Drupal Core to render the custom
   * page could itself cause another Exception. Therefore, this implementation uses the minimum of utilities provided by
   * Drupal itself, as they may behave unexpectedly.
   *
   * @param ExceptionEvent $event The event to process
   */
  final public function onException(ExceptionEvent $event): void
  {
    if ($event->getThrowable() instanceof HttpExceptionInterface) {
      // Only handle 'unhandled' Exceptions.
      return;
    }

    if ($response = $this->getCustomWhiteScreenOfDeathResponse()) {
      $event->setResponse($response);
    }
  }

  /**
   * Create and return a Response instance containing the custom White Screen of Death markup. If no such response can
   * be created, this method *MUST* return null so that other ExceptionSubscribers may attempt to handle the exception.
   *
   * @return null|Response The custom White Screen of Death response
   */
  abstract protected function getCustomWhiteScreenOfDeathResponse(): ?Response;

  /**
   * {@inheritdoc}
   */
  final protected function getHandledFormats(): array
  {
    return ['html'];
  }
}
