<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_general\Controller;

use Composer\InstalledVersions;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\xs_general\TwigExtension\MarkdownTwigExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class XpertSelectDashboardController.
 *
 * Drupal controller for rendering the XpertSelect Portals dashboard page.
 */
final class XpertSelectDashboardController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var MarkdownTwigExtension $markdownTwigExtension */
    $markdownTwigExtension = $container->get('xs_general.twig.markdown_extension');

    return new self($markdownTwigExtension);
  }

  /**
   * XpertSelectDashboardController constructor.
   *
   * @param MarkdownTwigExtension $markdownTwigExtension markdown twig extension
   */
  public function __construct(private readonly MarkdownTwigExtension $markdownTwigExtension)
  {
  }

  /**
   * Renders the XpertSelect Portals dashboard page.
   *
   * @return array<int, array<string, mixed>> The Drupal render array
   */
  public function showDashboard(): array
  {
    $rootPackage     = InstalledVersions::getRootPackage();
    $rootPackageName = $rootPackage['name'];
    $changelog       = $this->getChangelogFromPath($rootPackage['install_path']);

    if ($changelog) {
      $changelogText = ' (<a href=\'#\' id=\'open-main-changelog\'>changelog</a>)';
      $settingData   = [
        'xs_general' => [
          'packages' => [
            'main' => [
              'changelog' => $this->markdownTwigExtension->convertMdToHTML($changelog),
              'package'   => $rootPackageName,
            ],
          ],
        ],
      ];
    } else {
      $changelogText = '';
      $settingData   = [];
    }

    return [
      [
        '#theme'  => 'table',
        '#header' => [['colspan' => 1, 'data' => [
          '#type'   => '#markup',
          '#markup' => $rootPackageName . $changelogText,
        ]]],
        '#attached' => [
          'library' => [
            'xs_general/changelog-popup',
          ],
          'drupalSettings' => $settingData,
        ],
      ],
      [
        '#theme'  => 'table',
        '#header' => [['colspan' => 2, 'data' => 'XpertSelect (Drupal)']],
        '#rows'   => $this->packagesAsTableRows(array_merge(
          $this->getPackages('xpertselect-portals/'),
          $this->getPackages('drupal/xs_')
        )),
      ], [
        '#theme'  => 'table',
        '#header' => [['colspan' => 2, 'data' => 'XpertSelect (Core)']],
        '#rows'   => $this->packagesAsTableRows(array_merge(
          $this->getPackages('xpertselect/'),
          $this->getPackages('xs/')
        )),
      ],
    ];
  }

  /**
   * Renders a given array of Composer packages as table rows.
   *
   * @param array<int, string> $packages The Composer packages to render as table rows
   *
   * @return array<int, mixed> The packages as table rows
   */
  private function packagesAsTableRows(array $packages): array
  {
    return array_map(function(string $package): array {
      $version         = InstalledVersions::getPrettyVersion($package);
      $packageNameSlug = str_replace(['/', '-'], '_', $package);

      if (!$version) {
        $version = $this->t('Unknown')->render();
      }

      $changelog = $this->getChangelogFromPath(InstalledVersions::getInstallPath($package));

      if ($changelog) {
        $changelogText = new FormattableMarkup(
          '(<a href=\'#\' id=\'open-:package-changelog\'>:text</a>)',
          [
            ':package' => $packageNameSlug,
            ':text'    => $this->t('changelog'),
          ]
        );
        $settingData = [
          'drupalSettings' => [
            'xs_general' => [
              'packages' => [
                $packageNameSlug => [
                  'changelog' => $this->markdownTwigExtension->convertMdToHTML($changelog),
                  'package'   => $package,
                ],
              ],
            ],
          ],
        ];
      } else {
        $settingData   = [];
        $changelogText = '';
      }

      return [
        ['data' => $package],
        ['data' => [
          '#type'     => '#markup',
          '#markup'   => $version . '&nbsp;' . $changelogText,
          '#attached' => $settingData,
        ]],
      ];
    }, $packages);
  }

  /**
   * Finds all installed Composer packages that match the given filter. The filter is used as the second argument of
   * `str_starts_with`.
   *
   * @param string $filter The string to filter the installed packages with
   *
   * @return array<int, string> The installed Composer packages that match the filter
   */
  private function getPackages(string $filter = ''): array
  {
    $composerPackages = InstalledVersions::getInstalledPackages();

    return array_filter($composerPackages, function(string $package) use ($filter): bool {
      return str_starts_with($package, $filter);
    });
  }

  /**
   * Get the contents of the CHANGELOG.md file in the given path.
   *
   * @param ?string $path The name of the path to get the CHANGELOG.md file from
   *
   * @return false|string The contents of the file or false
   */
  private function getChangelogFromPath(?string $path): false|string
  {
    $file = $path . '/CHANGELOG.md';

    if (!file_exists($file)) {
      return FALSE;
    }

    $contents   = file_get_contents($file);
    $headerLine = '# Changelog';

    if ($contents && str_starts_with($contents, $headerLine)) {
      $contents = trim(substr($contents, strlen($headerLine)));
    }

    return $contents;
  }
}
