/* globals Drupal */

(function(Drupal) {
    const numberFormatter = new Intl.NumberFormat(navigator.language);

    Drupal.behaviors.formatAnnotatedNumbers = {
        attach: function () {
            "use strict";

            document.querySelectorAll(".formatNumber").forEach(function (element) {
                const currentValue = parseInt(element.innerText);

                if (isNaN(currentValue)) {
                    return;
                }

                element.innerText = numberFormatter.format(element.innerText);
                element.classList.remove("formatNumber");
            });
        }
    };
})(Drupal);
