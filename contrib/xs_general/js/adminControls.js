/* globals jQuery, Drupal */

(function ($, Drupal) {
    Drupal.behaviors.adminControls = {
        attach: function () {
            "use strict";

            const container = $(".admin-controls-animated");
            if (container === undefined) {
                return;
            }

            const button = container.find(".admin-controls-toggle");
            const icon   = container.find(".admin-controls-toggle-icon");
            const banner = container.find(".admin-controls-banner");
            let   open   = false;

            button.click(() => {
                icon.css("animation", open ? "turn-cog-wheel 0.5s 1 linear reverse" : "turn-cog-wheel 0.5s 1 linear");
                banner.css("right", open ? `-${banner.outerWidth() + 20}px` : "0");
                button.toggleClass("opened");
                button.attr("aria-expanded", button.attr("aria-expanded") === "false" ? "true" : "false");

                window.setTimeout(function () {
                    icon.css("animation", "");
                }, 500);

                open = !open;
            });
        }
    };
})(jQuery, Drupal);
