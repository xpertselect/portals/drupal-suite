/* globals jQuery, Drupal, drupalSettings */

(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.changelogPopUp = {
        attach: function () {
            "use strict";

            Object.entries(drupalSettings.xs_general.packages).forEach(([key, value]) => {
                $("#open-" + key + "-changelog").on("click", () => {
                    createChangelogModal(value.changelog, value.package).showModal();
                });
            });

            $(window).on("dialog:afterclose dialog:aftercreate", () => {
                $(":focus").blur();
            });
        }
    };

    function createChangelogModal(changelog, packageName) {
        return Drupal.dialog($("<div></div>").html(changelog), {
            dialogClass: "confirm-dialog",
            resizable: true,
            closeOnEscape: false,
            title: packageName + " changelog",
            width: 600,
            buttons: [{
                text: Drupal.t("Back"),
                class: "button-cancel",
                click: function () {
                    $(this).dialog("close");
                }
            }],
        });
    }

})(jQuery, Drupal, drupalSettings);
