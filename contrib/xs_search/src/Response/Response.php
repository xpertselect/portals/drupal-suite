<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search\Response;

use Drupal\xs_solr\Solr\Search\QueryResponse;

/**
 * Class Response.
 *
 * Represents a response from the search-engine.
 */
class Response
{
  /**
   * Response constructor.
   *
   * @param QueryResponse $solrResponse The response from the search-engine
   * @param string        $query        The query that was sent to the search-engine
   */
  public function __construct(private readonly QueryResponse $solrResponse, private readonly string $query)
  {
  }

  /**
   * Gets the Apache Solr response object.
   *
   * @return QueryResponse The Apache Solr response object
   */
  public function getSolrResponse(): QueryResponse
  {
    return $this->solrResponse;
  }

  /**
   * Gets the query that was submitted to Solr.
   *
   * @return string The query that was sent to Solr
   */
  public function getQuery(): string
  {
    return $this->query;
  }

  /**
   * Gets a spellcheck suggestion or `NULL` if there is none.
   *
   * @return null|string The spellcheck suggestion, if there is one
   */
  public function getSuggestion(): ?string
  {
    $spellcheck = $this->solrResponse->getSpellcheck();

    if (is_null($spellcheck) || !array_key_exists('collation', $spellcheck->getCollations())) {
      return NULL;
    }

    return $spellcheck->getCollations()['collation'];
  }
}
