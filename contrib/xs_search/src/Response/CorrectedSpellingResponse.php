<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search\Response;

use Drupal\xs_solr\Solr\Search\QueryResponse;

/**
 * Class CorrectedSpellingResponse.
 *
 * Represents a response from the search-engine which corrected the spelling of the query sent to the search-engine.
 */
final class CorrectedSpellingResponse extends Response
{
  /**
   * CorrectedSpellingResponse constructor.
   *
   * @param QueryResponse $solrResponse  The response from Solr
   * @param string        $query         The query that was sent to Solr
   * @param string        $originalQuery The original query that was sent to Solr that was automatically corrected
   * @param int           $originalCount The number of results of the original query
   */
  public function __construct(QueryResponse $solrResponse, string $query, private readonly string $originalQuery,
                              private readonly int $originalCount)
  {
    parent::__construct($solrResponse, $query);
  }

  /**
   * Gets the original query from the response where the spellcheck suggestion is from.
   *
   * @return string The original query
   */
  public function getOriginalQuery(): string
  {
    return $this->originalQuery;
  }

  /**
   * Gets the number of results of the original query.
   *
   * @return int The original result count
   */
  public function getOriginalCount(): int
  {
    return $this->originalCount;
  }
}
