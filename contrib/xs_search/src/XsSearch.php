<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search;

/**
 * Class XsSearch.
 *
 * Simple PHP class holding various constants used by the `xs_search` Drupal module.
 */
final class XsSearch
{
  /**
   * The key identifying the `xs_search` settings in Drupal.
   *
   * @var string
   */
  public const SETTINGS_KEY = 'xs_search.settings';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'xs_search';
}
