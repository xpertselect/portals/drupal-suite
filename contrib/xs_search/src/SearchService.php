<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\xs_search\Response\CorrectedSpellingResponse;
use Drupal\xs_search\Response\Response;
use Drupal\xs_solr\Solr\Collection;
use Drupal\xs_solr\Solr\Search\QueryResponse;
use Drupal\xs_solr\Solr\Search\ResultSet;
use Drupal\xs_solr\Solr\SolrClient;

/**
 * Class SearchService.
 *
 * A Drupal service for searching through an Apache Solr collection.
 */
final class SearchService
{
  /**
   * A default value for the Solr default collection when no such collection has been configured.
   *
   * @var string
   */
  public const FALLBACK_COLLECTION = 'search_collection';

  /**
   * Create a new SearchService instance.
   *
   * @param SolrClient                    $solrClient           The client for interacting with Apache Solr
   * @param ConfigFactoryInterface        $configFactory        The factory for retrieving Drupal configuration
   * @param LoggerChannelFactoryInterface $loggerChannelFactory The factory for creating log channel instances
   *
   * @return SearchService The created instance
   */
  public static function create(SolrClient $solrClient, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $loggerChannelFactory): SearchService
  {
    $settings   = $configFactory->get(XsSearch::SETTINGS_KEY);
    $collection = $settings->get('search_collection');

    if (!is_string($collection)) {
      $loggerChannelFactory->get(XsSearch::LOG_CHANNEL)
        ->warning('The "search_collection" config value is missing or invalid. Using fallback value "@value".', [
          '@value' => self::FALLBACK_COLLECTION,
        ]);

      $collection = self::FALLBACK_COLLECTION;
    }

    return new SearchService($solrClient, $collection);
  }

  /**
   * SearchService constructor.
   *
   * @param SolrClient $solrClient        The client for interacting with Apache Solr
   * @param string     $defaultCollection The name of the default Apache Solr collection
   */
  public function __construct(private readonly SolrClient $solrClient, private readonly string $defaultCollection)
  {
  }

  /**
   * Perform a search query against the search-engine.
   *
   * @param SearchProfileInterface $searchProfile    The searchProfile being used
   * @param string                 $query            The search terms
   * @param array<int, string>     $filters          The filters to apply to the query
   * @param int                    $start            From which index to start returning results
   * @param string                 $sort             The field to sort on and its direction
   * @param bool                   $followSpellcheck Whether to automatically redo the query when a spellchecked query
   *                                                 variation is available and viable
   *
   * @return null|Response The response from the search-engine
   */
  public function search(SearchProfileInterface $searchProfile, string $query = '*:*', array $filters = [],
                         int $start = 0, string $sort = 'score DESC', bool $followSpellcheck = TRUE): ?Response
  {
    $solrResponse = $this->performQuery($searchProfile, $query, $filters, $start, $sort);

    if (is_null($solrResponse)) {
      return NULL;
    }

    $response            = new Response($solrResponse, $query);
    $originalSolrResults = $response->getSolrResponse()->getResultSet();

    if ($this->resultsAreUsable($originalSolrResults)) {
      return $response;
    }

    if (($newQuery = $response->getSuggestion()) && $followSpellcheck) {
      $spellcheckedResponse = $this->performQuery($searchProfile, $newQuery, $filters, $start, $sort);

      if ($this->correctedSpellingIsViable($spellcheckedResponse, $originalSolrResults)) {
        $spellcheckedSolrResponse = $spellcheckedResponse->getResultSet();

        if ($this->resultsAreUsable($spellcheckedSolrResponse)) {
          return new CorrectedSpellingResponse($spellcheckedResponse, $newQuery, $query, $originalSolrResults->numFound());
        }
      }
    }

    return $response;
  }

  /**
   * Perform a suggest query against the search-engine.
   *
   * @param string $query      The search terms
   * @param string $handler    The search handler
   * @param string $dictionary The Solr dictionary component
   * @param int    $count      The maximum number of suggestions to return
   *
   * @return null|QueryResponse The response from the search-engine
   */
  public function suggest(string $query, string $handler, string $dictionary, int $count = 5): ?QueryResponse
  {
    $collection = $this->solrClient->collection($this->defaultCollection);

    return $collection->suggest($query, $handler, $dictionary, $count);
  }

  /**
   * Return the Collection instance that will receive the query.
   *
   * @param SearchProfileInterface $searchProfile The searchProfile that is being used
   *
   * @return Collection The Apache Solr collection that will receive the query
   */
  private function getCollection(SearchProfileInterface $searchProfile): Collection
  {
    $collectionName = $searchProfile instanceof CustomCollectionInterface
      ? $searchProfile->getCustomCollectionName()
      : $this->defaultCollection;

    return $this->solrClient->collection($collectionName);
  }

  /**
   * Send a query to the search-engine and return its response.
   *
   * @param SearchProfileInterface $searchProfile The searchProfile being used
   * @param string                 $query         The search terms
   * @param array<int, string>     $filters       The filters to apply to the query
   * @param int                    $start         From which index to start returning results
   * @param string                 $sort          The field to sort on and its direction
   *
   * @return null|QueryResponse The response from the search-engine
   */
  private function performQuery(SearchProfileInterface $searchProfile, string $query, array $filters, int $start,
                                string $sort): ?QueryResponse
  {
    $collection = $this->getCollection($searchProfile);

    return $collection->query(
      $searchProfile->requestHandler(),
      $query,
      array_merge($filters, $searchProfile->standardFilters()),
      $start,
      $searchProfile->resultsPerPage(),
      $this->buildAdditionalParameters($searchProfile, $sort)
    );
  }

  /**
   * Build the additional parameter array that will be included in the query sent to the search-engine.
   *
   * @param SearchProfileInterface $searchProfile The searchProfile that is being used
   * @param string                 $sort          The sorting instruction
   *
   * @return array<string, mixed> The additional parameters to include in the query
   */
  private function buildAdditionalParameters(SearchProfileInterface $searchProfile, string $sort): array
  {
    $parameters         = $searchProfile->additionalParameters();
    $parameters['sort'] = $sort;

    if (($fl = $searchProfile->fieldList())) {
      $parameters['fl'] = implode(',', $fl);
    }

    return $parameters;
  }

  /**
   * Determine if the given ResultSet instance is usable for providing search results.
   *
   * @param null|ResultSet $resultSet The instance to check
   *
   * @return bool Whether the ResultSet is usable
   */
  private function resultsAreUsable(?ResultSet $resultSet): bool
  {
    return !is_null($resultSet) && $resultSet->numFound() > 0;
  }

  /**
   * Determine if the response for the spellchecked query can be used.
   *
   * @param null|QueryResponse $spellcheckedResponse The spellchecked response
   * @param null|ResultSet     $originalResults      The results from the original query
   *
   * @return bool Whether the spellcheck is viable
   */
  private function correctedSpellingIsViable(?QueryResponse $spellcheckedResponse, ?ResultSet $originalResults): bool
  {
    return !is_null($spellcheckedResponse) && !is_null($originalResults);
  }
}
