<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search\Services\Search;

use Closure;
use Drupal\xs_search\QueryString\ParsedQuery;
use Drupal\xs_search\SearchProfileInterface;

class MutableSearchProfile implements SearchProfileInterface
{
  /**
   * MutableSearchProfile constructor.
   *
   * @param array<int, string>                                     $filters              The filters used by this search profile
   * @param array<int, string>                                     $fieldList            The fields that should be retrieved
   * @param array<int, string>                                     $facetFieldList       The facet fields that should be retrieved
   * @param string                                                 $requestHandler       The request handler to use
   * @param int                                                    $resultsPerPage       The amount of results that are returned by the search service
   * @param array<string, mixed>                                   $additionalParameters Additional parameters that are sent to the search engine
   * @param array<string, array{label: string, solrQuery: string}> $sortOptions          A list of allowed sort options for this profile
   * @param null|Closure                                           $sortCallback         The sort closure to use based on the incoming request
   * @param array<string, string>                                  $facetMapping         The mapping of the facet fields to their
   *                                                                                     more human-friendly variants as a {facet field} => {label} array
   */
  public function __construct(protected array $filters = [],
                              protected array $fieldList = [],
                              protected array $facetFieldList = [],
                              protected string $requestHandler = 'select',
                              protected int $resultsPerPage = 10,
                              protected array $additionalParameters = [],
                              protected array $sortOptions = [],
                              protected ?Closure $sortCallback = NULL,
                              protected array $facetMapping = [])
  {
  }

  /**
   * Get all filters for this searchProfile.
   *
   * @return array<int, string> All filters
   */
  public function getFilters(): array
  {
    return $this->filters;
  }

  /**
   * Add filters to this searchProfile.
   *
   * @param array<int, string> $filters The filters to add
   */
  public function addFilters(array $filters): void
  {
    $this->filters = array_values(array_unique(array_merge($this->filters, $filters)));
  }

  /**
   * sets the filters to this searchProfile.
   *
   * @param array<int, string> $filters The new filters
   */
  public function setFilters(array $filters): void
  {
    $this->filters = $filters;
  }

  /**
   * Add fields to be retrieved by the search engine to this searchProfile.
   *
   * @param array<int, string> $fields The fields to add
   */
  public function addFieldsToFieldList(array $fields): void
  {
    $this->fieldList = array_values(array_unique(array_merge($this->fieldList, $fields)));
  }

  /**
   * Set fields to be retrieved by the search engine to this searchProfile.
   *
   * @param array<int, string> $fields The fields to add
   */
  public function setFieldsToFieldList(array $fields): void
  {
    $this->fieldList = $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return $this->fieldList;
  }

  /**
   * Add facet fields to be retrieved by the search engine to this searchProfile.
   *
   * @param array<int, string> $fields The fields to add
   */
  public function addFacetFields(array $fields): void
  {
    $this->facetFieldList = array_values(array_unique(array_merge($this->facetFieldList, $fields)));
  }

  /**
   * Set facet fields to be retrieved by the search engine to this searchProfile.
   *
   * @param array<int, string> $fields The fields to add
   */
  public function setFacetFields(array $fields): void
  {
    $this->facetFieldList = $fields;
  }

  /**
   * Sets which request handler to use.
   *
   * @param string $newRequestHandler The handler to use
   */
  public function setRequestHandler(string $newRequestHandler): void
  {
    $this->requestHandler = $newRequestHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return $this->requestHandler;
  }

  /**
   * Sets how many results are returned by the search service.
   *
   * @param int $newResultsPerPage The amount that wil be returned
   */
  public function setResultsPerPage(int $newResultsPerPage): void
  {
    $this->resultsPerPage = $newResultsPerPage;
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return $this->resultsPerPage;
  }

  /**
   * Sets additional parameters that are sent to the search engine.
   *
   * @param array<string, mixed> $newAdditionalParameters The additional parameters that are sent to the search engine
   */
  public function setAdditionalParameters(array $newAdditionalParameters): void
  {
    $this->additionalParameters = $newAdditionalParameters;
  }

  /**
   * Adds additional parameters that are sent to the search engine.
   *
   * @param array<string, mixed> $newAdditionalParameters The additional parameters that are sent to the search engine
   */
  public function addAdditionalParameters(array $newAdditionalParameters): void
  {
    $this->additionalParameters = array_merge($this->additionalParameters, $newAdditionalParameters);
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return empty($this->facetFieldList)
      ? $this->additionalParameters
      : ['facet.field' => $this->facetFieldList] + $this->additionalParameters;
  }

  /**
   * Sets a list of allowed sort options for this profile.
   *
   * @param array<string, array{label: string, solrQuery: string}> $newSortOptions The list of allowed sort options for this profile
   */
  public function setSortOptions(array $newSortOptions): void
  {
    $this->sortOptions = $newSortOptions;
  }

  /**
   * Adds a list of allowed sort options for this profile.
   *
   * @param array<string, array{label: string, solrQuery: string}> $newSortOptions The list of allowed sort options for this profile
   */
  public function addSortOptions(array $newSortOptions): void
  {
    $this->sortOptions = array_merge($this->sortOptions, $newSortOptions);
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return $this->sortOptions;
  }

  /**
   * Sets the sort closure to use based on the incoming request.
   *
   * @param null|Closure $newSort The new sort callback
   */
  public function setSortCallback(?Closure $newSort): void
  {
    $this->sortCallback = $newSort;
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?ParsedQuery $parsedQuery = NULL): string
  {
    return is_callable($this->sortCallback) ? call_user_func($this->sortCallback, $parsedQuery) : 'score DESC';
  }

  /**
   * Provides the mapping of the facet fields to their more human-friendly variants as a {facet field} => {label} array.
   *
   * @param array<string, string> $newFacetMapping The new facet mapping
   */
  public function setFacetMapping(array $newFacetMapping): void
  {
    $this->facetMapping = $newFacetMapping;
  }

  /**
   * Adds to the mapping of the facet fields to their more human-friendly variants as a {facet field} => {label} array.
   *
   * @param array<string, string> $newFacetMapping The new facet mapping
   */
  public function addFacetMapping(array $newFacetMapping): void
  {
    $this->facetMapping = array_merge($this->facetMapping, $newFacetMapping);
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return $this->facetMapping;
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return $this->filters;
  }
}
