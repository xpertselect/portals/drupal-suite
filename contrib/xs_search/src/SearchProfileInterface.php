<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search;

use Drupal\xs_search\QueryString\ParsedQuery;

/**
 * Interface SearchProfileInterface.
 *
 * An interface that defines which methods a search profile should implement. A search profile is used when searching
 * with the search service.
 */
interface SearchProfileInterface
{
  /**
   * Returns which request handler to use.
   *
   * @return string The request handler
   */
  public function requestHandler(): string;

  /**
   * Returns what standard filters to apply.
   *
   * @return string[] The standard filters
   */
  public function standardFilters(): array;

  /**
   * Returns how many results are returned by the search service.
   *
   * @return int The number of results to return
   */
  public function resultsPerPage(): int;

  /**
   * Returns the list of fields that the search engine returns per document.
   *
   * @return string[] The list of fields
   */
  public function fieldList(): array;

  /**
   * Returns additional parameters that are sent to the search engine.
   *
   * @return array<string, mixed> The list of additional parameters
   */
  public function additionalParameters(): array;

  /**
   * Returns a list of allowed sort options for this profile.
   *
   * @return array<string, array{label: string, solrQuery: string}> The list of sort options
   */
  public function sortOptions(): array;

  /**
   * Returns the sort string to use based on the incoming request. The incoming request may not specify this
   * information, in such a case the SearchProfile must return a suitable default sorting value.
   *
   * @param null|ParsedQuery $parsedQuery The parsed query components
   *
   * @return string The sort field and sort order string
   */
  public function sort(?ParsedQuery $parsedQuery = NULL): string;

  /**
   * Provides the mapping of the facet fields to their more human-friendly variants as a {facet field} => {label} array.
   *
   * This mapping can provide translations from `facet_` fields to their non `facet_` variants as to not expose the
   * internal names of the fields.
   *
   * Any field not present in this mapping will not be considered eligible as a valid filter option by
   * `QueryStringParserInterface` implementations.
   *
   * @return array<string, string> The facet mapping as {field} => {label}
   */
  public function facetMapping(): array;
}
