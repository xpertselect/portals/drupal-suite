<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_general\ManagedSettingsTrait;
use Drupal\xs_search\XsSearch;

/**
 * Class AdminSettingsForm.
 *
 * Provides a Drupal form for managing the configuration of the `drupal/xs_search` module.
 */
final class AdminSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_search_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    if ($this->isManaged(XsSearch::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $form['search'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Search settings'),
      '#description' => $this->t('Settings pertaining to searching through the search engine.'),
      '#open'        => TRUE,

      'search_collection' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Search collection'),
        '#description'   => $this->t('Provide the name of the Apache Solr collection that holds the searchable documents.'),
        '#default_value' => $this->config(XsSearch::SETTINGS_KEY)->get('search_collection'),
        '#required'      => TRUE,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(XsSearch::SETTINGS_KEY, TRUE)) {
      return;
    }

    $config = $this->config(XsSearch::SETTINGS_KEY);
    $config->set('search_collection', $form_state->getValue('search_collection'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsSearch::SETTINGS_KEY];
  }
}
