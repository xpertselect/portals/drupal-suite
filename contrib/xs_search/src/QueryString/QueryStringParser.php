<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search\QueryString;

use Drupal\xs_lists\ListClient;
use Drupal\xs_search\SearchProfileInterface;

/**
 * Class QueryStringParser.
 *
 * A service for parsing the HTTP querystring into usable components that can be sent to the search-engine as a query.
 */
final class QueryStringParser
{
  /**
   * The default query to use when the query string provides no query.
   *
   * @var string
   */
  public const DEFAULT_QUERY = '*:*';

  /**
   * QueryStringParser constructor.
   *
   * @param ListClient $listClient The listClient to use for providing filter value translations
   */
  public function __construct(private readonly ListClient $listClient)
  {
  }

  /**
   * Parse the given query string input and determine what query, which filters and which pageNumber are to be used.
   *
   * @param string                 $query         The {query} component of the query-string
   * @param string                 $filters       The {filter} component of the query-string
   * @param int                    $pageNumber    The {page number} component of the query-string
   * @param SearchProfileInterface $searchProfile The searchProfile containing potential mappings for the filters
   * @param string                 $sorting       The {sorting} component of the query-string
   *
   * @return ParsedQuery The parsed query instance
   */
  public function parse(string $query, string $filters, int $pageNumber,
                        SearchProfileInterface $searchProfile, string $sorting = '-'): ParsedQuery
  {
    $parsedFilters = $this->parseFilters($filters, $searchProfile);

    return new ParsedQuery(
      $this->parseQuery($query),
      $parsedFilters,
      $this->translateFilters($parsedFilters, $searchProfile),
      $this->parsePageNumber($pageNumber),
      $sorting
    );
  }

  /**
   * Extract the current query from the given query string. Will use the default query if the given query is either
   * empty, a '*' or an '-'.
   *
   * @param string $query The query as it exists in the current URL
   *
   * @return string The active query
   */
  private function parseQuery(string $query): string
  {
    $query = trim($query);

    if (empty($query)) {
      return self::DEFAULT_QUERY;
    }

    if ('*' === $query) {
      return self::DEFAULT_QUERY;
    }

    if ('-' === $query) {
      return self::DEFAULT_QUERY;
    }

    return $query;
  }

  /**
   * Extract the active filters from a given filter string.
   *
   * @param string                 $filters       The filters as they exist in the URL
   * @param SearchProfileInterface $searchProfile The searchProfile containing the facetMapping to determine which
   *                                              filter fields are valid
   *
   * @return array<int, array{field: string, value: string}> The active filters
   */
  private function parseFilters(string $filters, SearchProfileInterface $searchProfile): array
  {
    $givenFilters = explode(';', $filters);
    $filters      = [];

    foreach ($givenFilters as $given_filter) {
      $parts = explode(':', $given_filter, 2);

      if (count($parts) < 2) {
        continue;
      }

      if (!array_key_exists($parts[0], $searchProfile->facetMapping())) {
        continue;
      }

      $field = $searchProfile->facetMapping()[$parts[0]];
      $value = $parts[1];

      if ('' === trim($value)) {
        continue;
      }

      $filters[] = [
        'field' => $field,
        'value' => $this->listClient->list($field)->labelToIdentifier($value),
      ];
    }

    return $filters;
  }

  /**
   * Translate the active filters to their human-friendly counterparts.
   *
   * @param array<int, array{field: string, value: string}> $filters       The active filters
   * @param SearchProfileInterface                          $searchProfile The profile containing the facetMapping used
   *                                                                       to translate the filter fields
   *
   * @return array<int, array{field: string, value: string}> The translated active filters
   */
  private function translateFilters(array $filters, SearchProfileInterface $searchProfile): array
  {
    $nameLookup = array_flip($searchProfile->facetMapping());
    $translated = [];

    foreach ($filters as $filter) {
      $field = $nameLookup[$filter['field']];

      $translated[] = [
        'field' => $field,
        'value' => $this->listClient->list($field)->identifierToLabel($filter['value']),
      ];
    }

    return $translated;
  }

  /**
   * Extracts the active pageNumber from the given input.
   *
   * @param int $pageNumber The pageNumber input
   *
   * @return int The active pageNumber
   */
  private function parsePageNumber(int $pageNumber): int
  {
    if ($pageNumber < 1) {
      return 1;
    }

    return $pageNumber;
  }
}
