<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search\QueryString;

/**
 * Class ParsedQuery.
 *
 * Represents the extracted query components that can be used to construct a query to the search-engine.
 */
final class ParsedQuery
{
  /**
   * The default 'q' value to use when querying Apache Solr.
   */
  public const DEFAULT_QUERY = '*:*';

  /**
   * Flattened representation of the requested filters such that they can be sent to Apache Solr.
   *
   * @var null|array<int, string>
   */
  private ?array $flattenedFilters;

  /**
   * The filters from the query as a single string.
   */
  private ?string $filterText;

  /**
   * ParsedQuery constructor.
   *
   * @param string                                          $query             The query to send
   * @param array<int, array{field: string, value: string}> $filters           The requested filters
   * @param array<int, array{field: string, value: string}> $translatedFilters The human-readable filters
   * @param int                                             $pageNumber        The requested page number
   * @param string                                          $sorting           The requested sorting
   */
  public function __construct(private readonly string $query, private readonly array $filters,
                              private readonly array $translatedFilters, private readonly int $pageNumber,
                              private readonly string $sorting)
  {
    $this->flattenedFilters = NULL;
    $this->filterText       = NULL;
  }

  /**
   * Retrieve the parsed query component.
   *
   * @return string The query from the query string
   */
  public function getQuery(): string
  {
    return $this->query;
  }

  /**
   * Retrieve the parsed filters as a {field} => {value} array. If applicable the value should already be mapped to the
   * value as it exists in the index.
   *
   * @return array<int, array<string, string>> The raw {field}:{value} filters
   */
  public function getFilters(): array
  {
    return $this->filters;
  }

  /**
   * Retrieve the sorting.
   *
   * @return string The sorting
   */
  public function getSorting(): string
  {
    return $this->sorting;
  }

  /**
   * Retrieve the filters flattened such that they can be used in a query to the search engine. The flattened array
   * looks like:.
   *
   * ```
   * [
   *   "{field1}:\"{value1\"}",
   *   "{field2}:\"{value2\"}",
   *   ...
   * ]
   * ```
   *
   * @return array<int, string> The flattened filters
   *
   * @see ParsedQuery::getFilters()
   */
  public function getFlattenedFilters(): array
  {
    if (is_null($this->flattenedFilters)) {
      $this->flattenedFilters = array_values(array_map(function(array $filter) {
        return $filter['field'] . ':"' . $filter['value'] . '"';
      }, $this->getFilters()));
    }

    return $this->flattenedFilters;
  }

  /**
   * Retrieve the translated versions of the filters where the fields and values may be translated to more
   * human-friendly versions.
   *
   * This method enables the following filter and translation:
   *
   * Solr Filter:
   * > `facet_dataset_status:"http://data.overheid.nl/status/beschikbaar"`
   *
   * Translation:
   * > `status:Beschikbaar`
   *
   * Obviously "`status:Beschikbaar`" is the preferred presentation for the end-user. This method should expose that
   * functionality.
   *
   * If a filter or value cannot be translated, its original value should be used.
   *
   * @return array<int, array{field: string, value: string}> The filters with human-friendly field and values
   *
   * @see SearchProfileInterface::facetMapping()
   * @see TranslatableList::identifierToLabel()
   */
  public function getTranslatedFilters(): array
  {
    return $this->translatedFilters;
  }

  /**
   * Retrieve the translated filters as a single string such that they can be reused to create a new search URL. The
   * string comes in the format:.
   *
   * ```
   * {field1}:{value1};{field2}:{value2}...
   * ```
   *
   * @return string The filters as a single string
   *
   * @see ParsedQuery::getTranslatedFilters()
   */
  public function getFiltersAsString(): string
  {
    if (is_null($this->filterText)) {
      $merged = '';

      foreach ($this->getTranslatedFilters() as $filter) {
        $merged .= $filter['field'] . ':' . $filter['value'] . ';';
      }

      if (mb_strlen($merged) > 1 && FALSE !== mb_strpos($merged, ';', -1)) {
        $merged = mb_substr($merged, 0, mb_strlen($merged) - 1);
      }

      $this->filterText = $merged;
    }

    return $this->filterText;
  }

  /**
   * Retrieve the requested page number.
   *
   * @return int The page number
   */
  public function getPageNumber(): int
  {
    return $this->pageNumber;
  }

  /**
   * Whether the parsed query is equal to the default Solr query.
   *
   * @return bool Whether the query equals the 'default' query
   */
  public function hasDefaultQuery(): bool
  {
    return self::DEFAULT_QUERY === $this->getQuery();
  }

  /**
   * Checks whether any filters are active.
   *
   * @return bool True if any filter is active, false otherwise
   */
  public function hasFilters(): bool
  {
    return count($this->getFilters()) > 0;
  }
}
