<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search;

/**
 * Interface CustomCollectionInterface.
 *
 * Implementations of this interface provide the name of an alternate collection to which search queries should be sent.
 */
interface CustomCollectionInterface
{
  /**
   * Provide the name of the alternate Apache Solr collection to which queries should be sent.
   *
   * @return string The alternate collection name
   */
  public function getCustomCollectionName(): string;
}
