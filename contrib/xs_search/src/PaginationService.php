<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_search;

/**
 * Class PaginationService.
 *
 * A service for reasoning about the available pages for a set of results.
 */
final class PaginationService
{
  /**
   * The total number of pages that are available.
   */
  private int $numberOfPages;

  /**
   * PaginationService constructor.
   *
   * @param int $currentPage    The current page being displayed
   * @param int $resultCount    The total amount of results
   * @param int $resultsPerPage The amount of results per page
   */
  public function __construct(private readonly int $currentPage, int $resultCount, int $resultsPerPage)
  {
    $this->numberOfPages = $resultCount > 0
      ? intval(ceil($resultCount / $resultsPerPage))
      : 1;
  }

  /**
   * Return the current page of the result set.
   *
   * @return int The current page
   */
  public function getCurrentPage(): int
  {
    return $this->currentPage;
  }

  /**
   * Return the previous page of the result set, if there is any.
   *
   * @return null|int The previous page number
   */
  public function getPreviousPage(): ?int
  {
    return 1 !== $this->currentPage
      ? $this->currentPage - 1
      : NULL;
  }

  /**
   * Return an array of previous pages of the result set, if there are previous pages.
   *
   * @param int $amountOfPages The amount of previous pages to return
   *
   * @return array<int, int> The list of previous page numbers
   */
  public function getPreviousPages(int $amountOfPages = 2): array
  {
    $pages = [];

    for ($i = 0; $i < $amountOfPages; ++$i) {
      $requested_page = $this->currentPage - ($amountOfPages - $i);

      if ($requested_page > 0) {
        $pages[] = $requested_page;
      }
    }

    return $pages;
  }

  /**
   * Return the next page number, if there is any.
   *
   * @return null|int The next page number
   */
  public function getNextPage(): ?int
  {
    return $this->currentPage < $this->numberOfPages
      ? $this->currentPage + 1
      : NULL;
  }

  /**
   * Return an array of next pages of the result set, if there are next pages.
   *
   * @param int $amountOfPages The amount of next pages to return
   *
   * @return array<int, int> The list of next page numbers
   */
  public function getNextPages(int $amountOfPages = 2): array
  {
    $pages = [];

    for ($i = 1; $i < $amountOfPages + 1; ++$i) {
      $requestedPage = $this->currentPage + $i;

      if ($requestedPage <= $this->numberOfPages) {
        $pages[] = $requestedPage;
      }
    }

    return $pages;
  }
}
