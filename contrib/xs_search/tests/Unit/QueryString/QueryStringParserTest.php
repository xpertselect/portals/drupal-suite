<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit\QueryString;

use Drupal\xs_lists\ListClient;
use Drupal\xs_lists\Lists\TranslatableList;
use Drupal\xs_search\QueryString\QueryStringParser;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
final class QueryStringParserTest extends TestCase
{
  public static function defaultQueryDataset(): array
  {
    return [
      'empty'    => [''],
      'trimmed'  => [' '],
      'wildcard' => ['*'],
      'dash'     => ['-'],
    ];
  }

  public static function filterParsingDataset(): array
  {
    return [
      'empty'           => ['', []],
      'field-only'      => ['foo', []],
      'empty-value'     => ['foo: ', []],
      'unmapped-field'  => ['Foo:bar', []],
      'field-and-value' => ['foo:bar', [['field' => 'foo', 'value' => 'bar']]],
      'multiple-pairs'  => ['foo:bar;foo:baz', [['field' => 'foo', 'value' => 'bar'], ['field' => 'foo', 'value' => 'baz']]],
    ];
  }

  public static function sortingParsingDataset(): array
  {
    return [
      'emptyRoute'    => ['-', '-'],
      'filledDefault' => ['relevance', 'relevance'],
      'filledOption'  => ['title:desc', 'title:desc'],
    ];
  }

  public static function pageNumberParsingDataset(): array
  {
    return [
      'zero'     => [0, 1],
      'negative' => [-1, 1],
      'one'      => [1, 1],
      'positive' => [5, 5],
    ];
  }

  /**
   * @dataProvider defaultQueryDataset
   */
  public function testDefaultQueryIsAssignedWhenAppropriate(string $givenQuery): void
  {
    $listClient = M::mock(ListClient::class);
    $parser     = new QueryStringParser($listClient);
    $result     = $parser->parse($givenQuery, '', 1, $this->dummySearchProfile());

    Assert::assertEquals(QueryStringParser::DEFAULT_QUERY, $result->getQuery());
  }

  public function testNonDefaultQueryIsCorrectlyParsed(): void
  {
    $listClient = M::mock(ListClient::class);
    $parser     = new QueryStringParser($listClient);
    $result     = $parser->parse('my query', '', 1, $this->dummySearchProfile());

    Assert::assertEquals('my query', $result->getQuery());
  }

  /**
   * @dataProvider filterParsingDataset
   */
  public function testFiltersAreCorrectlyParsed(string $filters, array $parsedFilters): void
  {
    $listClient = M::mock(ListClient::class, function(MI $mock) {
      $list = M::mock(TranslatableList::class, function(MI $mock) {
        $mock->shouldReceive('labelToIdentifier')
          ->andReturnUsing(function($value) {
            return $value;
          });

        $mock->shouldReceive('identifierToLabel')
          ->andReturnUsing(function($value) {
            return $value;
          });
      });

      $mock->shouldReceive('list')->andReturn($list);
    });

    $parser = new QueryStringParser($listClient);
    $result = $parser->parse('', $filters, 1, $this->dummySearchProfile());

    Assert::assertEquals($parsedFilters, $result->getFilters());
  }

  /**
   * @dataProvider sortingParsingDataset
   */
  public function testSortingParsing(string $input, string $expected): void
  {
    $listClient = M::mock(ListClient::class);
    $parser     = new QueryStringParser($listClient);
    $result     = $parser->parse('$givenQuery', '', 1, $this->dummySearchProfile(), $input);

    Assert::assertEquals($expected, $result->getSorting());
  }

  /**
   * @dataProvider pageNumberParsingDataset
   */
  public function testPageNumberParsing(int $input, int $expected): void
  {
    $listClient = M::mock(ListClient::class);
    $parser     = new QueryStringParser($listClient);
    $result     = $parser->parse('$givenQuery', '', $input, $this->dummySearchProfile());

    Assert::assertEquals($expected, $result->getPageNumber());
  }
}
