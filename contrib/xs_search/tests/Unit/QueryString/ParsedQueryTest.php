<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit\QueryString;

use Drupal\xs_search\QueryString\ParsedQuery;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
final class ParsedQueryTest extends TestCase
{
  public static function getFiltersAsStringDataset(): array
  {
    return [
      [[], ''],
      [[['field' => 'foo', 'value' => 'bar']], 'foo:bar'],
      [[['field' => 'foo', 'value' => 'bar'], ['field' => 'lorem', 'value' => 'ipsum']], 'foo:bar;lorem:ipsum'],
    ];
  }

  public static function hasDefaultQueryDataset(): array
  {
    return [
      ['*:*', TRUE],
      ['*:bar', FALSE],
      ['bar:*', FALSE],
    ];
  }

  public static function hasFiltersDataset(): array
  {
    return [
      [[], FALSE],
      [[['field' => 'foo', 'value' => 'bar']], TRUE],
    ];
  }

  public function testGetters(): void
  {
    $query             = 'foo:bar';
    $filters           = [['field' => 'foo', 'value' => 'bar']];
    $translatedFilters = [['field' => 'foo', 'value' => 'Bar']];
    $pageNumber        = 5;
    $sorting           = 'score DESC';

    $parsedQuery = new ParsedQuery($query, $filters, $translatedFilters, $pageNumber, $sorting);

    Assert::assertEquals($query, $parsedQuery->getQuery());
    Assert::assertEquals($filters, $parsedQuery->getFilters());
    Assert::assertEquals($translatedFilters, $parsedQuery->getTranslatedFilters());
    Assert::assertEquals(['foo:"bar"'], $parsedQuery->getFlattenedFilters());
    Assert::assertEquals($pageNumber, $parsedQuery->getPageNumber());
    Assert::assertEquals($sorting, $parsedQuery->getSorting());
  }

  /**
   * @dataProvider getFiltersAsStringDataset
   */
  public function testGetFiltersAsString(array $filters, string $flattened): void
  {
    $parsedQuery = new ParsedQuery('*:*', $filters, $filters, 1, '');

    Assert::assertEquals($flattened, $parsedQuery->getFiltersAsString());
  }

  /**
   * @dataProvider hasDefaultQueryDataset
   */
  public function testHasDefaultQuery(string $query, bool $expected): void
  {
    $parsedQuery = new ParsedQuery($query, [], [], 1, '');

    Assert::assertEquals($expected, $parsedQuery->hasDefaultQuery());
  }

  /**
   * @dataProvider hasFiltersDataset
   */
  public function testHasFilters(array $filters, bool $expected): void
  {
    $parsedQuery = new ParsedQuery('*:*', $filters, [], 1, '');

    Assert::assertEquals($expected, $parsedQuery->hasFilters());
  }
}
