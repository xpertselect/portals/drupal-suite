<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit;

use Drupal\xs_search\PaginationService;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
final class PaginationServiceTest extends TestCase
{
  public function testPageMathOnEmptyResult(): void
  {
    $service = new PaginationService(1, 0, 10);

    Assert::assertEquals(1, $service->getCurrentPage());
    Assert::assertNull($service->getPreviousPage());
    Assert::assertNull($service->getNextPage());
  }

  public function testSinglePageResultSet(): void
  {
    $service = new PaginationService(1, 10, 10);

    Assert::assertEquals(1, $service->getCurrentPage());
    Assert::assertNull($service->getPreviousPage());
    Assert::assertCount(0, $service->getPreviousPages());
    Assert::assertNull($service->getNextPage());
    Assert::assertCount(0, $service->getNextPages());
  }

  public function testWithSingleNextPage(): void
  {
    $service = new PaginationService(1, 20, 10);

    Assert::assertEquals(1, $service->getCurrentPage());
    Assert::assertNull($service->getPreviousPage());
    Assert::assertCount(0, $service->getPreviousPages());
    Assert::assertEquals(2, $service->getNextPage());
    Assert::assertCount(1, $service->getNextPages());
    Assert::assertEquals([2], $service->getNextPages());
  }

  public function testWithMultipleNextPages(): void
  {
    $service = new PaginationService(1, 30, 10);

    Assert::assertEquals(1, $service->getCurrentPage());
    Assert::assertNull($service->getPreviousPage());
    Assert::assertCount(0, $service->getPreviousPages());
    Assert::assertEquals(2, $service->getNextPage());
    Assert::assertCount(2, $service->getNextPages());
    Assert::assertEquals([2, 3], $service->getNextPages());
  }

  public function testWithSeveralNextPages(): void
  {
    $service = new PaginationService(1, 50, 10);

    Assert::assertEquals(1, $service->getCurrentPage());
    Assert::assertNull($service->getPreviousPage());
    Assert::assertCount(0, $service->getPreviousPages());
    Assert::assertEquals(2, $service->getNextPage());
    Assert::assertCount(3, $service->getNextPages(3));
    Assert::assertEquals([2, 3, 4], $service->getNextPages(3));
  }

  public function testWithSinglePreviousPage(): void
  {
    $service = new PaginationService(2, 20, 10);

    Assert::assertEquals(2, $service->getCurrentPage());
    Assert::assertEquals(1, $service->getPreviousPage());
    Assert::assertCount(1, $service->getPreviousPages());
    Assert::assertEquals([1], $service->getPreviousPages());
    Assert::assertNull($service->getNextPage());
    Assert::assertCount(0, $service->getNextPages());
  }

  public function testWithMultiplePreviousPages(): void
  {
    $service = new PaginationService(3, 30, 10);

    Assert::assertEquals(3, $service->getCurrentPage());
    Assert::assertEquals(2, $service->getPreviousPage());
    Assert::assertCount(2, $service->getPreviousPages());
    Assert::assertEquals([1, 2], $service->getPreviousPages());
    Assert::assertNull($service->getNextPage());
    Assert::assertCount(0, $service->getNextPages());
  }

  public function testWithSeveralPreviousPages(): void
  {
    $service = new PaginationService(5, 50, 10);

    Assert::assertEquals(5, $service->getCurrentPage());
    Assert::assertEquals(4, $service->getPreviousPage());
    Assert::assertCount(3, $service->getPreviousPages(3));
    Assert::assertEquals([2, 3, 4], $service->getPreviousPages(3));
    Assert::assertNull($service->getNextPage());
    Assert::assertCount(0, $service->getNextPages());
  }
}
