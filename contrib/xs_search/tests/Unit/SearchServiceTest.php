<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_search\SearchService;
use Drupal\xs_search\XsSearch;
use Drupal\xs_solr\Solr\Collection;
use Drupal\xs_solr\Solr\Search\QueryResponse;
use Drupal\xs_solr\Solr\SolrClient;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
final class SearchServiceTest extends TestCase
{
  public function testServiceCanBeCreatedWithConfig(): void
  {
    $solrClient    = M::mock(SolrClient::class);
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('search_collection')
          ->andReturn('foo');
      });

      $mock->shouldReceive('get')
        ->with(XsSearch::SETTINGS_KEY)
        ->andReturn($config);
    });

    $instance = SearchService::create($solrClient, $configFactory, M::mock(LoggerChannelFactoryInterface::class));

    Assert::assertInstanceOf(SearchService::class, $instance);
  }

  public function testServiceCanBeCreatedWithoutConfig(): void
  {
    $solrClient    = M::mock(SolrClient::class);
    $configFactory = M::mock(ConfigFactoryInterface::class, function(MI $mock) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('search_collection')
          ->andReturnNull();
      });

      $mock->shouldReceive('get')
        ->with(XsSearch::SETTINGS_KEY)
        ->andReturn($config);
    });

    $loggerChannelFactory = M::mock(LoggerChannelFactoryInterface::class, function(MI $mock) {
      $loggerChannel = M::mock(LoggerChannelInterface::class)
        ->shouldReceive('warning')
        ->with(
          'The "search_collection" config value is missing or invalid. Using fallback value "@value".',
          ['@value' => 'search_collection']
        )->getMock();

      $mock->shouldReceive('get')
        ->with('xs_search')
        ->andReturn($loggerChannel);
    });

    $instance = SearchService::create($solrClient, $configFactory, $loggerChannelFactory);

    Assert::assertInstanceOf(SearchService::class, $instance);
  }

  public function testSuggestReturnsQueryResponse(): void
  {
    $collectionName = 'lorem';
    $response       = ['foo' => ['bar' => 'baz']];

    $collection = M::mock(Collection::class)
      ->shouldReceive('suggest')
      ->andReturn(new QueryResponse($response))
      ->getMock();

    $solrClient = M::mock(SolrClient::class)
      ->shouldReceive('collection')
      ->with($collectionName)
      ->andReturn($collection)
      ->getMock();

    $searchService = new SearchService($solrClient, $collectionName);

    Assert::assertEquals($response, $searchService->suggest('', '', '')->getRawResponse());
  }

  public function testSuggestReturnsNull(): void
  {
    $collectionName = 'lorem';

    $collection = M::mock(Collection::class)
      ->shouldReceive('suggest')
      ->andReturn(NULL)
      ->getMock();

    $solrClient = M::mock(SolrClient::class)
      ->shouldReceive('collection')
      ->with($collectionName)
      ->andReturn($collection)
      ->getMock();

    $searchService = new SearchService($solrClient, $collectionName);

    Assert::assertNull($searchService->suggest('', '', ''));
  }
}
