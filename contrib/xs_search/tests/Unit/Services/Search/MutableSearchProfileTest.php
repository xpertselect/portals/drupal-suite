<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit\Services\Search;

use Drupal\xs_search\QueryString\ParsedQuery;
use Drupal\xs_search\Services\Search\MutableSearchProfile;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
class MutableSearchProfileTest extends TestCase
{
  public function testDefaultFilters(): void
  {
    $searchProfile = new MutableSearchProfile();

    Assert::assertEquals([], $searchProfile->standardFilters());
    Assert::assertEquals([], $searchProfile->fieldList());
    Assert::assertEquals('select', $searchProfile->requestHandler());
    Assert::assertEquals(10, $searchProfile->resultsPerPage());
    Assert::assertEquals([], $searchProfile->additionalParameters());
    Assert::assertEquals([], $searchProfile->sortOptions());
    Assert::assertEquals('score DESC', $searchProfile->sort());
    Assert::assertEquals([], $searchProfile->facetMapping());
  }

  public function testConstructorValuesFilters(): void
  {
    $filters              = ['type:("type")'];
    $fieldList            = ['type'];
    $facetFieldList       = [];
    $requestHandler       = 'suggest';
    $amountOfResults      = 5;
    $additionalParameters = ['hl' => TRUE];
    $sortOptions          = ['type' => [
      'label'     => 'label',
      'solrQuery' => 'solrQuery',
    ]];
    $sortCallback = function(?ParsedQuery $parsedQuery = NULL) {
      return 'other sorting';
    };
    $facetMapping = ['type' => 'Type'];

    $searchProfile = new MutableSearchProfile(
      $filters,
      $fieldList,
      $facetFieldList,
      $requestHandler,
      $amountOfResults,
      $additionalParameters,
      $sortOptions,
      $sortCallback,
      $facetMapping
    );

    Assert::assertEquals($filters, $searchProfile->standardFilters());
    Assert::assertEquals($fieldList, $searchProfile->fieldList());
    Assert::assertEquals($requestHandler, $searchProfile->requestHandler());
    Assert::assertEquals($amountOfResults, $searchProfile->resultsPerPage());
    Assert::assertEquals($additionalParameters, $searchProfile->additionalParameters());
    Assert::assertEquals($sortOptions, $searchProfile->sortOptions());
    Assert::assertEquals($sortCallback(), $searchProfile->sort());
    Assert::assertEquals($facetMapping, $searchProfile->facetMapping());
  }

  public function testSetValuesFilters(): void
  {
    $filters              = ['type:("type")'];
    $fieldList            = ['type'];
    $requestHandler       = 'suggest';
    $amountOfResults      = 5;
    $additionalParameters = ['hl' => TRUE];
    $sortOptions          = ['type' => [
      'label'     => 'label',
      'solrQuery' => 'solrQuery',
    ]];
    $sortCallback = function(?ParsedQuery $parsedQuery = NULL) {
      return 'other sorting';
    };
    $facetMapping = ['type' => 'Type'];

    $searchProfile = new MutableSearchProfile();

    $searchProfile->setFilters($filters);
    $searchProfile->setFieldsToFieldList($fieldList);
    $searchProfile->setRequestHandler($requestHandler);
    $searchProfile->setResultsPerPage($amountOfResults);
    $searchProfile->setAdditionalParameters($additionalParameters);
    $searchProfile->setSortOptions($sortOptions);
    $searchProfile->setSortCallback($sortCallback);
    $searchProfile->setFacetMapping($facetMapping);

    Assert::assertEquals($filters, $searchProfile->standardFilters());
    Assert::assertEquals($fieldList, $searchProfile->fieldList());
    Assert::assertEquals($requestHandler, $searchProfile->requestHandler());
    Assert::assertEquals($amountOfResults, $searchProfile->resultsPerPage());
    Assert::assertEquals($additionalParameters, $searchProfile->additionalParameters());
    Assert::assertEquals($sortOptions, $searchProfile->sortOptions());
    Assert::assertEquals($sortCallback(), $searchProfile->sort());
    Assert::assertEquals($facetMapping, $searchProfile->facetMapping());
  }

  public function testAddValuesFilters(): void
  {
    $searchProfile = new MutableSearchProfile(
      ['type:("type")', 'visibility:"public"'],
      ['type', 'visibility'],
      [],
      'suggest',
      5,
      ['hl' => TRUE, 'hl.method' => 'unified'],
      ['type' => [
        'label'     => 'label',
        'solrQuery' => 'solrQuery',
      ]],
      function(?ParsedQuery $parsedQuery = NULL) {
        return 'other sorting';
      },
      ['type' => 'Type', 'visibility' => 'visibility']
    );

    $searchProfile->addFilters(['visibility:"public"', 'visibility:"private"']);
    $searchProfile->addFieldsToFieldList(['visibility', 'title']);
    $searchProfile->addAdditionalParameters(['hl.method' => 'unified', 'hl.snippets' => 1]);
    $searchProfile->addSortOptions(['type2' => [
      'label'     => 'label2',
      'solrQuery' => 'solrQuery2',
    ]]);
    $searchProfile->addFacetMapping(['visibility' => 'visibility', 'title' => 'Title']);

    Assert::assertEquals(['type:("type")', 'visibility:"public"', 'visibility:"private"'], $searchProfile->standardFilters());
    Assert::assertEquals(['type', 'visibility', 'title'], $searchProfile->fieldList());
    Assert::assertEquals(['hl' => TRUE, 'hl.method' => 'unified', 'hl.snippets' => 1], $searchProfile->additionalParameters());
    Assert::assertEquals([
      'type' => [
        'label'     => 'label',
        'solrQuery' => 'solrQuery',
      ],
      'type2' => [
        'label'     => 'label2',
        'solrQuery' => 'solrQuery2',
      ],
    ], $searchProfile->sortOptions());
    Assert::assertEquals(['type' => 'Type', 'visibility' => 'visibility', 'title' => 'Title'], $searchProfile->facetMapping());
  }

  public function testFacetFields(): void
  {
    $facetFieldList       = ['facet_type'];
    $additionalParameters = ['a' => 'b'];

    $searchProfile = new MutableSearchProfile(
      facetFieldList: $facetFieldList,
      additionalParameters: $additionalParameters
    );

    Assert::assertEquals(['facet.field' => ['facet_type'], 'a' => 'b'], $searchProfile->additionalParameters());
  }

  public function testFacetFieldsOverwritesAdditionalParameters()
  {
    $facetFieldList       = ['facet_type'];
    $additionalParameters = ['facet.field' => ['facet_lorem'], 'a' => 'b'];

    $searchProfile = new MutableSearchProfile(
      facetFieldList: $facetFieldList,
      additionalParameters: $additionalParameters
    );

    Assert::assertEquals(['facet.field' => ['facet_type'], 'a' => 'b'], $searchProfile->additionalParameters());
  }
}
