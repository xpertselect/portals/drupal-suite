<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit\Response;

use Drupal\xs_search\Response\CorrectedSpellingResponse;
use Drupal\xs_solr\Solr\Search\QueryResponse;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
final class CorrectedSpellingResponseTest extends TestCase
{
  public function testGetters(): void
  {
    $queryResponse = new QueryResponse();
    $query         = 'foo';
    $originalQuery = 'bar';
    $originalCount = 20;

    $response = new CorrectedSpellingResponse($queryResponse, $query, $originalQuery, $originalCount);

    Assert::assertEquals($queryResponse, $response->getSolrResponse());
    Assert::assertEquals($query, $response->getQuery());
    Assert::assertEquals($originalQuery, $response->getOriginalQuery());
    Assert::assertEquals($originalCount, $response->getOriginalCount());
  }
}
