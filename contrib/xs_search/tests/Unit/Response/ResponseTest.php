<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search\Unit\Response;

use Drupal\xs_search\Response\Response;
use Drupal\xs_solr\Solr\Search\QueryResponse;
use Drupal\xs_solr\Solr\Search\Spellcheck;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_search\TestCase;

/**
 * @internal
 */
final class ResponseTest extends TestCase
{
  public function testGetters(): void
  {
    $queryResponse = new QueryResponse();
    $query         = 'foo';

    $response = new Response($queryResponse, $query);

    Assert::assertEquals($queryResponse, $response->getSolrResponse());
    Assert::assertEquals($query, $response->getQuery());
  }

  public function testGetSuggestionWithoutSpellcheck(): void
  {
    $queryResponse = M::mock(QueryResponse::class, function(MI $mock) {
      $mock->shouldReceive('getSpellcheck')->andReturnNull();
    });

    $response = new Response($queryResponse, 'foo');

    Assert::assertNull($response->getSuggestion());
  }

  public function testGetSuggestionWhenCollationIsMissing(): void
  {
    $queryResponse = M::mock(QueryResponse::class, function(MI $mock) {
      $spellcheck = M::mock(Spellcheck::class, function(MI $mock) {
        $mock->shouldReceive('getCollations')->andReturn([]);
      });

      $mock->shouldReceive('getSpellcheck')->andReturn($spellcheck);
    });

    $response = new Response($queryResponse, 'foo');

    Assert::assertNull($response->getSuggestion());
  }

  public function testGetSuggestionReturnsCollation(): void
  {
    $queryResponse = M::mock(QueryResponse::class, function(MI $mock) {
      $spellcheck = M::mock(Spellcheck::class, function(MI $mock) {
        $mock->shouldReceive('getCollations')->andReturn([
          'collation' => 'my-collation',
        ]);
      });

      $mock->shouldReceive('getSpellcheck')->andReturn($spellcheck);
    });

    $response = new Response($queryResponse, 'foo');

    Assert::assertEquals('my-collation', $response->getSuggestion());
  }
}
