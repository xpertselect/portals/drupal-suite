<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_search;

use Drupal\xs_search\QueryString\ParsedQuery;
use Drupal\xs_search\SearchProfileInterface;
use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * @internal
 */
abstract class TestCase extends BaseTestCase
{
  public function dummySearchProfile(): SearchProfileInterface
  {
    return new class () implements SearchProfileInterface {
      public function requestHandler(): string
      {
        return 'select';
      }

      public function standardFilters(): array
      {
        return [];
      }

      public function resultsPerPage(): int
      {
        return 10;
      }

      public function fieldList(): array
      {
        return [];
      }

      public function additionalParameters(): array
      {
        return [];
      }

      public function sortOptions(): array
      {
        return [
          'relevance'  => 'score DESC',
          'title:desc' => 'title DESC',
        ];
      }

      public function sort(?ParsedQuery $parsedQuery = NULL): string
      {
        return 'score DESC';
      }

      public function facetMapping(): array
      {
        return [
          'foo' => 'foo',
        ];
      }
    };
  }
}
