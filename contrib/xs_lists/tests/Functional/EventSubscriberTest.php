<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Functional;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\xs_api\Event\PreGenerationEvent;
use Drupal\xs_lists\EventSubscriber\ApiSpecificationGenerationSubscriber;
use Drupal\xs_lists\ListClient;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Tests\xs_lists\TestCase;

/**
 * @internal
 */
final class EventSubscriberTest extends TestCase
{
  public static function createConfigurationDataset(): array
  {
    return [
      [[], []],
      [[['name' => 'foo']], ['foo']],
      [[['name' => 'foo'], ['name' => 'bar']], ['foo', 'bar']],
    ];
  }

  /**
   * @dataProvider createConfigurationDataset
   */
  public function testCreateCorrectlyParsesAvailableConfiguration(array $configuration, array $expected): void
  {
    $listClient      = M::mock(ListClient::class);
    $eventDispatcher = M::mock(EventDispatcherInterface::class);
    $configFactory   = M::mock(ConfigFactoryInterface::class, function(MI $mock) use ($configuration) {
      $config = M::mock(ImmutableConfig::class, function(MI $mock) use ($configuration) {
        $mock->shouldReceive('getRawData')->andReturn($configuration);
      });

      $mock->shouldReceive('get')
        ->with('xs_lists.lists')
        ->andReturn($config);
    });

    $subscriber = ApiSpecificationGenerationSubscriber::create($listClient, $configFactory, $eventDispatcher);

    Assert::assertEquals($expected, $subscriber->getLists());
  }

  public function testSubscriberListensToApiGenerationEvent(): void
  {
    $subscribedEvents = ApiSpecificationGenerationSubscriber::getSubscribedEvents();

    Assert::assertArrayHasKey(PreGenerationEvent::class, $subscribedEvents);
  }
}
