<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit\Controller;

use Drupal\Core\Render\RendererInterface;
use Drupal\xs_lists\Controller\AdminListStatusController;
use Drupal\xs_lists\ListClient;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Tests\xs_lists\TestCase;

/**
 * @internal
 */
final class AdminListStatusControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    try {
      $container = M::mock(ContainerInterface::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('xs_lists.list_client')
          ->andReturn(M::mock(ListClient::class));

        $mock->shouldReceive('get')
          ->with('renderer')
          ->andReturn(M::mock(RendererInterface::class));
      });

      Assert::assertInstanceOf(
        AdminListStatusController::class,
        AdminListStatusController::create($container)
      );
    } catch (ServiceNotFoundException|ServiceCircularReferenceException $e) {
      $this->fail($e->getMessage());
    }
  }
}
