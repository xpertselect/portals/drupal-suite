<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit\Mapper;

use Drupal\xs_lists\Mapper\TranslationMapperInterface;
use Drupal\xs_lists\Mapper\TranslationMapperManager;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Tests\xs_searchable_content\TestCase;

/**
 * @internal
 */
final class TranslationMapperManagerTest extends TestCase
{
  public function testListSingleMapperNoPriority(): void
  {
    $list = ['foo' => ['labels' => ['nl' => 'bar', 'en' => 'baz']]];
    $name = 'foo';

    $translationMapper = M::mock(TranslationMapperInterface::class);
    $translationMapper->shouldReceive('mapping')->andReturn($list);
    $translationMapper->shouldReceive('name')->andReturn($name);

    $translationMapperManager = new TranslationMapperManager();
    $translationMapperManager->addTranslationMapper($translationMapper);

    Assert::assertEquals($list, $translationMapperManager->list($name));
  }

  public function testListMultipleMappersNoPriority(): void
  {
    $list1 = ['foo' => ['labels' => ['nl' => 'bar', 'en' => 'baz']]];
    $list2 = ['a' => ['labels' => ['nl' => 'b', 'en' => 'c']]];
    $name  = 'foo';

    $translationMapper1 = M::mock(TranslationMapperInterface::class);
    $translationMapper1->shouldReceive('mapping')->andReturn($list1);
    $translationMapper1->shouldReceive('name')->andReturn($name);

    $translationMapper2 = M::mock(TranslationMapperInterface::class);
    $translationMapper2->shouldReceive('mapping')->andReturn($list2);
    $translationMapper2->shouldReceive('name')->andReturn($name);

    $translationMapperManager = new TranslationMapperManager();
    $translationMapperManager->addTranslationMapper($translationMapper1);
    $translationMapperManager->addTranslationMapper($translationMapper2);

    Assert::assertEquals(array_merge($list1, $list2), $translationMapperManager->list($name));
  }

  public function testListHigherPriorityMapperOverwritesLowerPriorityMapper()
  {
    $list1 = ['foo' => ['labels' => ['nl' => 'bar', 'en' => 'baz']]];
    $list2 = ['foo' => ['labels' => ['nl' => 'b', 'en' => 'c']]];
    $name  = 'foo';

    $translationMapper1 = M::mock(TranslationMapperInterface::class);
    $translationMapper1->shouldReceive('mapping')->andReturn($list1);
    $translationMapper1->shouldReceive('name')->andReturn($name);

    $translationMapper2 = M::mock(TranslationMapperInterface::class);
    $translationMapper2->shouldReceive('mapping')->andReturn($list2);
    $translationMapper2->shouldReceive('name')->andReturn($name);

    $translationMapperManager = new TranslationMapperManager();
    $translationMapperManager->addTranslationMapper($translationMapper1);
    $translationMapperManager->addTranslationMapper($translationMapper2, 1);

    Assert::assertEquals($list2, $translationMapperManager->list($name));
  }

  public function testEmptyManagerEmptyList()
  {
    $translationMapperManager = new TranslationMapperManager();

    Assert::assertEquals([], $translationMapperManager->list('foo'));
  }

  public function testNoNamedMappersEmptyList()
  {
    $translationMapper = M::mock(TranslationMapperInterface::class);
    $translationMapper->shouldReceive('mapping')
      ->andReturn(['foo' => ['labels' => ['nl' => 'bar', 'en' => 'baz']]]);
    $translationMapper->shouldReceive('name')->andReturn('foo');

    $translationMapperManager = new TranslationMapperManager();
    $translationMapperManager->addTranslationMapper($translationMapper);

    Assert::assertEquals([], $translationMapperManager->list('bar'));
  }
}
