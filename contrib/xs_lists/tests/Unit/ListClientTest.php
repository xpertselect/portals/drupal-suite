<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_lists\ListClient;
use Drupal\xs_lists\Lists\TranslatableListFactory;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_lists\TestCase;

/**
 * @internal
 */
final class ListClientTest extends TestCase
{
  public function testSameFormatterIsReturnedOnEachInvocation(): void
  {
    $listClient = new ListClient(
      M::mock(TranslatableListFactory::class),
      M::mock(LoggerChannelInterface::class),
      M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('getRawData')->andReturn([]);
      })
    );

    Assert::assertSame($listClient->formatter(), $listClient->formatter());
  }

  public function testWhenNonExistentListIsRequestedEmptyListIsReturned(): void
  {
    $listClient = new ListClient(
      M::mock(TranslatableListFactory::class, function(MI $mock) {
        $mock->shouldReceive('getLanguageId')
          ->andReturn('en-US');
      })->makePartial(),
      M::mock(LoggerChannelInterface::class, function(MI $mock) {
        $mock->shouldReceive('warning');
      }),
      M::mock(ImmutableConfig::class, function(MI $mock) {
        $mock->shouldReceive('getRawData')->andReturn([]);
      })
    );

    $list = $listClient->list('foo');

    Assert::assertFalse($list->hasData());
    Assert::assertEquals('n/a', $list->getSource());
  }
}
