<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit\TwigExtension;

use Drupal\xs_lists\ListClient;
use Drupal\xs_lists\Lists\TranslatableList;
use Drupal\xs_lists\TwigExtension\TranslatableListTwigExtension;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_lists\TestCase;
use Twig\TwigFilter;

/**
 * @internal
 */
final class TranslatableListTwigExtensionTest extends TestCase
{
  public function testExposedFiltersAreTheCorrectClass(): void
  {
    $extension = new TranslatableListTwigExtension(M::mock(ListClient::class));
    $filters   = $extension->getFilters();

    Assert::assertNotEmpty($filters);
    Assert::assertContainsOnlyInstancesOf(TwigFilter::class, $filters);
  }

  public function testLabelToIdentifierReturnsLabelWhenListHasNoData(): void
  {
    $listClient = M::mock(ListClient::class, function(MI $mock) {
      $list = M::mock(TranslatableList::class, function(MI $mock) {
        $mock->shouldReceive('hasData')
          ->andReturnFalse();
      });

      $mock->shouldReceive('list')
        ->with('foo')
        ->andReturn($list);
    });

    $extension = new TranslatableListTwigExtension($listClient);

    Assert::assertEquals('lorem', $extension->labelToIdentifier('lorem', 'foo'));
  }

  public function testLabelToIdentifierDelegatesToList(): void
  {
    $listClient = M::mock(ListClient::class, function(MI $mock) {
      $list = M::mock(TranslatableList::class, function(MI $mock) {
        $mock->shouldReceive('hasData')
          ->andReturnTrue();

        $mock->shouldReceive('labelToIdentifier')
          ->with('lorem')
          ->andReturn('ipsum');
      });

      $mock->shouldReceive('list')
        ->with('foo')
        ->andReturn($list);
    });

    $extension = new TranslatableListTwigExtension($listClient);

    Assert::assertEquals('ipsum', $extension->labelToIdentifier('lorem', 'foo'));
  }

  public function testIdentifierToLabelReturnsIdentifierWhenListHasNoData(): void
  {
    $listClient = M::mock(ListClient::class, function(MI $mock) {
      $list = M::mock(TranslatableList::class, function(MI $mock) {
        $mock->shouldReceive('hasData')
          ->andReturnFalse();
      });

      $mock->shouldReceive('list')
        ->with('foo')
        ->andReturn($list);
    });

    $extension = new TranslatableListTwigExtension($listClient);

    Assert::assertEquals('lorem', $extension->identifierToLabel('lorem', 'foo'));
  }

  public function testIdentifierToLabelDelegatesToList(): void
  {
    $listClient = M::mock(ListClient::class, function(MI $mock) {
      $list = M::mock(TranslatableList::class, function(MI $mock) {
        $mock->shouldReceive('hasData')
          ->andReturnTrue();

        $mock->shouldReceive('identifierToLabel')
          ->with('lorem')
          ->andReturn('ipsum');
      });

      $mock->shouldReceive('list')
        ->with('foo')
        ->andReturn($list);
    });

    $extension = new TranslatableListTwigExtension($listClient);

    Assert::assertEquals('ipsum', $extension->identifierToLabel('lorem', 'foo'));
  }

  public function testLabelToIdentifierWhenInputIsNullReturnsEmptyString(): void
  {
    $listClient = M::mock(ListClient::class);

    $extension = new TranslatableListTwigExtension($listClient);
    $actual    = $extension->labelToIdentifier(NULL, 'foo');

    Assert::assertIsString($actual);
    Assert::assertEquals('', $actual);
  }

  public function testIdentifierToLabelWhenInputIsNullReturnsEmptyString(): void
  {
    $listClient = M::mock(ListClient::class);

    $extension = new TranslatableListTwigExtension($listClient);
    $actual    = $extension->identifierToLabel(NULL, 'foo');

    Assert::assertIsString($actual);
    Assert::assertEquals('', $actual);
  }
}
