<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit\Lists;

use Drupal\xs_lists\Lists\Formatter;
use Drupal\xs_lists\Lists\TranslatableList;
use PHPUnit\Framework\Assert;
use Tests\xs_lists\TestCase;

/**
 * @internal
 */
final class FormatterTest extends TestCase
{
  public static function flattenDataset(): array
  {
    return [
      [
        [],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']]],
        ['foo' => 'Foo'],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => ['labels' => ['en-US' => 'Bar']]],
        ['foo' => 'Foo', 'bar' => 'Bar'],
      ],
      [
        ['bar' => ['labels' => ['baz' => 'Bar']]],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => ['labels' => ['baz' => 'Bar']]],
        ['foo' => 'Foo'],
      ],
      [
        ['bar' => ['baz' => ['en-US' => 'Bar']]],
        [],
      ],
    ];
  }

  public static function sortedDataset(): array
  {
    return [
      [
        [],
        [],
      ],
      [
        ['foo' => [], 'bar' => ['labels' => ['en-US' => 'Bar']]],
        ['foo' => [], 'bar' => ['labels' => ['en-US' => 'Bar']]],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => []],
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => []],
      ],
      [
        ['foo' => [], 'bar' => ['labels' => ['' => 'Bar']]],
        ['foo' => [], 'bar' => ['labels' => ['' => 'Bar']]],
      ],
      [
        ['foo' => ['labels' => ['' => 'Foo']], 'bar' => []],
        ['foo' => ['labels' => ['' => 'Foo']], 'bar' => []],
      ],
      [
        ['foo' => ['labels' => ['' => 'Foo']], 'bar' => ['labels' => ['' => 'Bar']]],
        ['foo' => ['labels' => ['' => 'Foo']], 'bar' => ['labels' => ['' => 'Bar']]],
      ],
      [
        ['bar' => ['labels' => ['' => 'Bar']], 'foo' => ['labels' => ['' => 'Foo']]],
        ['bar' => ['labels' => ['' => 'Bar']], 'foo' => ['labels' => ['' => 'Foo']]],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => ['labels' => ['en-US' => 'Bar']]],
        ['bar' => ['labels' => ['en-US' => 'Bar']], 'foo' => ['labels' => ['en-US' => 'Foo']]],
      ],
    ];
  }

  public static function groupedDataset(): array
  {
    return [
      [
        [],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']]],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => NULL]],
        ['null' => ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => 'null']]],
        // Note that below is maybe (more) reasonable to expect
        // ['__ungrouped' => ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => NULL]]],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => NULL], 'bar' => ['labels' => ['en-US' => 'Bar'], 'field' => NULL]],
        ['null' => ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => 'null'], 'bar' => ['labels' => ['en-US' => 'Bar'], 'field' => 'null']]],
        // Note that below is maybe (more) reasonable to expect
        // ['__ungrouped' => ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => NULL], 'bar' => ['labels' => ['en-US' => 'Bar'], 'field' => NULL]],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => 'group']],
        ['group' => ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => 'group']]],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => 'group'], 'bar' => ['labels' => ['en-US' => 'Bar'], 'field' => 'group']],
        ['group' => ['foo' => ['labels' => ['en-US' => 'Foo'], 'field' => 'group'], 'bar' => ['labels' => ['en-US' => 'Bar'], 'field' => 'group']]],
      ],
    ];
  }

  public static function hierarchicalDataset(): array
  {
    return [
      [
        [],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']]],
        ['foo' => '<b>Foo</b>'],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => ['labels' => ['en-US' => 'Bar']]],
        ['foo' => '<b>Foo</b>', 'bar' => '<b>Bar</b>'],
      ],
      [
        ['bar' => ['labels' => ['baz' => 'Bar']]],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => ['labels' => ['baz' => 'Bar']]],
        ['foo' => '<b>Foo</b>'],
      ],
      [
        ['bar' => ['baz' => ['en-US' => 'Bar']]],
        [],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'parent' => NULL], 'bar' => ['labels' => ['en-US' => 'Bar'], 'parent' => 'foo']],
        ['foo' => '<b>Foo</b>', 'bar' => '<b>Foo</b> — Bar'],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo']], 'bar' => ['labels' => ['en-US' => 'Bar'], 'parent' => 'foo']],
        ['foo' => '<b>Foo</b>', 'bar' => '<b>Foo</b> — Bar'],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'parent' => NULL], 'bar' => ['labels' => ['en-US' => 'Bar'], 'parent' => 'foo'], 'baz' => ['labels' => ['en-US' => 'Baz'], 'parent' => 'bar']],
        ['foo' => '<b>Foo</b>', 'bar' => '<b>Foo</b> — Bar', 'baz' => '<b>Foo</b> — <b>Bar</b> — Baz'],
      ],
      [
        ['foo' => ['labels' => ['en-US' => 'Foo'], 'parent' => NULL], 'bar' => ['NO-LABEL' => ['en-US' => 'Bar'], 'parent' => 'foo'], 'baz' => ['labels' => ['en-US' => 'Baz'], 'parent' => 'bar']],
        ['foo' => '<b>Foo</b>', 'baz' => '<b>Foo</b> — Baz'],
      ],
    ];
  }

  /**
   * @dataProvider flattenDataset
   */
  public function testFlattenWithDataset(array $listData, array $expected): void
  {
    $list      = new TranslatableList('foo', '', $listData, 'en-US', $expected);
    $formatter = new Formatter();

    Assert::assertEquals($expected, $formatter->flatten($list));
  }

  /**
   * @dataProvider sortedDataset
   */
  public function testSortedWithDataset(array $listData, array $expected): void
  {
    $list      = new TranslatableList('foo', '', $listData, 'en-US', $expected);
    $formatter = new Formatter();

    Assert::assertEquals($expected, $formatter->sorted($list));
  }

  /**
   * @dataProvider groupedDataset
   */
  public function testGroupedWithDataset(array $listData, array $expected): void
  {
    $list      = new TranslatableList('foo', '', $listData, 'en-US', $expected);
    $formatter = new Formatter();

    Assert::assertEquals($expected, $formatter->grouped($list, 'field'));
  }

  /**
   * @dataProvider hierarchicalDataset
   */
  public function testHierarchicalWithDataset(array $listData, array $expected): void
  {
    $list      = new TranslatableList('foo', '', $listData, 'en-US', $expected);
    $formatter = new Formatter();

    Assert::assertEquals($expected, $formatter->hierarchical($list));
  }
}
