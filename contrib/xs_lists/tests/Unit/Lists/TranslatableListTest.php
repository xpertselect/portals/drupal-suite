<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit\Lists;

use Drupal\xs_lists\Lists\TranslatableList;
use PHPUnit\Framework\Assert;
use Tests\xs_lists\TestCase;

/**
 * @internal
 */
final class TranslatableListTest extends TestCase
{
  public static function untranslatableLabelDataset(): array
  {
    return [
      [[], 'foo'],
      [['bar' => []], 'foo'],
      [['bar' => ['labels' => []]], 'foo'],
      [['bar' => ['labels' => ['en-US' => NULL]]], 'foo'],
      [['bar' => ['labels' => ['en-US' => 'bar']]], 'foo'],
    ];
  }

  public static function untranslatableIdentifierDataset(): array
  {
    return [
      [[], 'bar'],
      [['bar' => []], 'bar'],
      [['bar' => ['labels' => []]], 'bar'],
      [['bar' => ['labels' => ['en-US' => 'bar']]], 'bar'],
    ];
  }

  public static function entryHasFieldEmptyValueDataset(): array
  {
    return [
      [[]],
      [['identifier' => []]],
      [['identifier' => ['field' => NULL]]],
      [['identifier' => ['field' => FALSE]]],
      [['identifier' => ['field' => 0]]],
      [['identifier' => ['field' => []]]],
    ];
  }

  public function testGettersAndSetters(): void
  {
    $title    = 'foo:bar';
    $source   = 'https://example.com/baz.json';
    $data     = ['my' => [], 'data' => [], 'set' => []];
    $language = 'en-US';
    $alias    = ['bar:foo'];

    $list = new TranslatableList($title, $source, $data, $language, $alias);

    Assert::assertEquals($title, $list->getTitle());
    Assert::assertEquals($source, $list->getSource());
    Assert::assertEquals($data, $list->getData());
    Assert::assertEquals($language, $list->getLanguageCode());
    Assert::assertEquals($alias, $list->getAliases());
    Assert::assertEquals(['my', 'data', 'set'], $list->getIdentifiers());
  }

  public function testHasDataChecks(): void
  {
    $list = new TranslatableList('foo', 'bar', [], 'en-US', []);

    Assert::assertFalse($list->hasData());

    $list->setData(['my' => [], 'data' => [], 'set' => []]);

    Assert::assertTrue($list->hasData());
  }

  /**
   * @dataProvider untranslatableLabelDataset
   */
  public function testLabelToIdentifierReturnsLabelWhenNoTranslationsCanBeMade(array $data, string $label): void
  {
    $list = new TranslatableList('foo', 'bar', $data, 'en-US', []);

    Assert::assertEquals($label, $list->labelToIdentifier($label));
  }

  public function testLabelToIdentifierCanBeTranslated(): void
  {
    $list = new TranslatableList('foo', 'bar', ['bar' => ['labels' => ['en-US' => 'foo']]], 'en-US', []);

    Assert::assertEquals('bar', $list->labelToIdentifier('foo'));
  }

  /**
   * @dataProvider untranslatableIdentifierDataset
   */
  public function testIdentifierToLabelReturnsIdentifierWhenNoTranslationCanBeMade(array $data, string $identifier): void
  {
    $list = new TranslatableList('foo', 'bar', $data, 'en-US', []);

    Assert::assertEquals($identifier, $list->identifierToLabel($identifier));
  }

  public function testIdentifierToLabelCanBeTranslated(): void
  {
    $list = new TranslatableList('foo', 'bar', ['bar' => ['labels' => ['en-US' => 'foo']]], 'en-US', []);

    Assert::assertEquals('foo', $list->identifierToLabel('bar'));
  }

  /**
   * @dataProvider entryHasFieldEmptyValueDataset
   */
  public function testEntryHasFieldFailsWithGivenDataset(array $data): void
  {
    $list = new TranslatableList('foo', 'bar', $data, 'en-US', []);

    Assert::assertFalse($list->entryHasField('identifier', 'field'));
  }

  public function testEntryHasFieldSucceeds(): void
  {
    $list = new TranslatableList('foo', 'bar', ['identifier' => ['field' => 'value']], 'en-US', []);

    Assert::assertTrue($list->entryHasField('identifier', 'field'));
  }
}
