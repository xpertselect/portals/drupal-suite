<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_lists\Unit\Event;

use Drupal\xs_lists\Event\ListToEnumEvent;
use Drupal\xs_lists\Lists\TranslatableList;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\StoppableEventInterface;
use Tests\xs_lists\TestCase;

/**
 * @internal
 */
final class ListToEnumEventTest extends TestCase
{
  public function testEventIsStoppable(): void
  {
    $event = new ListToEnumEvent(M::mock(TranslatableList::class));

    Assert::assertInstanceOf(StoppableEventInterface::class, $event);
  }

  public function testEventDoesNotStopPropagation(): void
  {
    $event = new ListToEnumEvent(M::mock(TranslatableList::class));

    Assert::assertFalse($event->isPropagationStopped());
  }
}
