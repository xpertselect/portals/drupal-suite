<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_api\Event\PreGenerationEvent;
use Drupal\xs_lists\Event\ListToEnumEvent;
use Drupal\xs_lists\ListClient;
use Drupal\xs_lists\Lists\TranslatableList;
use OpenApi\Annotations\OpenApi;
use OpenApi\Annotations\Schema;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ApiSpecificationGenerationSubscriber.
 *
 * Listens to events related to the generation of the Open API Specification of this XpertSelect Portals application.
 */
final class ApiSpecificationGenerationSubscriber implements EventSubscriberInterface
{
  /**
   * Creates a new ApiSpecificationGenerationSubscriber instance from the given parameters.
   *
   * @param ListClient               $listClient      The client for interacting with lists
   * @param ConfigFactoryInterface   $configFactory   The Drupal configuration factory
   * @param EventDispatcherInterface $eventDispatcher The service for dispatching events
   *
   * @return self A new instance of the ApiSpecificationGenerationSubscriber class
   */
  public static function create(ListClient $listClient, ConfigFactoryInterface $configFactory,
                                EventDispatcherInterface $eventDispatcher): self
  {
    $lists = array_map(function(array $configEntry) {
      return $configEntry['name'];
    }, $configFactory->get('xs_lists.lists')->getRawData());

    return new self($listClient, $lists, $eventDispatcher);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      PreGenerationEvent::class => 'addListsAsEnums',
    ];
  }

  /**
   * ApiSpecificationGenerationSubscriber constructor.
   *
   * @param ListClient               $listClient The client for interacting with the configured lists
   * @param array<int, string>       $lists      The available lists
   * @param EventDispatcherInterface $dispatcher The event dispatcher
   */
  public function __construct(private readonly ListClient $listClient, private readonly array $lists,
                              private readonly EventDispatcherInterface $dispatcher)
  {
  }

  /**
   * Return the names of the lists that will be included as OAS Enums.
   *
   * @return string[] The names of the lists
   */
  public function getLists(): array
  {
    return $this->lists;
  }

  /**
   * Appends all the configured lists as OAS Enums to the specification that is about to be generated. Only the lists
   * with actual content will be included.
   *
   * @param PreGenerationEvent $event The event to process
   */
  public function addListsAsEnums(PreGenerationEvent $event): void
  {
    if (!$this->specificationHasDesiredState($event->apiSpecification)) {
      return;
    }

    foreach ($this->lists as $listName) {
      $processedEvent = $this->dispatcher->dispatch($this->createListToEnumEvent($listName));

      if (!$processedEvent instanceof ListToEnumEvent) {
        continue;
      }

      if (!$processedEvent->list->hasData()) {
        continue;
      }

      if (is_null($event->apiSpecification->_analysis->context)) {
        continue;
      }

      $event->apiSpecification->_analysis->addAnnotation(
        $this->createEnumFromList($processedEvent->list),
        $event->apiSpecification->_analysis->context
      );
    }
  }

  /**
   * Checks if the specification has the desired structure to be able to include the lists as Enum entries.
   *
   * @param OpenApi $specification The specification to check
   *
   * @return bool Whether the specification has the desired state
   */
  private function specificationHasDesiredState(OpenApi $specification): bool
  {
    return is_array($specification->components->schemas);
  }

  /**
   * Creates a ListToEnumEvent for the given list.
   *
   * @param string $name The name of the list
   *
   * @return ListToEnumEvent The created event
   */
  private function createListToEnumEvent(string $name): ListToEnumEvent
  {
    return new ListToEnumEvent($this->listClient->list($name));
  }

  /**
   * Creates an OAS Enum Schema from the given list.
   *
   * @param TranslatableList $list The list to base the Enum on
   *
   * @return Schema The generated Enum Schema
   */
  private function createEnumFromList(TranslatableList $list): Schema
  {
    $description = sprintf('Acceptable values for fields that use the %s taxonomy', $list->getTitle());

    if (!function_exists($list->getSource()) && !is_file($list->getSource())) {
      $description .= sprintf(' Source: %s', $list->getSource());
    }

    return new Schema([
      'schema'      => $list->getTitle(),
      'type'        => 'string',
      'enum'        => $list->getIdentifiers(),
      'description' => $description,
    ]);
  }
}
