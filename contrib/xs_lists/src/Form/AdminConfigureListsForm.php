<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminConfigureListsForm.
 *
 * Provides the form for configuring the lists of the xs_lists module.
 */
final class AdminConfigureListsForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var CacheTagsInvalidatorInterface $cacheInvalidator */
    $cacheInvalidator = $container->get('cache_tags.invalidator');

    return new AdminConfigureListsForm($configFactory, $cacheInvalidator);
  }

  /**
   * {@inheritdoc}
   *
   * @param CacheTagsInvalidatorInterface $tagsInvalidator The cache tag invalidator to use
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              protected CacheTagsInvalidatorInterface $tagsInvalidator)
  {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_lists_configure_lists_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $listData  = $this->config('xs_lists.lists')->getRawData();
    $lists     = array_values($listData);
    $listCount = $form_state->get('listsCount');

    if (NULL === $listCount) {
      $listCount = count(array_keys($lists)) + 1;
      $form_state->set('listsCount', $listCount);
    }

    $form['lists'] = [
      '#type'   => 'container',
      '#tree'   => TRUE,
      '#prefix' => '
        <div id="lists-wrapper">
          <table>
            <thead>
            <tr>
              <th>' . $this->t('Name') . '</th>
              <th>' . $this->t('Source (function, filepath or URL)') . '</th>
              <th>' . $this->t('Aliases') . '</th>
            </tr>
            </thead>
            <tbody>
      ',
      '#suffix' => '
            </tbody>
          </table>
        </div>
      ',
    ];

    for ($i = 0; $i < $listCount; ++$i) {
      $form['lists'][$i] = [
        'name' => [
          '#type'          => 'textfield',
          '#title'         => $this->t('Name'),
          '#title_display' => 'invisible',
          '#default_value' => $lists[$i]['name'] ?? NULL,
          '#prefix'        => '<tr><td>',
          '#suffix'        => '</td>',
        ],
        'source' => [
          '#type'          => 'textfield',
          '#title'         => $this->t('Source'),
          '#title_display' => 'invisible',
          '#default_value' => $lists[$i]['source'] ?? NULL,
          '#prefix'        => '<td>',
          '#suffix'        => '</td>',
        ],
        'alias' => [
          '#type'          => 'textfield',
          '#title'         => $this->t('Aliases'),
          '#title_display' => 'invisible',
          '#default_value' => $lists[$i]['alias'] ?? NULL,
          '#prefix'        => '<td>',
          '#suffix'        => '</td></tr>',
        ],
      ];
    }

    $form['row']['#type'] = 'actions';
    $form['row']['add']   = [
      '#type'                    => 'submit',
      '#value'                   => $this->t('Add another list'),
      '#submit'                  => ['::addOne'],
      '#limit_validation_errors' => [],
      '#ajax'                    => [
        'callback' => '::addMoreCallback',
        'wrapper'  => 'lists-wrapper',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void
  {
    $values = $form_state->cleanValues()->getValue('lists', []);
    $names  = [];

    foreach ($values as $index => $contents) {
      $count = 0;

      if (empty($contents['name'])) {
        ++$count;
      }

      if (empty($contents['source'])) {
        ++$count;
      }

      if ($count > 0 && $count < 2) {
        $form_state->setErrorByName(
          'lists][' . $index,
          $this->t('Name and source are required')
        );
      }

      if (!empty($contents['name'])) {
        $name = $contents['name'];

        if (in_array($name, $names)) {
          $form_state->setErrorByName(
            'lists][' . $index,
            $this->t('This name is already in use as a name or alias')
          );
        }

        $names[] = $name;
      }

      if (!empty($contents['alias'])) {
        $aliases = explode(',', $contents['alias']);

        foreach ($aliases as $alias) {
          if (in_array($alias, $names)) {
            $alias = trim($alias);

            if (empty($alias)) {
              continue;
            }

            $form_state->setErrorByName(
              'lists][' . $index,
              $this->t('One of the aliases is already in use as a name or alias')
            );
          }

          $names[] = $alias;
        }
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $newConfig = [];
    $lists     = $form_state->cleanValues()->getValue('lists', []);

    foreach ($lists as $list) {
      if (empty($list['name']) || empty($list['source'])) {
        continue;
      }

      $newConfig[$list['name']] = [
        'name'   => $list['name'],
        'source' => $list['source'],
        'alias'  => $list['alias'] ?? '',
      ];
    }

    ksort($newConfig);

    $config = $this->config('xs_lists.lists');
    $config->setData($newConfig);
    $config->save();

    $this->tagsInvalidator->invalidateTags(['xs_lists']);

    parent::submitForm($form, $form_state);
  }

  /**
   * Submit handler for the "add-one-more" button. Increments the max counter and causes a rebuild.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   */
  public function addOne(array &$form, FormStateInterface $form_state): void
  {
    $form_state->set('listsCount', $form_state->get('listsCount') + 1);
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons. Selects and returns the fieldset with the names in it.
   *
   * @param array<string, mixed> $form       The form to apply the callback to
   * @param FormStateInterface   $form_state The current form state
   *
   * @return array<string, mixed> The modified $form
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state): array
  {
    return $form['lists'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['xs_lists.lists'];
  }
}
