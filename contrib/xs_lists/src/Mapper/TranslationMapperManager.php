<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Mapper;

/**
 * Class TranslationMapperManager.
 *
 * A service collector that generates a list of translations.
 */
final class TranslationMapperManager
{
  /**
   * @var array<string, array<int, TranslationMapperInterface[]>>
   */
  private array $translationMappers;

  /**
   * TranslationMapperManager constructor.
   */
  public function __construct()
  {
    $this->translationMappers = [];
  }

  /**
   * Adds a named translation mapper that will be used when list(string $name) is called.
   *
   * @param TranslationMapperInterface $translationMapper The translation mapper to add
   * @param int                        $priority          The priority of the mapper. Higher priority mappers
   *                                                      overwrite lower priority mappers when providing the
   *                                                      same key in a mapping
   */
  public function addTranslationMapper(TranslationMapperInterface $translationMapper, int $priority = 0): void
  {
    $this->translationMappers[$translationMapper->name()][$priority][] = $translationMapper;
  }

  /**
   * Returns the list of combined mappings of a set of named mappers.
   *
   * @param string $name The name of the mappers to combine
   *
   * @return array<string, array{labels: array{nl-NL: string, en-US: string}}> The list of combined mappings of a set of
   *                                                                           named mappers
   */
  public function list(string $name): array
  {
    if (!array_key_exists($name, $this->translationMappers)) {
      return [];
    }

    /** @var TranslationMapperInterface[] $sortedTranslationMappers */
    $sortedTranslationMappers = array_merge(...$this->translationMappers[$name]);

    $list = [];

    foreach ($sortedTranslationMappers as $mapper) {
      $list = array_merge($list, $mapper->mapping());
    }

    return $list;
  }
}
