<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Mapper;

/**
 * Interface TranslationMapperInterface.
 *
 * An interface that defines the contract a translation mapper must implement.
 */
interface TranslationMapperInterface
{
  /**
   * Returns the mapping of ids and their translations.
   *
   * @return array<string, array{labels: array{nl-NL: string, en-US: string}}>
   */
  public function mapping(): array;

  /**
   * Returns the name of the mapper, which can be used for grouping mappers.
   *
   * @return string The name of the mapper
   */
  public function name(): string;
}
