<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\TwigExtension;

use Drupal\xs_lists\ListClient;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

/**
 * Class TranslatableListTwigExtension.
 *
 * Contains several Twig filter functions for translating identifiers and labels to their counterparts.
 */
final class TranslatableListTwigExtension extends AbstractExtension implements ExtensionInterface
{
  /**
   * TranslatableListTwigExtension constructor.
   *
   * @param ListClient $listClient The list client to use
   */
  public function __construct(private readonly ListClient $listClient)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array
  {
    return [
      new TwigFilter('label_to_identifier', [$this, 'labelToIdentifier']),
      new TwigFilter('identifier_to_label', [$this, 'identifierToLabel']),
    ];
  }

  /**
   * Translate a label to its linked identifier based on the list its in.
   *
   * @param ?string $label The label to translate
   * @param string  $list  The list containing the label
   *
   * @return string The translated identifier, or the original label if no translation could be made
   */
  public function labelToIdentifier(?string $label, string $list): string
  {
    if (NULL === $label) {
      return '';
    }

    $list = $this->listClient->list($list);

    return $list->hasData()
      ? $list->labelToIdentifier($label)
      : $label;
  }

  /**
   * Translate an identifier to its label based on the list its in and the current language.
   *
   * @param ?string $identifier The identifier to translate
   * @param string  $list       The lost containing the identifier
   *
   * @return string The translated label, or the original identifier if no translation could be made
   */
  public function identifierToLabel(?string $identifier, string $list): string
  {
    if (NULL === $identifier) {
      return '';
    }

    $list = $this->listClient->list($list);

    return $list->hasData()
      ? $list->identifierToLabel($identifier)
      : $identifier;
  }
}
