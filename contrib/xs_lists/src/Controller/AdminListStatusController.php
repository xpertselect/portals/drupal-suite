<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\xs_lists\ListClient;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminListStatusController.
 *
 * Controller for the admin status page of the xs_lists module.
 */
final class AdminListStatusController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ListClient $listClient */
    $listClient = $container->get('xs_lists.list_client');

    /** @var RendererInterface $renderer */
    $renderer = $container->get('renderer');

    return new AdminListStatusController($listClient, $renderer);
  }

  /**
   * AdminListStatusController constructor.
   *
   * @param ListClient        $listClient The client for loading lists
   * @param RendererInterface $renderer   The Drupal template renderer
   */
  public function __construct(private readonly ListClient $listClient, private readonly RendererInterface $renderer)
  {
  }

  /**
   * Retrieves all the information necessary for the list statusPage and returns it as a Drupal render array.
   *
   * @return array<string, mixed> The render array
   *
   * @throws Exception Thrown on any rendering error
   */
  public function statusPage(): array
  {
    $build = [
      'list_header' => [
        '#markup' => $this->t('Configured lists'),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
      ],
      'list_table'  => [
        '#theme'  => 'table',
        '#header' => [
          $this->t('List'),
          $this->t('Items'),
        ],
        '#rows'   => [],
      ],
    ];

    foreach ($this->listClient->names() as $list) {
      $build['list_table']['#rows'][] = $this->buildListStatus($list);
    }

    return [
      '#type'   => '#markup',
      '#markup' => $this->renderer->render($build),
    ];
  }

  /**
   * Determine and return the status of a given list.
   *
   * @param string $title The name of the list
   *
   * @return array<int, mixed> The list status
   */
  private function buildListStatus(string $title): array
  {
    $list = $this->listClient->list($title);

    return !$list->hasData()
      ? [$title, $this->t('Unknown')]
      : [$title, count($list->getData())];
  }
}
