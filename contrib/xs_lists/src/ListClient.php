<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\xs_lists\Lists\Formatter;
use Drupal\xs_lists\Lists\TranslatableList;
use Drupal\xs_lists\Lists\TranslatableListFactory;

/**
 * Class ListClient.
 *
 * Client for interacting with the various lists and their contents.
 */
final class ListClient
{
  /**
   * The configured lists which are accessible.
   *
   * @var array<string, array>
   */
  private array $knownLists;

  /**
   * The names of the available lists (without any aliases).
   *
   * @var string[]
   */
  private array $originalNames;

  /**
   * Creates a ListClientInterface implementation.
   *
   * @param TranslatableListFactory       $listFactory    The listFactory to assign
   * @param ConfigFactoryInterface        $configFactory  The factory holding the xs_lists data
   * @param LoggerChannelFactoryInterface $channelFactory The factory for the appropriate logger
   *
   * @return ListClient The created implementation
   */
  public static function create(TranslatableListFactory $listFactory, ConfigFactoryInterface $configFactory,
                                LoggerChannelFactoryInterface $channelFactory): ListClient
  {
    return new ListClient($listFactory, $channelFactory->get('xs_lists'), $configFactory->get('xs_lists.lists'));
  }

  /**
   * ListClient constructor.
   *
   * @param TranslatableListFactory $listFactory The listFactory to assign
   * @param LoggerChannelInterface  $logger      The logger to use
   * @param ImmutableConfig         $knownLists  The configured lists
   */
  public function __construct(private readonly TranslatableListFactory $listFactory,
                              private readonly LoggerChannelInterface $logger, ImmutableConfig $knownLists)
  {
    $this->buildKnownLists($knownLists);
  }

  /**
   * Retrieve the original names of the configured lists without any aliases.
   *
   * @return string[] The original names
   */
  public function names(): array
  {
    return $this->originalNames;
  }

  /**
   * Retrieve the formatter to apply various formatting options to the contents of a translatable list.
   *
   * @return Formatter The formatter to use
   */
  public function formatter(): Formatter
  {
    static $formatter = NULL;

    if (is_null($formatter)) {
      $formatter = new Formatter();
    }

    return $formatter;
  }

  /**
   * Retrieve a list specified by its name.
   *
   * @param string $name The name of the list to retrieve
   *
   * @return TranslatableList The requested list
   */
  public function list(string $name): TranslatableList
  {
    if (!array_key_exists($name, $this->knownLists)) {
      $this->logger->warning('Unknown list "@list" requested', ['@list' => $name]);

      return $this->listFactory->dummy($name);
    }

    $list = $this->knownLists[$name];

    return $this->listFactory->createTranslatableList(
      $list['name'],
      $list['source'],
      $this->filterAliases($list['alias'])
    );
  }

  /**
   * Builds an array of known lists based on the configured lists and their aliases.
   *
   * @param ImmutableConfig $knownLists The configured lists
   */
  private function buildKnownLists(ImmutableConfig $knownLists): void
  {
    $names = [];
    $lists = [];

    foreach ($knownLists->getRawData() as $name => $data) {
      $names[]      = $name;
      $lists[$name] = $data;

      if (!empty($data['alias'])) {
        $aliases = explode(',', $data['alias']);

        foreach ($aliases as $alias) {
          $lists[trim($alias)] = $data;
        }
      }
    }

    $this->originalNames = $names;
    $this->knownLists    = $lists;
  }

  /**
   * Extracts and filters the aliases from a comma-separated string.
   *
   * @param string $aliases The string to filter the aliases from
   *
   * @return array<int, string> The aliases present in the string
   */
  private function filterAliases(string $aliases): array
  {
    $aliases  = explode(',', $aliases);
    $filtered = [];

    foreach ($aliases as $alias) {
      $alias = trim($alias);

      if (!empty($alias)) {
        $filtered[] = $alias;
      }
    }

    return $filtered;
  }
}
