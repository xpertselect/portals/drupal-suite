<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Lists;

/**
 * Class TranslatableList.
 *
 * Describes a list of identifiers and accompanying labels and offers functionality to translate between the two.
 */
final class TranslatableList
{
  /**
   * TranslatableList constructor.
   *
   * @param string               $name     The name of this list
   * @param string               $source   The source of the list data
   * @param array<string, array> $data     The raw contents of this list
   * @param string               $language The ISO 639-1 language code
   * @param string[]             $aliases  The name aliases of this list
   */
  public function __construct(private readonly string $name, private readonly string $source, private array $data,
                              private readonly string $language, private readonly array $aliases = [])
  {
  }

  /**
   * Return the name of this list.
   *
   * @return string The name of the list
   */
  public function getTitle(): string
  {
    return $this->name;
  }

  /**
   * Retrieve the source of the data (either a URL, filename or name of a function).
   *
   * @return string The source of the data
   */
  public function getSource(): string
  {
    return $this->source;
  }

  /**
   * Return all the name aliases for this list.
   *
   * @return array<int, string> The list aliases
   */
  public function getAliases(): array
  {
    return $this->aliases;
  }

  /**
   * Return the ISO 639-1 language code that will be used for `identifier` to `label` translations.
   *
   * @return string The ISO 639-1 language code
   */
  public function getLanguageCode(): string
  {
    return $this->language;
  }

  /**
   * Determine if this list contains any data.
   *
   * @return bool Whether the list has data
   */
  public function hasData(): bool
  {
    return count($this->data) > 0;
  }

  /**
   * Return the raw data of this list.
   *
   * @return array<string, array> The list data
   */
  public function getData(): array
  {
    return $this->data;
  }

  /**
   * Replace the data of this list with the given data.
   *
   * @param array<string, array> $data The new data
   */
  public function setData(array $data): void
  {
    $this->data = $data;
  }

  /**
   * Return all the identifiers of the entries in this list.
   *
   * @return array<int, string> The identifiers of the list
   */
  public function getIdentifiers(): array
  {
    return array_keys($this->data);
  }

  /**
   * Translates a given label to its matching identifier if such a translation can be made. Otherwise, the original
   * label will be returned.
   *
   * @param string $label The label to translate to an identifier
   *
   * @return string The identifier or original label
   */
  public function labelToIdentifier(string $label): string
  {
    foreach ($this->data as $identifier => $contents) {
      if (!array_key_exists('labels', $contents)) {
        continue;
      }

      if (!array_key_exists($this->language, $contents['labels'])) {
        continue;
      }

      if ($contents['labels'][$this->language] === $label) {
        return $identifier;
      }
    }

    return $label;
  }

  /**
   * Translates a given identifier to its matching display label based on the configured ISO 639-1 language code. If no
   * translation could be made the original identifier will be returned.
   *
   * @param string $identifier The identifier to translate to a display label
   *
   * @return string The label or original identifier
   */
  public function identifierToLabel(string $identifier): string
  {
    if (array_key_exists($identifier, $this->data)) {
      $item = $this->data[$identifier];

      if (!array_key_exists('labels', $item)) {
        return $identifier;
      }

      if (array_key_exists($this->language, $item['labels'])) {
        return $item['labels'][$this->language];
      }
    }

    return $identifier;
  }

  /**
   * Checks if an entry identified by the given identifier has a given field and a non-empty value for that field.
   *
   * @param string $identifier The identifier to translate to a display label
   * @param string $field      The field to check for
   *
   * @return bool Whether the given entry has a non-empty field
   */
  public function entryHasField(string $identifier, string $field): bool
  {
    if (!array_key_exists($identifier, $this->data)) {
      return FALSE;
    }

    $entry = $this->data[$identifier];

    if (!array_key_exists($field, $entry)) {
      return FALSE;
    }

    return !empty($entry[$field]);
  }
}
