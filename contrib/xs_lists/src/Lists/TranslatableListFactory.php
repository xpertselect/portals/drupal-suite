<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Lists;

use Drupal\Core\Language\LanguageManagerInterface;
use RuntimeException;

/**
 * Class TranslatableListFactory.
 *
 * Factory for creating TranslatableList instances including the retrieval of the actual list data.
 */
final class TranslatableListFactory
{
  /**
   * A `key => value` array where key is the Drupal language code and value equals the equivalent ISO 639-1 language.
   *
   * @var array<string, string>
   */
  public const LANGUAGE_MAP = [
    'nl' => 'nl-NL',
    'en' => 'en-US',
  ];

  /**
   * The in-memory cache of the created instances.
   *
   * @var array<string, TranslatableList>
   */
  private static array $memoryCache = [];

  /**
   * Creates a TranslatableListFactory instance.
   *
   * @param LanguageManagerInterface $languageManager The language that the implementations should provide translations
   *                                                  for
   * @param DataLoader               $dataLoader      The implementation responsible for retrieving the list data
   *
   * @return static The created instance
   */
  public static function create(LanguageManagerInterface $languageManager, DataLoader $dataLoader): self
  {
    return new TranslatableListFactory(
      self::LANGUAGE_MAP[$languageManager->getCurrentLanguage()->getId()],
      $dataLoader
    );
  }

  /**
   * TranslatableListFactory constructor.
   *
   * @param string     $languageId The language that the implementations should provide translations for
   * @param DataLoader $dataLoader The implementation responsible for retrieving the list data
   */
  public function __construct(private readonly string $languageId, private readonly DataLoader $dataLoader)
  {
  }

  /**
   * Return the assigned language ID.
   *
   * @return string The language ID
   */
  public function getLanguageId(): string
  {
    return $this->languageId;
  }

  /**
   * Creates a dummy list that doesn't hold any translations.
   *
   * @param string   $name  The name of the list
   * @param string[] $alias The aliases of the name
   *
   * @return TranslatableList The dummy list
   */
  public function dummy(string $name, array $alias = []): TranslatableList
  {
    return new TranslatableList($name, 'n/a', [], $this->getLanguageId(), $alias);
  }

  /**
   * Create a TranslatableList implementation based on the given data.
   *
   * @param string   $name   The name of the list
   * @param string   $source The source containing the data of the list
   * @param string[] $alias  The aliases of the name
   *
   * @return TranslatableList The created list
   */
  public function createTranslatableList(string $name, string $source, array $alias): TranslatableList
  {
    if (!empty(self::$memoryCache[$name])) {
      return self::$memoryCache[$name];
    }

    $listData = '/dev/null' !== $source ? $this->dataLoader->loadData($source) : [];
    $list     = new TranslatableList($name, $source, $listData, $this->getLanguageId(), $alias);

    self::$memoryCache[$name] = $list;

    foreach ($list->getAliases() as $alias) {
      if (array_key_exists($alias, self::$memoryCache)) {
        throw new RuntimeException('List alias conflicts with name or alias from another list');
      }

      self::$memoryCache[$alias] = $list;
    }

    return self::$memoryCache[$name];
  }
}
