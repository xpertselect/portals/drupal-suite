<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Lists;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;

final class DataLoader
{
  /**
   * The cache duration in seconds for how long the contents of a list should be stored in the Drupal cache.
   */
  public const CACHE_DURATION = 60 * 60;

  /**
   * Create a Dataloader instance based on the given arguments.
   *
   * @param CacheBackendInterface         $cacheBackend  The caching implementation to use
   * @param LoggerChannelFactoryInterface $loggerFactory The factory for creating logging channels
   * @param ClientFactory                 $clientFactory The factory for Guzzle implementations
   * @param HandlerStack                  $handlerStack  The Guzzle handler stack to add retry middleware to
   *
   * @return DataLoader The created instance
   */
  public static function create(CacheBackendInterface $cacheBackend, LoggerChannelFactoryInterface $loggerFactory,
                                ClientFactory $clientFactory, HandlerStack $handlerStack): self
  {
    $handlerStack->push(Middleware::retry(xs_general_create_guzzle_retry_decider(2, 'xs_lists')));

    return new DataLoader($cacheBackend, $loggerFactory->get('xs_lists'), $clientFactory->fromOptions([
      'handler' => $handlerStack,
    ]));
  }

  /**
   * DataLoader constructor.
   *
   * @param CacheBackendInterface  $cache  The caching implementation to use
   * @param LoggerChannelInterface $logger The logging channel to use
   * @param ClientInterface        $client The GuzzleHttp\ClientInterface to use for HTTP requests
   */
  public function __construct(private readonly CacheBackendInterface $cache,
                              private readonly LoggerChannelInterface $logger, private readonly ClientInterface $client)
  {
  }

  public function loadData(string $source): array
  {
    $cid    = 'xs_lists.list.' . $source;
    $cached = $this->cache->get($cid);

    if ($cached && isset($cached->data)) {
      return $cached->data;
    }

    if (function_exists($source)) {
      $data = $source();
    } elseif (is_file($source)) {
      $data = $this->loadJSONFile($source);
    } else {
      $data = $this->loadJSONURL($source);
    }

    if (!empty($data)) {
      $this->cache->set($cid, $data, time() + self::CACHE_DURATION, ['xs_lists']);
    }

    return $data;
  }

  /**
   * Loads the JSON contents of the given file.
   *
   * @param string $file The file to load
   *
   * @return array<string, array> The JSON contents of the file, or an empty array on any error
   */
  private function loadJSONFile(string $file): array
  {
    if (!is_readable($file)) {
      $this->logger->error('Cannot read file @file', ['@file' => $file]);

      return [];
    }

    $fileContents = file_get_contents($file);

    if (FALSE === $fileContents) {
      $this->logger->error('Unable to get file contents of @file', ['@file' => $file]);

      return [];
    }

    $json = json_decode($fileContents, TRUE);

    if (json_last_error() !== JSON_ERROR_NONE) {
      $this->logger->error('JSON decoding error for @file', ['@file' => $file]);

      return [];
    }

    return $json;
  }

  /**
   * Loads the JSON contents behind a given URL.
   *
   * @param string $url The URL to load
   *
   * @return array<string, array> The JSON contents behind the URL, or an empty array on any error
   */
  private function loadJSONURL(string $url): array
  {
    try {
      $response = $this->client->request('GET', $url);
      $json     = json_decode($response->getBody()->getContents(), TRUE);

      if (json_last_error() !== JSON_ERROR_NONE) {
        $this->logger->error('JSON decoding error (@url)', ['@url' => $url]);

        return [];
      }

      return $json;
    } catch (GuzzleException|RequestException $e) {
      $message = ($e instanceof RequestException && NULL !== $response = $e->getResponse())
        ? $response->getBody()->getContents()
        : $e->getMessage();

      $this->logger->error('HTTP request error while loading @list; @message', [
        '@list'    => $url,
        '@message' => $message,
      ]);

      return [];
    }
  }
}
