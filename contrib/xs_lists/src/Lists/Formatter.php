<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Lists;

/**
 * Class Formatter.
 *
 * Provides various utilities for formatting the contents of a TranslatableList.
 */
final class Formatter
{
  /**
   * Flatten the contents of TranslatableList::getData() such that it becomes an {identifier} => {display label} array.
   *
   * @param TranslatableList $list The list to flatten its contents of
   *
   * @return array<string, string> The {identifier} => {display label} list data
   */
  public function flatten(TranslatableList $list): array
  {
    $flattened = [];

    foreach ($list->getData() as $identifier => $contents) {
      if (!array_key_exists('labels', $contents)) {
        continue;
      }

      if (!array_key_exists($list->getLanguageCode(), $contents['labels'])) {
        continue;
      }

      $flattened[$identifier] = $contents['labels'][$list->getLanguageCode()];
    }

    return $flattened;
  }

  /**
   * Format the list with hierarchical information such that it becomes an {identifier} => {display label} array, with
   * parent labels included.
   *
   * @param TranslatableList $list The list to be formatted
   *
   * @return array<string, string> The formatted list
   */
  public function hierarchical(TranslatableList $list): array
  {
    $hierarchical = [];
    $data         = $list->getData();

    foreach ($data as $identifier => $contents) {
      $parents = $this->getParents($contents, $data);
      $label   = $contents['labels'][$list->getLanguageCode()] ?? NULL;

      if (is_null($label)) {
        continue;
      }

      if (empty($parents)) {
        $label = sprintf('<b>%s</b>', $label);
      }

      foreach ($parents as $parent) {
        $parentLabel = $parent['labels'][$list->getLanguageCode()] ?? NULL;

        if (is_null($parentLabel)) {
          continue;
        }

        $label = sprintf('<b>%s</b> — %s', $parentLabel, $label);
      }

      $hierarchical[$identifier] = $label;
    }

    return $hierarchical;
  }

  /**
   * Sort the output of TranslatableList::getData() alphabetically.
   *
   * @param TranslatableList $list The list to sort its contents of
   *
   * @return array<string, array<int|string, mixed>> The sorted list contents
   */
  public function sorted(TranslatableList $list): array
  {
    $data     = $list->getData();
    $language = $list->getLanguageCode();

    uasort($data, function($a, $b) use ($language) {
      if (!array_key_exists('labels', $a)) {
        return 0;
      }

      if (!array_key_exists('labels', $b)) {
        return 0;
      }

      if (!array_key_exists($language, $a['labels'])) {
        return 0;
      }

      if (!array_key_exists($language, $b['labels'])) {
        return 0;
      }

      return strcmp($a['labels'][$language], $b['labels'][$language]);
    });

    return $data;
  }

  /**
   * Group the output of TranslatableList::getData() based on the field present in the items of the list.
   *
   * @param TranslatableList $list       The list to group
   * @param string           $groupField The field to group by
   *
   * @return array<int|string, array<string, array>> The grouped list contents
   */
  public function grouped(TranslatableList $list, string $groupField): array
  {
    $grouped = [];

    foreach ($list->getData() as $itemId => $value) {
      if (!array_key_exists($groupField, $value)) {
        continue;
      }

      if (NULL === $value[$groupField]) {
        $value[$groupField] = 'null';
      }

      if (!array_key_exists($value[$groupField], $grouped)) {
        $grouped[$value[$groupField]] = [];
      }

      $grouped[$value[$groupField]][$itemId] = $value;
    }

    return $grouped;
  }

  /**
   * Get the parent list entries of a given child entry recursively.
   *
   * @param array<string, mixed>                $child The child entry
   * @param array<string, array<string, mixed>> $data  All list entries
   *
   * @return array<string, array<string, mixed>> The parent entries
   */
  private function getParents(array $child, array $data): array
  {
    if (!array_key_exists('parent', $child)) {
      return [];
    }

    $parentIdentifier = $child['parent'];

    if (!array_key_exists($parentIdentifier, $data)) {
      return [];
    }

    $parent = [$parentIdentifier => $data[$parentIdentifier]];

    return array_merge($parent, $this->getParents($data[$parentIdentifier], $data));
  }
}
