<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_lists\Event;

use Drupal\xs_lists\Lists\TranslatableList;
use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class ListToEnumEvent.
 *
 * Event that describes that a list is about to be transformed into an Open API Specification Enum.
 */
final class ListToEnumEvent implements StoppableEventInterface
{
  /**
   * ListToEnumEvent constructor.
   *
   * @param TranslatableList $list The list that will be transformed
   */
  public function __construct(public readonly TranslatableList $list)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function isPropagationStopped(): bool
  {
    return FALSE;
  }
}
