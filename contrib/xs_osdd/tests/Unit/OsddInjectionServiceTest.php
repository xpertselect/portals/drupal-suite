<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_osdd\Unit;

use Drupal\xs_osdd\OsddInjectionService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_osdd\TestCase;

/**
 * @internal
 */
final class OsddInjectionServiceTest extends TestCase
{
  public function testAttachToPage(): void
  {
    $pageAttributes       = ['foo' => 'bar'];

    // Phpunit can't handle the static method call
    $osddInjectionService = M::mock(OsddInjectionService::class, [TRUE], function(MI $mock) {
      $mock->shouldReceive('createOSDDRoute')->andReturn('https://portals.local');
    })->makePartial();

    Assert::assertArrayNotHasKey('#attached', $pageAttributes);

    $osddInjectionService->attachToPage($pageAttributes);

    Assert::assertArrayHasKey('#attached', $pageAttributes);
  }

  public function testNotAttachToPageWhenOSDDIsNotActive(): void
  {
    $pageAttributes       = ['foo' => 'bar'];

    // Phpunit can't handle the static method call
    $osddInjectionService = M::mock(OsddInjectionService::class, [FALSE], function(MI $mock) {
      $mock->shouldReceive('createOSDDRoute')->andReturn('https://portals.local');
    })->makePartial();

    Assert::assertArrayNotHasKey('#attached', $pageAttributes);

    $osddInjectionService->attachToPage($pageAttributes);

    Assert::assertArrayNotHasKey('#attached', $pageAttributes);
  }
}
