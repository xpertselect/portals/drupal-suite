<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_osdd\Unit\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xs_osdd\Form\AdminOsddSettingsForm;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\xs_osdd\TestCase;

/**
 * @internal
 */
final class AdminOsddSettingsFormTest extends TestCase
{
  public function testFormCanBeCreated(): void
  {
    $container = M::mock(ContainerInterface::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with('config.factory')
        ->andReturn(M::mock(ConfigFactoryInterface::class));
    });

    Assert::assertInstanceOf(AdminOsddSettingsForm::class, AdminOsddSettingsForm::create($container));
  }
}
