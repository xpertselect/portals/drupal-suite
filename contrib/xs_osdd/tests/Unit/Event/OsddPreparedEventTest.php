<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_osdd\Unit\Event;

use Drupal\xs_osdd\Event\OsddPreparedEvent;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\StoppableEventInterface;
use Tests\xs_osdd\TestCase;

/**
 * @internal
 */
final class OsddPreparedEventTest extends TestCase
{
  public function testEventIsStoppable(): void
  {
    $osdd  = [];
    $event = new OsddPreparedEvent($osdd);

    Assert::assertInstanceOf(StoppableEventInterface::class, $event);
  }

  public function testEventDoesNotStopPropagation(): void
  {
    $osdd  = [];
    $event = new OsddPreparedEvent($osdd);

    Assert::assertFalse($event->isPropagationStopped());
  }

  public function testOsddDataIsPassedByReference(): void
  {
    $osdd  = [];
    $event = new OsddPreparedEvent($osdd);

    Assert::assertArrayNotHasKey('foo', $event->osdd);
    Assert::assertArrayNotHasKey('foo', $osdd);

    $event->osdd['foo'] = 'bar';

    Assert::assertArrayHasKey('foo', $event->osdd);
    Assert::assertArrayHasKey('foo', $osdd);
  }
}
