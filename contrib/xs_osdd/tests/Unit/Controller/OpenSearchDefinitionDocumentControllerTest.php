<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_osdd\Unit\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\xs_osdd\Controller\OpenSearchDefinitionDocumentController;
use Drupal\xs_osdd\XsOsdd;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\xs_osdd\TestCase;

/**
 * @internal
 */
final class OpenSearchDefinitionDocumentControllerTest extends TestCase
{
  public function testControllerCanBeCreated(): void
  {
    $container = M::mock(ContainerInterface::class, function(MI $mock) {
      $mock->shouldReceive('get')
        ->with('event_dispatcher')
        ->andReturn(M::mock(EventDispatcherInterface::class));

      $mock->shouldReceive('get')
        ->with('renderer')
        ->andReturn(M::mock(RendererInterface::class));

      $mock->shouldReceive('get')
        ->with('config.factory')
        ->andReturn(M::mock(ConfigFactoryInterface::class, function(MI $mock) {
          $mock->shouldReceive('get')
            ->with(XsOsdd::SETTINGS_KEY)
            ->andReturn(M::mock(ImmutableConfig::class, function(MI $mock) {
              $mock->shouldReceive('get')
                ->with('osdd_shortname')
                ->andReturn('portals.local');
            }));
        }));

      $mock->shouldReceive('get')
        ->with('logger.factory')
        ->andReturn(M::mock(LoggerChannelFactoryInterface::class, function(MI $mock) {
          $mock->shouldReceive('get')
            ->with(XsOsdd::LOG_CHANNEL)
            ->andReturn(M::mock(LoggerChannelInterface::class));
        }));
    });

    Assert::assertInstanceOf(
      OpenSearchDefinitionDocumentController::class,
      OpenSearchDefinitionDocumentController::create($container)
    );
  }

  public function testViewDocumentDispatchedEvent(): void
  {
    $eventFired = FALSE;

    $eventDispatcher = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventFired) {
      $mock->shouldReceive('dispatch')->andReturnUsing(function() use (&$eventFired) {
        $eventFired = TRUE;
      });
    });

    $renderer = M::mock(RendererInterface::class, function(MI $mock) {
      $mock->shouldReceive('renderRoot')->andReturn('');
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldNotReceive('error');
    });

    Assert::assertFalse($eventFired);

    // Phpunit can't handle the static method call
    $controller = M::mock(OpenSearchDefinitionDocumentController::class, [$eventDispatcher, $renderer, 'portals.local', $logger], function(MI $mock) {
      $mock->shouldReceive('getSearchURL')->andReturn('https://portals.local');
      $mock->shouldReceive('getSuggestURL')->andReturn('https://portals.local');
    })->makePartial();
    $controller->viewDocument();

    Assert::assertTrue($eventFired);
  }

  public function testViewDocumentReturnsOkResponse(): void
  {
    $eventDispatcher = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventFired) {
      $mock->shouldReceive('dispatch')->andReturn();
    });

    $renderer = M::mock(RendererInterface::class, function(MI $mock) {
      $mock->shouldReceive('renderRoot')->andReturn('');
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldNotReceive('error');
    });

    // Phpunit can't handle the static method call
    $controller = M::mock(OpenSearchDefinitionDocumentController::class, [$eventDispatcher, $renderer, 'portals.local', $logger], function(MI $mock) {
      $mock->shouldReceive('getSearchURL')->andReturn('https://portals.local');
      $mock->shouldReceive('getSuggestURL')->andReturn('https://portals.local');
    })->makePartial();
    $response   = $controller->viewDocument();

    Assert::assertInstanceOf(Response::class, $response);
    Assert::assertEquals(200, $response->getStatusCode());
  }

  public function testViewDocumentWithoutShortNameReturns404Response(): void
  {
    $this->expectException(NotFoundHttpException::class);

    $eventDispatcher = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventFired) {
      $mock->shouldReceive('dispatch')->andReturn();
    });

    $renderer = M::mock(RendererInterface::class, function(MI $mock) {
      $mock->shouldReceive('renderRoot')->andReturn('');
    });

    $logger = M::mock(LoggerChannelInterface::class, function(MI $mock) {
      $mock->shouldReceive('error');
    });

    // Phpunit can't handle the static method call
    $controller = M::mock(OpenSearchDefinitionDocumentController::class, [$eventDispatcher, $renderer, NULL, $logger], function(MI $mock) {
      $mock->shouldReceive('getSearchURL')->andReturn('https://portals.local');
      $mock->shouldReceive('getSuggestURL')->andReturn('https://portals.local');
    })->makePartial();

    $response = $controller->viewDocument();
  }
}
