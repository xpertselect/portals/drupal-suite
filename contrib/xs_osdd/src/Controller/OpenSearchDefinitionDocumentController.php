<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_osdd\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\xs_general\DrupalUrlUtilities;
use Drupal\xs_osdd\Event\OsddPreparedEvent;
use Drupal\xs_osdd\XsOsdd;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OpenSearchDefinitionDocumentController.
 *
 * Drupal controller for rendering an OSD Document as an HTTP response.
 */
final class OpenSearchDefinitionDocumentController extends ControllerBase
{
  use DrupalUrlUtilities;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var EventDispatcherInterface $eventDispatcher */
    $eventDispatcher = $container->get('event_dispatcher');

    /** @var RendererInterface $renderer */
    $renderer = $container->get('renderer');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    $shortName = $configFactory->get(XsOsdd::SETTINGS_KEY)->get('osdd_shortname');

    /** @var LoggerChannelFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('logger.factory');

    return new self($eventDispatcher, $renderer, $shortName, $loggerFactory->get(XsOsdd::LOG_CHANNEL));
  }

  /**
   * OpenSearchDefinitionDocumentController constructor.
   *
   * @param EventDispatcherInterface $eventDispatcher The service for dispatching events
   * @param RendererInterface        $twigRenderer    The service for rendering twig templates
   * @param null|string              $shortName       The short name to display or null if the config is not set
   * @param LoggerChannelInterface   $loggerChannel   The logging implementation
   */
  public function __construct(private readonly EventDispatcherInterface $eventDispatcher,
                              private readonly RendererInterface $twigRenderer,
                              private readonly ?string $shortName,
                              private readonly LoggerChannelInterface $loggerChannel)
  {
  }

  /**
   * Render the OSD document in its XML representation.
   *
   * @return Response The OSD Document as an HTTP response
   */
  public function viewDocument(): Response
  {
    if (empty($this->shortName)) {
      $this->loggerChannel->error('The osdd route was accessed while the short name was not configured.');

      throw new NotFoundHttpException();
    }

    $build = $this->createRenderArray();

    return new Response($this->twigRenderer->renderRoot($build), Response::HTTP_OK, [
      'Content-Type' => 'text/xml; charset=utf-8',
    ]);
  }

  /**
   * Return the URL to the search endpoint that should be offered. The URL **must** include a `{searchTerms}` section
   * indicating where the terms should be injected.
   *
   * @return string The search endpoint URL
   */
  public function getSearchURL(): string
  {
    $url = Url::fromRoute('xs_searchable_content.web.search', [
      'query'       => 'QUERY_PLACEHOLDER',
      'filters'     => '-',
      'page_number' => 1,
    ])->setAbsolute();

    return str_replace(
      'QUERY_PLACEHOLDER',
      '{searchTerms}',
      $this->drupalUrlAsString($url)
    );
  }

  /**
   * Return the URL to the suggest endpoint that should be offered.
   * The URL **must** include a `{searchTerms}` section indicating where the terms should be injected.
   *
   * @return string The suggest endpoint URL or null if none is available
   */
  public function getSuggestURL(): string
  {
    $url = Url::fromRoute('xs_searchable_content.api.suggest_content_osdd', [
      'query' => 'QUERY_PLACEHOLDER',
    ])->setAbsolute();

    return str_replace(
      'QUERY_PLACEHOLDER',
      '{searchTerms}',
      $this->drupalUrlAsString($url)
    );
  }

  /**
   * Creates the Twig render array for rendering the OSD Document.
   *
   * @return array<string, null|string> The Twig render array
   */
  private function createRenderArray(): array
  {
    $build = [
      '#short_name'  => $this->shortName,
      '#description' => NULL,
      '#search_url'  => $this->getSearchURL(),
      '#suggest_url' => $this->getSuggestURL(),
    ];

    $this->eventDispatcher->dispatch(new OsddPreparedEvent($build));

    return array_merge($build, ['#theme' => 'open_search_description_document']);
  }
}
