<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_osdd\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_general\ManagedSettingsTrait;
use Drupal\xs_osdd\XsOsdd;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminOsddSettingsForm.
 *
 * Drupal form for managing the settings of the `xs_osdd` module.
 */
final class AdminOsddSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    return new self($configFactory);
  }

  /**
   * AdminOsddSettingsForm constructor.
   *
   * @param ConfigFactoryInterface $configFactory The Drupal configuration factory
   */
  public function __construct(ConfigFactoryInterface $configFactory)
  {
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xs_osdd_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    if ($this->isManaged(XsOsdd::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $form['osdd_settings'] = [
      '#type'        => 'details',
      '#title'       => $this->t('OSDD settings'),
      '#open'        => TRUE,
    ];

    $form['osdd_settings']['osdd_shortname'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('OSDD shortname'),
      '#description'   => $this->t('Enter a shortname to display to the user when tab-to-search is used.'),
      '#default_value' => $this->config(XsOsdd::SETTINGS_KEY)->get('osdd_shortname'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(XsOsdd::SETTINGS_KEY, TRUE)) {
      return;
    }

    $config = $this->config(XsOsdd::SETTINGS_KEY);
    $config->set('osdd_shortname', $form_state->getValue('osdd_shortname'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XsOsdd::SETTINGS_KEY];
  }
}
