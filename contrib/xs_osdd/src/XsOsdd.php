<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_osdd;

/**
 * Class XsOsdd.
 *
 * Class holding the constants of the `xs_osdd` Drupal module.
 */
final class XsOsdd
{
  /**
   * The key holding the Drupal configuration of the `xs_osdd` module.
   */
  public const SETTINGS_KEY = 'xs_osdd.settings';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'xs_osdd';
}
