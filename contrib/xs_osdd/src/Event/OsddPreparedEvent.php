<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_osdd\Event;

use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class OsddPreparedEvent.
 *
 * Event that indicates that the Twig render array of the OSD Document has been prepared and is about to be given to the
 * Twig rendering service.
 */
final class OsddPreparedEvent implements StoppableEventInterface
{
  /**
   * OsddPreparedEvent constructor.
   *
   * @param array<string, null|string> $osdd The OSDD Twig render array
   */
  public function __construct(public array &$osdd)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function isPropagationStopped(): bool
  {
    return FALSE;
  }
}
