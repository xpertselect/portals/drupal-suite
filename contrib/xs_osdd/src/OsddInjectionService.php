<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_osdd;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;

/**
 * Class OsddInjectionService.
 *
 * Drupal service for attaching a link to the OSDD endpoint in HTML responses.
 */
final class OsddInjectionService
{
  /**
   * Creates a new OsddInjectionService instance.
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    $shortName = $configFactory->get(XsOsdd::SETTINGS_KEY)->get('osdd_shortname');

    return new self(!empty($shortName));
  }

  /**
   * OsddInjectionService constructor.
   *
   * @param bool $isActive A boolean indicating whether dataset MLT is enabled
   */
  public function __construct(private readonly bool $isActive)
  {
  }

  /**
   * Attach a reference to the active OSDD endpoint to the page, if there is one.
   *
   * @param array<string, mixed> $page The Twig attributes of the page
   */
  public function attachToPage(array &$page): void
  {
    if (!$this->isActive) {
      return;
    }

    $page['#attached']['html_head'][] = [[
      '#tag'        => 'link',
      '#attributes' => [
        'type' => 'application/opensearchdescription+xml',
        'rel'  => 'search',
        'href' => $this->createOSDDRoute(),
      ],
    ], 'osdd-link'];
  }

  /**
   * Returns the route for retrieving the osdd definitions.
   *
   * @return GeneratedUrl|string the route for retrieving the osdd definitions
   */
  public function createOSDDRoute(): GeneratedUrl|string
  {
    return Url::fromRoute('xs_osdd.open-search-definition-document')
      ->setAbsolute()
      ->toString();
  }
}
