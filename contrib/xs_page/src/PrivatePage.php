<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_page;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\NodeInterface;

/**
 * Class PrivatePage.
 *
 * Container class containing utilities pertaining to the PrivatePage feature.
 */
final class PrivatePage
{
  /**
   * Retrieve the field definition of the private page field.
   *
   * @param bool $withDisplayOptions Whether to include the display options in the definition
   *
   * @return BaseFieldDefinition The field definition
   */
  public static function getFieldDefinition(bool $withDisplayOptions): BaseFieldDefinition
  {
    $definition = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Private page'))
      ->setDescription(t('If you mark this checkbox the page will be hidden from all anonymous users.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    if ($withDisplayOptions) {
      $definition->setDisplayConfigurable('form', TRUE);
      $definition->setDisplayOptions('form', [
        'type'     => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ]);
    }

    return $definition;
  }

  /**
   * Determine if a given Node is considered 'private'.
   *
   * @param NodeInterface $node The node to check
   *
   * @return bool Whether the page is private
   */
  public static function isPrivate(NodeInterface $node): bool
  {
    if (!$node->hasField('private_page')) {
      return FALSE;
    }

    $values = $node->get('private_page')->getValue();

    if (0 === count($values)) {
      return FALSE;
    }

    $value = $values[0];

    if (!array_key_exists('value', $value)) {
      return FALSE;
    }

    return 1 === (int) $value['value'];
  }
}
