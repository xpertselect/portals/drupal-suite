<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_page\Unit;

use Drupal;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Mockery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\xs_page\TestCase;

/**
 * @internal
 */
final class ModuleTest extends TestCase
{
  public function testPrivatePageIsNotAddedForNonNodes(): void
  {
    $mockedEntity = Mockery::mock(EntityTypeInterface::class);
    $mockedEntity->shouldReceive('id')
      ->andReturn('foo');

    $output = xs_page_entity_base_field_info($mockedEntity);

    $this->assertCount(0, $output);
  }

  public function testPrivatePageIsAddedForNodes(): void
  {
    $mockedContainer = Mockery::mock(ContainerInterface::class);
    $mockedManager   = Mockery::mock(FieldTypePluginManagerInterface::class);

    $mockedContainer->shouldReceive('get')
      ->with('plugin.manager.field.field_type')
      ->andReturn($mockedManager);

    $mockedManager->shouldReceive('getDefaultStorageSettings')
      ->with('boolean')
      ->andReturn([]);

    $mockedManager->shouldReceive('getDefaultFieldSettings')
      ->with('boolean')
      ->andReturn([]);

    Drupal::setContainer($mockedContainer);

    $mockedEntity = Mockery::mock(EntityTypeInterface::class);
    $mockedEntity->shouldReceive('id')
      ->andReturn('node');

    $output = xs_page_entity_base_field_info($mockedEntity);

    $this->assertCount(1, $output);
    $this->assertArrayHasKey('private_page', $output);
  }

  public function testNodeAccessIsAllowedForLoggedInUsers(): void
  {
    $mockedNode    = Mockery::mock(NodeInterface::class);
    $mockedField   = Mockery::mock(FieldItemList::class);
    $mockedAccount = Mockery::mock(AccountInterface::class);

    $mockedField->shouldReceive('getValue')
      ->andReturn([['value' => 1]]);

    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnTrue();

    $mockedNode->shouldReceive('get')
      ->with('private_page')
      ->andReturn($mockedField);

    $mockedAccount->shouldReceive('isAnonymous')
      ->andReturnFalse();

    $this->assertInstanceOf(
      AccessResultNeutral::class,
      xs_page_node_access($mockedNode, NULL, $mockedAccount)
    );
  }

  public function testNodeAccessIsForbiddenForAnonymousUsers(): void
  {
    $mockedNode    = Mockery::mock(NodeInterface::class);
    $mockedField   = Mockery::mock(FieldItemList::class);
    $mockedAccount = Mockery::mock(AccountInterface::class);

    $mockedField->shouldReceive('getValue')
      ->andReturn([['value' => 1]]);

    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnTrue();

    $mockedNode->shouldReceive('get')
      ->with('private_page')
      ->andReturn($mockedField);

    $mockedAccount->shouldReceive('isAnonymous')
      ->andReturnTrue();

    $this->assertInstanceOf(
      AccessResultForbidden::class,
      xs_page_node_access($mockedNode, NULL, $mockedAccount)
    );
  }
}
