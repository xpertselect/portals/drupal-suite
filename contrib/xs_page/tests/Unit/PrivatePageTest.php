<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_drupal_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_page\Unit;

use Drupal;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_page\PrivatePage;
use Mockery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\xs_page\TestCase;

/**
 * @internal
 */
final class PrivatePageTest extends TestCase
{
  public function testFieldDefinitionAttributes(): void
  {
    $mockedContainer = Mockery::mock(ContainerInterface::class);
    $mockedManager   = Mockery::mock(FieldTypePluginManagerInterface::class);

    $mockedContainer->shouldReceive('get')
      ->with('plugin.manager.field.field_type')
      ->andReturn($mockedManager);

    $mockedManager->shouldReceive('getDefaultStorageSettings')
      ->with('boolean')
      ->andReturn([]);

    $mockedManager->shouldReceive('getDefaultFieldSettings')
      ->with('boolean')
      ->andReturn([]);

    Drupal::setContainer($mockedContainer);

    $fieldDefinition = PrivatePage::getFieldDefinition(TRUE);

    $this->assertEquals('boolean', $fieldDefinition->getType());
    $this->assertTrue($fieldDefinition->isTranslatable());
    $this->assertTrue($fieldDefinition->isRevisionable());
  }

  public function testNodeIsNotPrivateWhenFieldIsMissing(): void
  {
    $mockedNode = Mockery::mock(NodeInterface::class);
    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnFalse();

    $this->assertFalse(PrivatePage::isPrivate($mockedNode));
  }

  public function testNodeIsNotPrivateWhenValueIsZero(): void
  {
    $mockedNode  = Mockery::mock(NodeInterface::class);
    $mockedField = Mockery::mock(FieldItemList::class);

    $mockedField->shouldReceive('getValue')
      ->andReturn([['value' => 0]]);

    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnTrue();

    $mockedNode->shouldReceive('get')
      ->with('private_page')
      ->andReturn($mockedField);

    $this->assertFalse(PrivatePage::isPrivate($mockedNode));
  }

  public function testNodeIsPublicWhenValueIsEmptyArray(): void
  {
    $mockedNode  = Mockery::mock(NodeInterface::class);
    $mockedField = Mockery::mock(FieldItemList::class);

    $mockedField->shouldReceive('getValue')
      ->andReturn([]);

    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnTrue();

    $mockedNode->shouldReceive('get')
      ->with('private_page')
      ->andReturn($mockedField);

    $this->assertFalse(PrivatePage::isPrivate($mockedNode));
  }

  public function testNodeIsPublicWhenValueKeyIsMissing(): void
  {
    $mockedNode  = Mockery::mock(NodeInterface::class);
    $mockedField = Mockery::mock(FieldItemList::class);

    $mockedField->shouldReceive('getValue')
      ->andReturn([['foo' => 'bar']]);

    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnTrue();

    $mockedNode->shouldReceive('get')
      ->with('private_page')
      ->andReturn($mockedField);

    $this->assertFalse(PrivatePage::isPrivate($mockedNode));
  }

  public function testNodeIsPrivateWhenValueIsOne(): void
  {
    $mockedNode  = Mockery::mock(NodeInterface::class);
    $mockedField = Mockery::mock(FieldItemList::class);

    $mockedField->shouldReceive('getValue')
      ->andReturn([['value' => 1]]);

    $mockedNode->shouldReceive('hasField')
      ->with('private_page')
      ->andReturnTrue();

    $mockedNode->shouldReceive('get')
      ->with('private_page')
      ->andReturn($mockedField);

    $this->assertTrue(PrivatePage::isPrivate($mockedNode));
  }
}
